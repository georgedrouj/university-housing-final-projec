<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?php echo e(asset('css/login.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/framework.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/main.css')); ?>" rel="stylesheet">
    <title>تسجيل الدخول</title>
</head>


<body>
    <div class="background">
        <div class="shape"></div>
        <div class="shape"></div>
    </div>
    <form method="POST" action="<?php echo e(route('login')); ?>">
        <?php echo csrf_field(); ?>
        <h3> تسجيل الدخول</h3>

        <label for="username">اسم المستخدم</label>
        <input class="c-black" type="text" placeholder=" اسم المستخدم " id="username" name="user_id" required>

        <label for="password">كلمة المرور</label>
        <input class="c-black" type="password" placeholder="كلمة السر" id="password" name='password' required>
        <button type="submit"> تسجيل الدخول</button>
    </form>
    <?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>;
</body>

</html><?php /**PATH G:\Programing\universtyHousing\resources\views/auth/login.blade.php ENDPATH**/ ?>