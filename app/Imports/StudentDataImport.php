<?php

namespace App\Imports;

use App\Models\StudentData;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithHeadings;

class StudentDataImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new StudentData([
            'university_id'=>$row['university_id'],
            'academic_status'=>$row['academic_status'],
            'collage'=>$row['collage'],
            'gender'=>$row['gender'],
            'name'=>$row['name'],

        ]);
    }
}
