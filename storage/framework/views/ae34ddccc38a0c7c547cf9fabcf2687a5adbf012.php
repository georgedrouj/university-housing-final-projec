<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- main template css file -->
    
    <link rel="stylesheet" href="<?php echo e(asset('css/framework.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="<?php echo e(asset('css/normalize.css')); ?>">
    <!-- font awesome library -->
    <link rel="stylesheet" href="<?php echo e(asset('css/all.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/rooms.css')); ?>">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <script src="<?php echo e(asset('vendor/sweetalert/sweetalert.all.js')); ?>"></script>
    <title>جامعة القلمون الخاصة</title>
</head>

<body>

    <div class="page d-flex  ">
        <div class="sidebar  p-20 p-relative">
            <div class="logo">
                <img src="<?php echo e(asset('images/homoon.png')); ?>" alt="">          
          </div>
            <ul>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/student">
                        <i class="fa-solid fa-sliders fa-fw"></i>
                        <span> الصفحة الرئيسية</span>
                    </a>
                </li>
                <li>
                    <a class="active d-flex align-center fs-14  rad-6 p-10 " href="/student/houses">
                        <i class="fa-solid fa-hotel fa-fw"></i>
                        <span> السكنات</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="myorder.html">

                        <i class="fa-solid fa-users-gear fa-fw"></i>
                        <span>  طلباتي </span>
                    </a>
                </li>
                <li>
                    <form method="POST" action="<?php echo e(route('logout')); ?>" id="my_form">
                        <?php echo csrf_field(); ?>
                    <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">

                        <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                        <span>  تسجيل خروج</span>
                    </a>
                    </form>
                </li>
            </ul>

        </div>
        <div class="content ">
            <!-- start head -->
            <!-- end head -->
            <h1 class="p-relative"> السكنات</h1>
            <div class="wrapper d-flex gap-20 ">
                <!-- <div class="welcome bg-white rad-10 "> -->
                <div class="intro p-20   bg-eee">
               
              
                    <?php $__currentLoopData = $rooms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $room): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                
                    <?php if($room->getEmpty() >0): ?>
                    <div id="container">
                     
                       
                            
                        <div class="product">

                            <div class="info">
                                <h2> <?php echo e($room->room_name); ?></h2>
                                <ul>
                                  
                                    <li><strong>المساحة : </strong><?php echo e($room->space); ?> </li>
                                    <li><strong>السعة : </strong><?php echo e($room->capacity); ?></li>
                                    <li><strong>الشواغر: </strong> <?php echo e($room->getEmpty()); ?></li>
                                    <li><strong>الاتجاه: </strong><?php echo e($room->direction); ?></li>
                                    <li><strong>السعر: </strong> <?php echo e($room->price); ?></li>

                                </ul>
                                <?php if(!$hasRoom): ?>
                           
                                <form method="POST" action="/student/regestroom/<?php echo e($room->id); ?>" id="<?php echo e($room->id); ?>">
                                    <?php echo csrf_field(); ?>
                                <button class="btn" > 
                                <a href="javascript:{}" onclick="confirm(<?php echo e($room->id); ?>)"  >
                                 تسجيل
                                </a>
                                </button>
                                </form>
                                <?php elseif(!$hasSwitchRoom && $hasActiveRoom): ?>
                                <form method="POST" action="/student/switch_room/<?php echo e($room->id); ?>" id="<?php echo e($room->id); ?>">
                                    <?php echo csrf_field(); ?>
                                <button class="btn" > 
                                <a href="javascript:{}" onclick="confirm(<?php echo e($room->id); ?>)"  >
                                 تبديل
                                </a>
                                </button>
                                </form>
                        
                                <?php endif; ?>
                            </div>
                        </div>
                        
                        
                    </div>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>



            </div>


        </div>
    </div>
<?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<script type="text/javascript">
function confirm(room_id){
  
    var form = document.getElementById(room_id);
    Swal.fire({
        title: "هل انت متأكد",
        text: "لا يمكنك الغاء الطلب بعد التأكيد",
        showCancelButton: true,
        confirmButtonColor:" var(--move-color)",
        confirmButtonText: "تم", 
        closeOnConfirm: false,
        cancelButtonText: 'إغلاق',
    }).then((result)=>{
        if (result.isConfirmed) {
            form.submit();
        }
    });
return false;
}


</script>
</body><?php /**PATH C:\Users\User\Desktop\university-housing-final-projec\resources\views/student/myrooms.blade.php ENDPATH**/ ?>