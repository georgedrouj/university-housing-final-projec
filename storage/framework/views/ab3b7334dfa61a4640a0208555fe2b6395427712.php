<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- framework css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/framework.css')); ?>">
    <!-- main template css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="<?php echo e(asset('css/normalize.css')); ?>">
    <!-- font awesome library -->
    <link rel="stylesheet" href="<?php echo e(asset('css/all.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/users.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/create-user.css')); ?>">
  
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <script src="<?php echo e(asset('vendor/sweetalert/sweetalert.all.js')); ?>"></script>
 <?php echo \Livewire\Livewire::styles(); ?>

    <title>جامعة القلمون الخاصة</title>
</head>

<body>
    <div class="page d-flex ">
        <div class="sidebar  p-20 p-relative">
            <div class="logo">
                <img src="<?php echo e(asset('images/homoon.png')); ?>" alt="">          
          </div>
            <ul>
                <li>
                    <a class="d-flex align-center fs-14  rad-6 p-10 " href="/">
                        <i class="fa-solid fa-sliders fa-fw"></i>
                        <span>الصفحة الرئيسية</span>
                    </a>
                </li>
                <li>
                    <a class="d-flex align-center fs-14  rad-6 p-10 " href="/admin/houses">
                        <i class="fa-solid fa-hotel fa-fw"></i>
                        <span>إدارة السكنات</span>
                    </a>
                </li>
                <li>
                    <a class="active d-flex align-center fs-14  rad-6 p-10 " href="/admin/users">

                        <i class="fa-solid fa-users-gear fa-fw"></i>
                        <span> إدارة المستخدمين </span>
                    </a>
                </li>
                <li>
                    
                    <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">

                        <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                        <span>  تسجيل خروج</span>
                    </a>
                    <form method="POST" action="<?php echo e(route('logout')); ?>" id="my_form" class="remove-all-styles">
                        <?php echo csrf_field(); ?>
                    </form>
                </li>
            </ul>

        </div>
        <div class="content">
            <!-- start head -->
            <!-- end head -->
            <h1 class="p-relative">إدارة المستخدمين</h1>

            <div class="wrapper d-grid gap-20 ">
                <div class="welcome bg-white rad-10 ">
                    <div class="intro p-20 d-flex space-between bg-eee">
                        <button class="user">
                            <a href="javascript:{}"  onclick="openForm('add-form')"> إنشاء مستخدم جديد
                                <i class="fa-solid fa-plus fa-fw"></i></a>
                        </button>
                
                     <?php
if (! isset($_instance)) {
    $html = \Livewire\Livewire::mount('users', [])->html();
} elseif ($_instance->childHasBeenRendered('D44984J')) {
    $componentId = $_instance->getRenderedChildComponentId('D44984J');
    $componentTag = $_instance->getRenderedChildComponentTagName('D44984J');
    $html = \Livewire\Livewire::dummyMount($componentId, $componentTag);
    $_instance->preserveRenderedChild('D44984J');
} else {
    $response = \Livewire\Livewire::mount('users', []);
    $html = $response->html();
    $_instance->logRenderedChild('D44984J', $response->id(), \Livewire\Livewire::getRootElementTagName($html));
}
echo $html;
?>
              
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <form method="POST" action="<?php echo e(route('admin.user.update',$user->id)); ?>" class="popupForm" style="display: none" id="<?php echo e($user->id); ?>" >
        <?php echo csrf_field(); ?>
        <?php echo method_field('PUT'); ?>
        <h3> تعديل مستخدم</h3>
        <button class="closeModal" onclick="colseForm('<?php echo e($user->id); ?>')" type="button"> </button>
        <label for="username">اسم المستخدم</label>
        <input type="text" placeholder="أدخل اسم المستخدم: " id="username" name="name" value="<?php echo e($user->name); ?>" required>
        <label for="text"> اختر دور المستخدم</label>
        <select class=" gender"  name="type" value="<?php echo e($user->type); ?>">
            <option value="manager">مدير</option>
            <option value="supervisor">مشرف</option>
            <option value="admin">أدمن</option>
        </select>
        <label for=""> اختر جنس المستخدم</label>
        <select  name="gender" class=" gender" value=" <?php echo e($user->gender); ?>">
         
            <option value="male">ذكر</option>
            <option value="female">أنثى</option>
        </select>
        <button type="submit"> تعديل </button>
    </form>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <form method="POST" action="<?php echo e(route('admin.user.store')); ?>" class="popupForm"  style="display: none" id="add-form">
        <?php echo csrf_field(); ?>
        <h3> إضافة مستخدم</h3>
        <button class="closeModal" onclick="colseForm('add-form')" type="button"> </button>
        <label for="username">اسم المستخدم</label>
        <input type="text" placeholder="أدخل اسم المستخدم: " name="name" required>
        <label for="text"> اختر دور المستخدم</label>
        <select class=" gender" name="type">
            <option value="manager">مدير</option>
            <option value="supervisor">مشرف</option>
            <option value="admin">أدمن</option>
        </select>
        <label for=""> اختر جنس المستخدم</label>
        <select  name="gender" class=" gender">
         
            <option value="male">ذكر</option>
            <option value="female">أنثى</option>
        </select>
        <button type="submit"> إضافة </button>
    </form>
    <?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>;
   
    <script type="text/javascript">
        function openForm(id){
          console.log(id);
          document.getElementById(id).style.display = "block";
          document.body.id='popedup';
      }
      function colseForm(id){
        document.getElementById(id).style.display = "none";
          document.body.id='';
      }
        
     
  
          </script>
  <script>
      function confirm(user_id){
          console.log("hi");
          var form = document.getElementById(user_id);
         Swal.fire({
              title: "هل انت متأكد",
              text: "هل تريد حذف هذا المستخدم",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "red",
              confirmButtonText: "حذف", 
              closeOnConfirm: false
          }).then((result)=>{
              if(result.isConfirmed){
                  form.submit();
              }
          });
      return false;
      }
  </script>
            <?php echo \Livewire\Livewire::scripts(); ?>


</body>
<?php /**PATH G:\Programing\universtyHousing\resources\views/admin/users2.blade.php ENDPATH**/ ?>