<?php


use App\Http\Controllers\Shared\MaintenanceController;
use App\Http\Controllers\Manager\StudentDataController;
use App\Http\Controllers\Shared\ContractController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Shared\AdvertismentController;
use App\Http\Controllers\Shared\HouseController;
use App\Http\Controllers\Shared\RoomController;
use App\Http\Controllers\testController;
use App\Http\Controllers\Manager\BlockedRoomController;
use App\Http\Controllers\Manager\SupervisorController;
use App\Http\Controllers\Shared\ConfirmationController;
use App\Http\Controllers\Shared\QuittanceController;
use App\Http\Controllers\Shared\SwitchRoomController;
use App\Http\Controllers\Student\OrderController;
use App\Http\Livewire\ManagerOrders;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;



Route::group(['middleware'=>'auth'],function(){
Route::get('/home',function(){

 if(Auth::user()->type=='admin'){
    return redirect('/admin');
 }
 if(Auth::user()->type=='student'){
    return redirect('/student');
 }
 if(Auth::user()->type=='manager'){
    return redirect('/manager');
 }
 if(Auth::user()->type=='supervisor'){
    return redirect('/supervisor');
 }
});
Route::group(['middleware'=>'role:admin','prefix'=>'admin','as'=>'admin.'],function(){
   Route::view('/','admin.admin');
   Route::resource('/houses',HouseController::class)->except('show');
   Route::resource('/houses/{house_id}',RoomController::class)->only(['index','store']);
   Route::resource('/rooms',RoomController::class)->only(['update','destroy']);
   Route::resource('/user',UserController::class);
 
});



Route::group(['middleware'=>'role:manager','prefix'=>'manager','as'=>'manager.'],function(){
   Route::view('/','manager.manager');
    Route::resource('/contract',ContractController::class)->only(['index','store','create','update']);
    Route::resource('/students',StudentDataController::class)->only(['index','store']);
    Route::post('/contract/{room_id}',[ContractController::class,'store']);
    Route::resource('/contract',ContractController::class)->only(['index','update']);
    Route::resource('/maintenance',MaintenanceController::class)->only(['index','update']);
    Route::resource('/quittance',QuittanceController::class)->only(['index','update']);
    Route::get('/houses/{house_id}/block/{room_id}',[BlockedRoomController::class,'index']);
    Route::post('/block/{room_id}',[BlockedRoomController::class,'blockRoom'])->name('block');
    Route::post('/unblock/{room_id}',[BlockedRoomController::class,'UnBlockedRoom'])->name('unblock');
    Route::resource('/houses',HouseController::class)->only(['index']);
    Route::resource('/houses/{house_id}',RoomController::class)->only(['index']);
    Route::view('/houses/{house_id}/{room_id}','manager.student_regstration');
    Route::resource('/switch_room', SwitchRoomController::class)->only(['index','update']);
    Route::post('/quittance_by_manager/{contract_id}',[QuittanceController::class,'quittance_by_manager'])->name('quittanceByManager');
    Route::resource('/supervisor', SupervisorController::class)->only(['index','update']);
    Route::get('/viewroom/{room_id}',ManagerOrders::class);
    Route::get('/viewstudent/{student_id}',ManagerOrders::class);
    Route::resource('/advertisments',AdvertismentController::class)->only(['index','store']);
});





Route::group(['middleware'=>'role:student','prefix'=>'student','as'=>'student.'],function(){
         Route::view('/','student.student');
         Route::resource('/houses',HouseController::class)->only(['index']);
         Route::resource('/houses/{house_id}', RoomController::class)->only(['index']);
         Route::post('/regestroom/{roomid}',[ContractController::class,'regestRoom']);
         Route::resource('/maintenance',MaintenanceController::class)->only(['store']);
         Route::get('/myOrder',[OrderController::class,'index'])->name('order');
         Route::resource('/quittance',QuittanceController::class)->only(['store']);
         Route::post('/switch_room/{room_id}',[SwitchRoomController::class,'store']);
         Route::resource('/advertisments',AdvertismentController::class)->only(['index']);
});

Route::group(['middleware'=>'role:supervisor','prefix'=>'supervisor','as'=>'supervisor.'],function(){
         Route::view('/','supervisor.supervisor');
         Route::resource('/maintenance',MaintenanceController::class)->only(['index','update']);
         Route::resource('/quittance',QuittanceController::class)->only(['index','update']);
         Route::resource('/switch_room',SwitchRoomController::class)->only(['index','update']);
         Route::resource('/advertisments',AdvertismentController::class)->only(['index']);
});

});


Route::redirect('/', '/login', 301);
Route::get('/test',[testController::class,'s']);

Route::get('/test',[testController::class,'index']);

