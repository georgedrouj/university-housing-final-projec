<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- main template css file -->

    <link rel="stylesheet" href="<?php echo e(asset('css/framework.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="<?php echo e(asset('css/normalize.css')); ?>">
    <!-- font awesome library -->
    <link rel="stylesheet" href="<?php echo e(asset('css/all.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/rooms.css')); ?>">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <?php echo \Livewire\Livewire::styles(); ?>

    <script src="<?php echo e(asset('vendor/sweetalert/sweetalert.all.js')); ?>"></script>
    <title>جامعة القلمون الخاصة</title>
</head>

<body>

    <div class="page d-flex  ">
        <div class="sidebar  p-20 p-relative">
            <div class="logo">
                <img src="<?php echo e(asset('images/homoon.png')); ?>" alt="">
            </div>
            <ul>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/student">
                        <i class="fa-solid fa-sliders fa-fw"></i>
                        <span> الصفحة الرئيسية</span>
                    </a>
                </li>
                <li>
                    <a class="active d-flex align-center fs-14  rad-6 p-10 " href="/student/houses">
                        <i class="fa-solid fa-hotel fa-fw"></i>
                        <span> السكنات</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="myorder.html">

                        <i class="fa-solid fa-users-gear fa-fw"></i>
                        <span> طلباتي </span>
                    </a>
                </li>
                <li>
                    <form method="POST" action="<?php echo e(route('logout')); ?>" id="my_form">
                        <?php echo csrf_field(); ?>
                        <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 "
                            onclick="document.getElementById('my_form').submit();">

                            <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                            <span> تسجيل خروج</span>
                        </a>
                    </form>
                </li>
            </ul>

        </div>
        <div class="content ">
            <!-- start head -->
            <!-- end head -->
            <h1 class="p-relative"> السكنات</h1>

            <?php
if (! isset($_instance)) {
    $html = \Livewire\Livewire::mount('rooms')->html();
} elseif ($_instance->childHasBeenRendered('iPh6X6v')) {
    $componentId = $_instance->getRenderedChildComponentId('iPh6X6v');
    $componentTag = $_instance->getRenderedChildComponentTagName('iPh6X6v');
    $html = \Livewire\Livewire::dummyMount($componentId, $componentTag);
    $_instance->preserveRenderedChild('iPh6X6v');
} else {
    $response = \Livewire\Livewire::mount('rooms');
    $html = $response->html();
    $_instance->logRenderedChild('iPh6X6v', $response->id(), \Livewire\Livewire::getRootElementTagName($html));
}
echo $html;
?>

        </div>
    </div>
    <?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo \Livewire\Livewire::scripts(); ?>

    <script type="text/javascript">
        function confirm(room_id){
  
    var form = document.getElementById(room_id);
    Swal.fire({
        title: "هل انت متأكد",
        text: "لا يمكنك الغاء الطلب بعد التأكيد",
        showCancelButton: true,
        confirmButtonColor:" var(--move-color)",
        confirmButtonText: "تم", 
        closeOnConfirm: false,
        cancelButtonText: 'إغلاق',
    }).then((result)=>{
        if (result.isConfirmed) {
            form.submit();
        }
    });
return false;
}


    </script>
 
</body><?php /**PATH G:\Programing\universtyHousing\resources\views/student/myrooms.blade.php ENDPATH**/ ?>