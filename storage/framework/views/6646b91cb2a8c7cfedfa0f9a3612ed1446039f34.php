<!DOCTYPE html>
<html dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- main template css file -->
    
    <link rel="stylesheet" href="<?php echo e(asset('css/framework.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="<?php echo e(asset('css/normalize.css')); ?>">
    <!-- font awesome library -->
    <link rel="stylesheet" href="<?php echo e(asset('css/all.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/users.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/supervisor.css')); ?>">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <title>جامعة القلمون الخاصة</title>
</head>
<body>
    <div class="page d-flex ">
    <div class="sidebar  p-20 p-relative">
        <h3 class="p-relative txt-c mt-0">
            النبراس
        </h3>
        <ul>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/">
                    <i class="fa-solid fa-sliders fa-fw"></i>
                    <span>الصفحة الرئيسية</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/houses">
                    <i class="fa-solid fa-hotel fa-fw"></i>
                    <span>السكنات </span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/import">

                    <i class="fa-solid fa-people-line fa-fw"></i>
                    <span> الطلاب</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="comfirmation.html">

                    <i class="fa-brands fa-readme fa-fw"></i>
                    <span> طلب التثبيت</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/contract">

                    <i class="fa-brands fa-readme fa-fw"></i>
                    <span> طلب التسجيل</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="quitence.html">

                    <i class="fa-brands fa-readme fa-fw"></i>
                    <span> طلب براءة الذمة</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="mantinance.html">

                    <i class="fa-brands fa-readme fa-fw"></i>
                    <span> طلب  الصيانة</span>
                </a>
            </li>
            <li>
                <a class=" active d-flex align-center fs-14  rad-6 p-10 " href="<?php echo e(route('manager.supervisor.index')); ?>">

                    <i class="fa-solid fa-users"></i>
                    <span>مشرفي الوحدات السكنية</span>
                </a>
            </li>
            <li>
                <form method="POST" action="<?php echo e(route('logout')); ?>" id="my_form">
                    <?php echo csrf_field(); ?>
                <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">

                    <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                    <span>  تسجيل خروج</span>
                </a>
                </form>
            </li>

        </ul>

    </div>
    <div class="content">
        <!-- start head -->

        <!-- end head -->



        <div class="wrapper d-grid gap-20 ">
            <div class="welcome bg-white rad-10 ">
                <h1 class="p-relative">إدارة المشرفين</h1>

                    <div class="container">
                        <table>
                            <thead>
                                <tr>
                                    
                                    <th> اسم المشرف</th>
                                    <th> رقم السكن</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                <?php $__currentLoopData = $supervisors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $supervisor): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    
                                    <td> <?php echo e($supervisor->name); ?></td>
                                    
                                    <td>  
                                        <select id="housesList" name="house_id" onchange="StoreSupervisor()">
                                            <?php if($supervisor->gender=="female"): ?>
                                            <?php $__currentLoopData = $femaleHouses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $female): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option><?php echo e($female->id); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php else: ?>
                                            <?php $__currentLoopData = $maleHouses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $male): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option><?php echo e($male->id); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </select>
                                        
                                        <button class="pen" > <a  href="javascript:{}" > <i class="fa-solid fa-plus"></i> </a> 
                                            <form id="" method="POST" action='<?php echo e(route('manager.supervisor.store')); ?>' style="all: revert">
                                                <?php echo csrf_field(); ?>
                                          </form>
                                        </button>
                                        
                                    </td>
                                   
                                    
                                  
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                           
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>
    <?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <script>
function StoreSupervisor($house_id)
{
  var e = document.getElementById("housesList");
  var value = e.value;
  Swal.fire({
          title: "هل انت متأكد",
          text: " لايمكنك التراجع",
      //    html: '<input type="text" id="login" class="swal2-input" placeholder="سبب الرفض" required>',
          showCancelButton: true,
          confirmButtonColor: "var(--red-color)",
          confirmButtonText: "تم", 
          cancelButtonText: 'أغلاق',
          closeOnConfirm: false,
          preConfirm: () => {
    const login = Swal.getPopup().querySelector('#login').value
    return {login:login}
    }
   
      }).then((result) => {
    if(result.isConfirmed){
    var ok = document.createElement("input");
    ok.value="false";
    ok.name="ok";
    ok.style.display="none";
    form.appendChild(ok);
    var reason = document.createElement("input");
    reason.value= `${result.value.login}`.trim();
    reason.name="reason";
    reason.style.display="none";
    form.appendChild(reason);
   form.submit();}
})
    return false;
    }
         
    </script>
</body>
</html><?php /**PATH C:\Users\User\Desktop\university-housing-final-projec\resources\views/manager/supervisors.blade.php ENDPATH**/ ?>