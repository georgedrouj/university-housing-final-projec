<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo e(asset('css/create-house.css')); ?>">
    <title></title>
</head>

<body>
    <form method="POST" action="<?php echo e(route('admin.house.store')); ?>"  id="popupForm">
        <?php echo csrf_field(); ?>
        <h3> إضافة سكن</h3>
        <label for="housename">اسم السكن</label>
        <input type="text" placeholder="اسم السكن" name="house_id" required>

        <select  name="gender" class="gender"> 
            <option value="اختر جنس السكن">اختر جنس السكن </option>
            <option value="male">ذكر</option>
            <option value="female">أنثى</option>
        </select>
        <button type="submit">  إضافة </button>

    </form>
</body>

</html><?php /**PATH C:\Users\User\Desktop\university-housing-final-projec\resources\views/admin/house/create.blade.php ENDPATH**/ ?>