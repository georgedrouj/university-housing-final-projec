<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quittance extends Model
{
    use HasFactory;
    use SoftDeletes;
    use \Znck\Eloquent\Traits\BelongsToThrough;
    protected $table='quittances';
    protected $fillable=['quittance_date','quittance_state','missings','contract_id','manager_comment','electricity'];
   public function getContract(){
    return $this->belongsTo(Contract::class,'contract_id');
  }
  public function getStudent(){
    return $this->belongsToThrough(Student::class,Contract::class);
}
public function getRoom(){
    return $this->belongsToThrough(Room::class,Contract::class);
}
public  function getHouse()
{
return $this->belongsToThrough(House::class,[Room::class,Contract::class]);
}
}
