<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Imports\StudentDataImport;
use App\Models\Student;
use App\Models\StudentData;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;

class StudentDataController extends Controller
{
   
    public function index()
    {
        $students=Student::search(request('student-search'))->get();
      // $students=Student::all();
        return view('manager.students')->with(['students'=>$students]);
    }
    public function generateUsersFromExcel(){
        $studentsDatas=StudentData::all();
        foreach($studentsDatas as $data){
            User::firstOrCreate(['user_id'=>$data->university_id],['password'=>Hash::make($data->university_id),'name'=>$data->name,'type'=>'student','gender'=>$data->gender]);
            Student::firstOrcreate(['university_id'=>$data->university_id],['student_name'=>$data->name,'gender'=>$data->gender,'disapproval'=>false]);
        }
    }
    public function store(Request $request)
    {
        StudentData::truncate();
        try{
        Excel::import(new StudentDataImport,$request->file);}
        catch(Exception $e){
            return redirect()->back()->with('errors','أن هذ الملف غير صالح للعملية');
        }
        $this->generateUsersFromExcel();
        return redirect()->back()->with('success','تمت العملية بنجاح');
    }


   

 
}
