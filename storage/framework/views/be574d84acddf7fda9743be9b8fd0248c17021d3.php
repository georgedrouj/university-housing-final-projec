<div>
    <input wire:model.debounce.500ms="searchQuery" type="text" placeholder="بحث" >
    <select wire:model="houseQuery" style="background-color: var(--black-color); color:var(--white-color); text-align-last:center;border: none;">
        <option value="">الكل</option>
        <option value="without house">بدون سكن</option>
        <?php $__currentLoopData = $maleHouses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $male): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <option value="<?php echo e($male->id); ?>"><?php echo e($male->id); ?></option>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php $__currentLoopData = $femaleHouses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $female): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

        <option value="<?php echo e($female->id); ?>"><?php echo e($female->id); ?></option>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
    <div class="container">
        <table>
            <thead>
                <tr>
                    <th> رقم المشرف</th>
                    <th> اسم المشرف</th>
                    <th> رقم السكن</th>
                </tr>
            </thead>
            
            <tbody>
                <?php $__currentLoopData = $supervisors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $supervisor): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
               
                <tr>
                    <td> <?php echo e($supervisor->user_id); ?></td>
                    <td> <?php echo e($supervisor->name); ?></td>
                 
                    <td>  
                     <form  method="POST" action="/manager/supervisor/<?php echo e($supervisor->id); ?>" id="<?php echo e($supervisor->id); ?>" >
                            <?php echo method_field('PUT'); ?>
                            <?php echo csrf_field(); ?>
                        <select  name="house_id" onchange="StoreSupervisor('<?php echo e($supervisor->id); ?>')" style="background-color: var(--black-color); color:var(--white-color); text-align-last:center;border: none;">
                            <option value="">بدون سكن</option>
                            <?php if($supervisor->gender=="female"): ?>
                            <?php $__currentLoopData = $femaleHouses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $female): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <option value="<?php echo e($female->id); ?>" <?php if($supervisor->house_id==$female->id): ?>
                                selected
                            <?php endif; ?>><?php echo e($female->id); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                            <?php $__currentLoopData = $maleHouses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $male): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($male->id); ?>" <?php if($supervisor->house_id==$male->id): ?>
                                selected
                            <?php endif; ?>><?php echo e($male->id); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </select>
                    </form>
                        
                        
                    </td>
                   
                    
                  
                </tr>
           
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
           
        </table>
    </div>
    
</div>
<?php /**PATH G:\Programing\universtyHousing\resources\views/livewire/supervisors.blade.php ENDPATH**/ ?>