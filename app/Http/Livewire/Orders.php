<?php

namespace App\Http\Livewire;

use App\Models\Contract;
use App\Models\Maintenance;
use App\Models\Quittance;
use App\Models\SwitchRoom;
use Illuminate\Support\Arr;
use Livewire\Component;
use Stringable;

use function PHPUnit\Framework\returnSelf;

class Orders extends Component
{
   public $orders2;
    public $searchQuery;
    public $type;
    public function mount(){
        $student_id=Auth()->user()->user_id;
        $contract=Contract::where([['student_id','=',$student_id],['contract_state','=','active_now']])->first();
        $maintenances=Maintenance::where('contract_id','=',$contract->id)->get();
        $switchRooms=SwitchRoom::where('contract_id','=',$contract->id)->get();
        $quittances=Quittance::where('contract_id','=',$contract->id)->get();
       
        $state=array(
            "in_manager"=>"عند الادارة",
            "in_supervisor"=>"عند مشرف السكن",
            "accpeted"=>'مقبول',
            "active_now"=>'جاري',
            "rejected"=>'مرفوض',
            'deleted'=>'محذوف'
        );
        $this->orders2=collect();
        foreach($maintenances as $maintenance){
            $this->orders2->push(
                [
                    'order_state'=>$state[$maintenance->maintenace_state],
                    'order_id'=>$maintenance->id,
                    'created_at'=>$maintenance->created_at,
                    'name'=>'صيانة'
                ]
            );
        }
        foreach($quittances as $quittance){
            $this->orders2->push(
                [
                    'order_state'=>$state[ $quittance->quittance_state],
                    'order_id'=> $quittance->id,
                    'created_at'=> $quittance->created_at,
                    'name'=>'براءة ذمة'
                ]
            );
        }
        foreach($switchRooms as $switchRoom){
            $this->orders2->push(
                [
                    'order_state'=>$state[$switchRoom->switch_room_state],
                    'order_id'=> $switchRoom->id,
                    'created_at'=> $switchRoom->created_at,
                    'name'=>'تبديل'
                ]
            );
        }
        $this->orders2 = collect( $this->orders2)->sortBy('created_at')->reverse();
      $this->searchQuery="";
      $this->type="";
       }
    public function render()
    {

       $orders=$this->orders2->filter(function ($item)  {
        return  (stristr($item['name'], $this->searchQuery)||stristr($item['order_id'], $this->searchQuery))&&(stristr($item['name'], $this->type));
    })->sortBy('created_at')->reverse();
        return view('livewire.orders',['orders'=>$orders]);
    }
}
