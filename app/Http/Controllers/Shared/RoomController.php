<?php

namespace App\Http\Controllers\Shared;

use App\Http\Controllers\Controller;
use App\Models\House;
use App\Models\Room;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Row;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($house_id)
    {
        if (Auth()->user()->type == 'student') {
            $rooms = Room::where([['house_id', '=', $house_id], ['blocked_now', '=', '0']])->get();
            $studnet = Student::find(Auth()->user()->user_id);
            $hasRoom = $studnet->hasRoom();
            $hasSwitchRoom = $studnet->hasSwitchRoom();
            $hasActiveRoom = ($studnet->getActiveContract->count() > 0);

            return view('student.myrooms')->with(['rooms' => $rooms, 'hasRoom' => $hasRoom, 'hasSwitchRoom' => $hasSwitchRoom, 'hasActiveRoom' => $hasActiveRoom]);
        } else {
            $rooms = Room::where('house_id', '=', $house_id)->get();
            if (Auth()->user()->type == 'manager') {
                return view('manager.room')->with(['rooms' => $rooms]);
            } else {
                return view('admin.rooms')->with(['rooms' => $rooms]);
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Room $room)
    {
        return view('admin.room.create')->with('room', $room);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $house_id)
    {

        $this->validate(
            $request,
            [
                'name' => ['required', 'regex:/[0-9][0-9][0-9][A|B]?/'],
                'capacity' => 'required|min:1|max:3',
                'floor' => 'required',
            ],
            [
                'room_number.required' => 'يجب إدخال رقم الغرفة',
                'room_number.regex:/[0-9][0-9][0-9][A|B]?/' => 'رقم الغرفة يجب أن يكون مكون من ثلات أرقام',
                'room_capacity.required' => 'يجب إدخال سعة الغرفة',
                'room_capacity.min:1|max:3' => ' يجب أن تكون سعة الغرفة أكبر أو تساوي 1 وأصغر أو تساوي 3',
                'floor_name.required' => 'يجب إدخال رقم الطابق',
            ]
        );
        $room = new Room();
        $room->room_name = $request->input('name');
        $room->capacity = $request->input('capacity');
        $room->floor_name = $request->input('floor');
        $room->direction = $request->input('direction');
        $room->space = $request->input('space');
        $room->price = $request->input('price');
        $room->blocked_now = false;
        $room->blocked_reason = null;
        $room->house_id = $house_id;
        $room->save();
        return redirect()->back()->with('success', 'تمت إضافة الغرفة بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function show(Room $room)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function edit(Room $room)
    {
        $room->first();
        return view('admin.room.edit')->with('room', $room);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Room $room)
    {
        $room->update([
            'room_name' => $request->input('name'),
            'capacity' => $request->input('capacity'),
            'price' => $request->input('price'),
            'floor_name' => $request->input('floor'),
            'direction' => $request->input('direction'),
            'space' => $request->input('space'),
            'house_id' => $room->house_id

        ]);
        return redirect()->back()->with('success', 'تمت العملية بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function destroy(Room $room)
    {
        $room->delete();
        return redirect()->back()->with('success', 'تمت العملية بنجاح');
    }
    
}
