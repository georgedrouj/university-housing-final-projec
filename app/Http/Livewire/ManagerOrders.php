<?php

namespace App\Http\Livewire;

use App\Models\Contract;
use App\Models\Maintenance;
use App\Models\Quittance;
use App\Models\Student;
use App\Models\SwitchRoom;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\UnauthorizedException;
use Livewire\Component;

class ManagerOrders extends Component
{
   
    public $orders2;
    public function mount(){
    $student_id=request('student_id');
    $room_id=request('room_id');
    $contracts="";
    $this->orders2=collect();
    if(isset($student_id)){
        if(Auth()->user()->type=="sutdent"){
            if(Auth()->user()->user_id!=$student_id){
               abort(403);
            }
        }
        $contracts=Contract::withTrashed()->where('student_id',$student_id)->get();
    }
    else{
        $contracts=Contract::withTrashed()->where('room_id',$room_id)->get();
        }
        $state=array(
            "in_manager"=>"عند الادارة",
            "in_supervisor"=>"عند مشرف السكن",
            "accepted"=>'مقبول',
            "active_now"=>'جاري',
            "rejected"=>'مرفوض',
            'deleted'=>'منتهي'
        );
        $img=array(
            "in_manager"=>"عند الادارة",
            "in_supervisor"=>"عند مشرف السكن",
            "accepted"=>'مقبول',
            "active_now"=>'جاري',
            "rejected"=>'مرفوض',
            'deleted'=>'انتهى'
        );
        $color=array(
            "in_manager"=>"var(--gray-color)",
            "in_supervisor"=>"var(--gray-color)",
            "accepted"=>'var(--green-color)',
            "active_now"=>'var(--green-color)',
            "rejected"=>'var(--red-color)',
            'deleted'=>'var(--red-color)'
        );
    foreach($contracts as $contract){
        $maintenances=Maintenance::where('contract_id','=',$contract->id)->get();
        $switchRooms=SwitchRoom::where('contract_id','=',$contract->id)->get();
        $quittances=Quittance::where('contract_id','=',$contract->id)->get();
        $this->orders2->push(
            [
                
                'order_state'=>$state[$contract->contract_state],
                'order_id'=>$contract->id,
                'created_at'=>$contract->created_at,
                'name'=>'عقد',
                'color'=>$color[$contract->contract_state],
                'img'=>$img[$contract->contract_state],
            ]
        );
        foreach($maintenances as $maintenance){
            $this->orders2->push(
                [
                    'order_state'=>$state[$maintenance->maintenace_state],
                    'order_id'=>$maintenance->id,
                    'created_at'=>$maintenance->created_at,
                    'name'=>'صيانة',
                    'color'=>$color[$maintenance->maintenace_state],
                    'img'=>$img[$maintenance->maintenace_state],
                ]
            );
        }
        foreach($quittances as $quittance){
            $this->orders2->push(
                [
                    'order_state'=>$state[ $quittance->quittance_state],
                    'order_id'=> $quittance->id,
                    'created_at'=> $quittance->created_at,
                    'name'=>'براءة ذمة',
                    'color'=>$color[$quittance->quittance_state],
                    'img'=>$img[$quittance->quittance_state],
                ]
            );
        }
        foreach($switchRooms as $switchRoom){
            $this->orders2->push(
                [
                    'order_state'=>$state[$switchRoom->switch_room_state],
                    'order_id'=> $switchRoom->id,
                    'created_at'=> $switchRoom->created_at,
                    'name'=>'تبديل',
                    'color'=>$color[$switchRoom->switch_room_state],
                    'img'=>$img[$switchRoom->switch_room_state],
                ]
            );
        }
     
    }
    $this->orders2 = collect( $this->orders2)->sortBy('created_at')->reverse();
    dd($this->orders2);
    }
    public function render()
    {
        return view('livewire.manager-orders');
    }
}
