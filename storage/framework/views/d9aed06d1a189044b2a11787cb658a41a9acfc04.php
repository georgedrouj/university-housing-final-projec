<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo e(asset('css/create-room.css')); ?>">
    <title></title>
</head>

<body>
    <form method="POST" action="<?php echo e(route('admin.room.update',$room->id)); ?>">
        <?php echo csrf_field(); ?>
        <?php echo method_field('PUT'); ?>
        <h3> تعديل غرفة</h3>
        <button class="closeModal"> </button>
        <label for="roomname">رقم الغرفة</label>
        <input type="text" placeholder="أدخل رقم الغرفة:" name="name" value="<?php echo e($room->room_name); ?>">
        <label for="floorname">رقم الطابق</label>
        <input type="text" placeholder="أدخل رقم الطابق:" name="floor" value="<?php echo e($room->floor_name); ?>">
        <label for="roomcapacity">سعة الغرفة</label>
        <input type="text" placeholder="أدخل سعة الغرفة:" name="capacity" value="<?php echo e($room->capacity); ?>">
        <label for="roomspace">مساحة الغرفة</label>
        <input type="text" placeholder="أدخل مساحة الغرفة:" name="space" value="<?php echo e($room->space); ?>" >
        <label for="roomdirection">اتجاه الغرفة</label>
        <input type="text" placeholder="أدخل اتجاه الغرفة:" name="direction" value="<?php echo e($room->direction); ?>">
        <label for="roomprice">سعر الغرفة</label>
        <input type="price" placeholder="أدخل سعر الغرفة:" name="price" value="<?php echo e($room->price); ?>">
        <button> تعديل </button>

    </form>
</body>

</html><?php /**PATH C:\Users\User\Desktop\university-housing-final-projec\resources\views/admin/room/edit.blade.php ENDPATH**/ ?>