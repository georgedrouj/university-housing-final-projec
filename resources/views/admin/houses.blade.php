<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- framework css file -->
    <link rel="stylesheet" href="{{ asset('css/framework.css') }}">
    <!-- main template css file -->
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <!-- font awesome library -->
    <link rel="stylesheet" href="{{ asset('css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/houses.css') }}">

    <link rel="stylesheet" href="{{ asset('css/create-house.css') }}">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <script src="{{ asset('vendor/sweetalert/sweetalert.all.js') }}"></script>

    <title>جامعة القلمون الخاصة</title>
</head>

<body>

    <div class="page d-flex  ">
        <div class="sidebar  p-20 p-relative">
            <h3 class="p-relative txt-c mt-0">
                النبراس
            </h3>
            <ul>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/">
                        <i class="fa-solid fa-sliders fa-fw"></i>
                        <span>الصفحة الرئيسية</span>
                    </a>
                </li>
                <li>
                    <a class="active d-flex align-center fs-14  rad-6 p-10 " href="/admin/houses">
                        <i class="fa-solid fa-hotel fa-fw"></i>
                        <span>إدارة السكنات </span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/admin/user">

                        <i class="fa-solid fa-users-gear fa-fw"></i>
                        <span> إدارة المستخدمين </span>
                    </a>
                    <li>
                       
                        <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">
    
                            <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                            <span>  تسجيل خروج</span>
                        </a>
                        <form method="POST" action="{{ route('logout') }}" id="my_form" style="display: none">
                            @csrf
                        </form>
                    </li>

            </ul>

        </div>
        <div class="content ">
            <!-- start head -->
            <!-- end head -->
            <h1 class="p-relative">إدارة السكن</h1>
            <div class="wrapper d-flex gap-20 ">
                <!-- <div class="welcome bg-white rad-10 "> -->
                        
                <div class="intro p-20   bg-eee">
                  
                    @foreach ($houses as $house)
                    <div class="container">
                        <div class="card" >

                            <div class="contentBx">
                                <h2>{{ $house->id }}</h2>
                                <a href="/admin/houses/{{$house->id}}" style="margin:10px" title="دخول" id="enter">
                                    <i class="fa-sharp fa-solid fa-arrow-right"></i>
                                </a>
                                <div>
                                    <a href="javascript:{}"  title="تعديل" onclick="openForm('{{ $house->id }}')">
                                        <i class="fa-solid fa-pencil fa-fw"></i>

                                    </a>
                                    <a  href="javascript:{}" onclick="confirm('{{ $house->id }}d')" title="حذف">
                                        <i class="fa-solid fa-trash fa-fw"></i>
                                    </a>
                                <form id="{{ $house->id }}d" method="POST" action='{{ route('admin.houses.destroy',$house->id) }}' style="all: revert">
                                @csrf
                                @method('DELETE')   
                               </form>
                                </div>
 
                            </div>
                        </div>
                    </div>
               
                    @endforeach

                    <div class="container">
                        <div class="card-add">

                            <div class="contentBx-add">
                                <h2>إضافة سكن</h2>
                                <a href="javascript:{}" style="margin:10px" title="إضافة سكن" id="enter" onclick="openForm('add-form')" >
                                    <i class="fa-solid fa-plus "></i>
                                </a>
                              
                            </div>
                        </div>
                    </div>



                </div>


            </div>
            
        </div>
    </div>
    @foreach  ($houses as $house)
    <form method="POST" action="{{route('admin.houses.update',$house->id)  }}"   style="display: none" class="add-form popupForm" id="{{$house->id}}">
        @csrf
        @method('PUT')
        <h3>تعديل سكن</h3>
        <button class="closeModal" onclick="colseForm('{{$house->id}}')" type="button"> </button>
        <input type="text" placeholder="اسم السكن"  name="house_id" required value="{{$house->id}}">

        <select class="gender" name="gender" value="{{ $house->gender }}"> 
            <option value="male" @if ( $house->gender=="male")
                selected              
            @endif>ذكر</option>
            <option value="female"@if ( $house->gender=="female")
                selected              
            @endif>أنثى</option>
        </select>
        <button class="add-button" type="submit">تعديل</button>

    </form>
    @endforeach
    <form method="POST" action="{{ route('admin.houses.store') }}"  id="add-form" style="display: none" class="add-form popupForm">
        @csrf
        <h3> إضافة سكن</h3>
        <button class="closeModal" onclick="colseForm('add-form')" type="button"> </button>
  
        <input type="text" placeholder="اسم السكن"  name="house_id" required >

        <select class="gender" name="gender"> 
            <option value="" selected hidden>اختر جنس السكن </option>
            <option value="male">ذكر</option>
            <option value="female">أنثى</option>
        </select>
        <button class="add-button" type="submit"> اضافة</button>

    </form>
    <script type="text/javascript">
    function openForm(id){
        document.getElementById(id).style.display = "block";
        document.body.id='popedup';
    }
    function colseForm(id){
      document.getElementById(id).style.display = "none";
        document.body.id='';
    }
    function confirm(house_id){
  var form = document.getElementById(house_id);
  Swal.fire({
      title: "حذف هذ السكن",
      text: " لايمكنك التراجع",
      background:"var(--black-color)",
              color:"var(--white-color)",
      showCancelButton: true,
      confirmButtonColor: "var(--red-color)",
      confirmButtonText: "تم", 
      cancelButtonText: 'أغلاق',
      closeOnConfirm: false
  }).then((result) => {
  /* Read more about isConfirmed, isDenied below */
  if (result.isConfirmed) {
   form.submit();
  } 
});
return false;
}
    
      </script>
@include('sweetalert::alert');
</body>