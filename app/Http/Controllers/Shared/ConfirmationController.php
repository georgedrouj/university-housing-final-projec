<?php

namespace App\Http\Controllers\Shared;
use App\Http\Controllers\Controller;
use App\Models\Contract;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use PSpell\Config as PSpellConfig;

class ConfirmationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('date')->except(['index','update','setDate']);

    }

    public function index()
    {
        $reqs=Contract::where(['contract_state','=','under_confirmation'])->get();
        return view('manager.confirmation',compact('reqs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Contract $oldContract)
    {
        $student_id=Auth()->user()->user_id;
     if($oldContract->contract_state=='active_now')
     {
        $newContract=new Contract();
        $newContract->student_id=$student_id;
        $newContract->room_id=$oldContract->room_id;
        $newContract->room_entery_date=null;
        $newContract->contract_state='under_confirmation';
        $newContract->save();
        return redirect()->back()->with('success','تم ارسال طلب التثبيت الرجاء الانتظار حتى الموافقة عليه');
     }
     else
     {
        return redirect()->back()->with('error','لا يحق لك التثبيت');
     }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  Contract $newContract, Contract $oldContract)
    {
        $acc=$request->input('ok');
        if($acc==true){
            $oldContract->update([
                'contract_state' => 'deleted',
               'room_checkout_date'=>Carbon::now()
        ]);
        $newContract->update([
            'contract_state' => 'active_now_confirmation',
            'room_entery_date'=>Carbon::now()
         ]);
        }
        else{
            $newContract->update([
                'contract_state' => 'rejected'
            ]);
        } 
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function setDate(Request $request)
    {
        $start_date2=$request->input('date1');
    
        $end_date=$request->input('date2');
        Config::set('global.start_date',$start_date2);
        Config::set('global.end_date',$end_date);
        dd($start_date2);
        return redirect()->back();
    }
}
