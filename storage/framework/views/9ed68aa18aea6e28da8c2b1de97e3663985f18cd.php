<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- framework css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/framework.css')); ?> ">
    <!-- main template css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="<?php echo e(asset('css/normalize.css')); ?>">
    <!-- font awesome library -->
    <link rel="stylesheet" href="<?php echo e(asset('css/all.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/rooms.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/create-room.css')); ?>">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <?php echo \Livewire\Livewire::styles(); ?>

    <script src="<?php echo e(asset('vendor/sweetalert/sweetalert.all.js')); ?>"></script>
    <title>جامعة القلمون الخاصة</title>
</head>

<body>

    <div class="page d-flex  ">
        <div class="sidebar  p-20 p-relative">
            <h3 class="p-relative txt-c mt-0">
                النبراس
            </h3>
            <ul>
                <li>
                    <a class="d-flex align-center fs-14  rad-6 p-10 " href="/">
                        <i class="fa-solid fa-sliders fa-fw"></i>
                        <span>الصفحة الرئيسية</span>
                    </a>
                </li>
                <li>
                    <a class="active d-flex align-center fs-14  rad-6 p-10 " href="/admin/houses">
                        <i class="fa-solid fa-hotel fa-fw"></i>
                        <span>إدارة السكنات </span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/admin/user">

                        <i class="fa-solid fa-users-gear fa-fw"></i>
                        <span> إدارة المستخدمين </span>
                    </a>
                    <li>
                       
                        <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">
    
                            <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                            <span>  تسجيل خروج</span>
                        </a>
                        <form method="POST" action="<?php echo e(route('logout')); ?>" id="my_form" style="display: none">
                            <?php echo csrf_field(); ?>
                        </form>
                    </li>

            </ul>

        </div>
        <div class="content ">
            <!-- start head -->
            <!-- end head -->
            <h1 class="p-relative">إدارة السكن</h1>
  
         
            <?php
if (! isset($_instance)) {
    $html = \Livewire\Livewire::mount('rooms', [])->html();
} elseif ($_instance->childHasBeenRendered('bSt4Plz')) {
    $componentId = $_instance->getRenderedChildComponentId('bSt4Plz');
    $componentTag = $_instance->getRenderedChildComponentTagName('bSt4Plz');
    $html = \Livewire\Livewire::dummyMount($componentId, $componentTag);
    $_instance->preserveRenderedChild('bSt4Plz');
} else {
    $response = \Livewire\Livewire::mount('rooms', []);
    $html = $response->html();
    $_instance->logRenderedChild('bSt4Plz', $response->id(), \Livewire\Livewire::getRootElementTagName($html));
}
echo $html;
?>



        </div>
     
      
    </div>
   
</div>
<?php $__currentLoopData = $rooms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $room): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<form method="POST" action="/admin/rooms/<?php echo e($room->id); ?>" id="<?php echo e($room->id); ?>" class="popupForm add-room" style="display: none">
    <?php echo csrf_field(); ?>
    <?php echo method_field('PUT'); ?>
    <h3> تعديل غرفة</h3>
    <button class="closeModal" type="button" onclick="colseForm('<?php echo e($room->id); ?>')"> </button>
    <input type="text" placeholder="أدخل رقم الغرفة:" required name="name" value="<?php echo e($room->room_name); ?>">
    <select class="direction" name="direction"> 
        <option value="شمالي" <?php if($room->direction=="شمالي"): ?> selected <?php endif; ?>>شمالي</option>
        <option value="شرقي" <?php if($room->direction=="شرقي"): ?> selected <?php endif; ?>>شرقي</option>
        <option value="جنوبي" <?php if($room->direction=="جنوبي"): ?> selected <?php endif; ?>>جنوبي</option>
        <option value="غربي" <?php if($room->direction=="غربي"): ?> selected <?php endif; ?>>غربي</option>
        <option value="شمالي-شرقي" <?php if($room->direction=="شمالي-شرقي"): ?> selected <?php endif; ?>>الشمال الشرقي</option>
        <option value="جنوبي-غربي" <?php if($room->direction=="جنوبي-غربي"): ?> selected <?php endif; ?>>الجنوب الغربي</option>
        <option value="جنوبي-شرقي" <?php if($room->direction=="جنوبي-شرقي"): ?> selected <?php endif; ?>>الجنوب الشرقي</option>
        <option value="شمالي-غربي" <?php if($room->direction=="شمالي-غربي"): ?> selected <?php endif; ?>>الشمال الغربي</option>
        <option value="شمالي-جنوبي" <?php if($room->direction=="شمالي-جنوبي"): ?> selected <?php endif; ?>>الشمال الجنوبي </option>
        <option value="شرقي-غربي" <?php if($room->direction=="شرقي-جنوبي"): ?> selected <?php endif; ?>>الشرق الغربي</option>
        <option value="شمالي-شرقي-جنوبي" <?php if($room->direction=="شمالي-شرقي-جنوبي"): ?> selected <?php endif; ?>>الشمال الشرقي الجنوبي</option>
        <option value="شمالي-غربي-جنوبي" <?php if($room->direction=="شمالي-غربي-جتوبي"): ?> selected <?php endif; ?>>الشمال الغربي الجنوبي</option>
        <option value="شرقي-شمالي-غربي" <?php if($room->direction=="شرقي-شمالي-غربي"): ?> selected <?php endif; ?>>الشرق الشمالي الغربي</option>
        <option value="شرقي-جنوبي-غربي" <?php if($room->direction=="شرقي-جنوبي-غربي"): ?> selected <?php endif; ?>>الشرق الجنوبي الغربي</option>
        <option value="شمالي-جنوبي-شرقي-غربي" <?php if($room->direction=="شمالي-جنوبي-شرقي-غربي"): ?> selected <?php endif; ?>>الشمال الشرقي الجنوبي الغربي</option>
    </select>
    <input type="number" min="1" step="1" placeholder="أدخل سعة الغرفة:" name="capacity" value="<?php echo e($room->capacity); ?>">
    <input type="text" placeholder="أدخل رقم الطابق:" required name="floor" value="<?php echo e($room->floor_name); ?>">
    <input type="text" placeholder="أدخل مساحة الغرفة:" required name="space" value="<?php echo e($room->space); ?>">
    <input type="number" min="1" step="any" placeholder="أدخل سعر الغرفة:" required name="price" value="<?php echo e($room->price); ?>">
    <button class="add_room_button"> تعديل</button>


</form>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<form method="POST" action="/admin/houses/<?php echo e(last(request()->segments())); ?>" class="popupForm add-room" id="add-form" style="display: none">
    <?php echo csrf_field(); ?>
    <h3> إضافة غرفة</h3>
    <button class="closeModal"  type="button" onclick="colseForm('add-form')"> </button>
    <input type="text" placeholder="أدخل رقم الغرفة:" required name="name">
    <select class="direction" name="direction"> 
        <option selected hidden value="اختر اتجاه الغرفة" name="direction">اختر اتجاه الغرفة </option>
        <option value="شمالي">شمالي</option>
        <option value="شرقي">شرقي</option>
        <option value="جنوبي">جنوبي</option>
        <option value="غربي">غربي</option>
        <option value="شمالي-شرقي">الشمال الشرقي</option>
        <option value="جنوبي-غربي">الجنوب الغربي</option>
        <option value="جنوبي-شرقي">الجنوب الشرقي</option>
        <option value="شمالي-غربي">الشمال الغربي</option>
        <option value="شمالي-جنوبي">الشمال الجنوبي </option>
        <option value="شرقي-غربي">الشرق الغربي</option>
        <option value="شمالي-شرقي-جنوبي">الشمال الشرقي الجنوبي</option>
        <option value="شمالي-غربي-جنوبي">الشمال الغربي الجنوبي</option>
        <option value="شرقي-شمالي-غربي">الشرق الشمالي الغربي</option>
        <option value="شرقي-جنوبي-غربي">الشرق الجنوبي الغربي</option>
        <option value="شمالي-جنوبي-شرقي-غربي">الشمال الشرقي الجنوبي الغربي</option>
    </select>
    <input type="number" min="1" step="1" placeholder="أدخل سعة الغرفة:" name="capacity" >
    <input type="text" placeholder="أدخل رقم الطابق:" required name="floor">
    <input type="text" placeholder="أدخل مساحة الغرفة:" required name="space">
    <input type="number" min="1" step="any" placeholder="أدخل سعر الغرفة:" required name="price">
    <button class="add_room_button"> إضافة </button>

</form>
<script type="text/javascript">
    function openForm(id){
        document.getElementById(id).style.display = "block";
        document.body.id='popedup';
    }
    function colseForm(id){
      document.getElementById(id).style.display = "none";
        document.body.id='';
    }
    function confirm(room_id){
  var form = document.getElementById(room_id);
  Swal.fire({
      title: "حذف هذه الغرفة",
      text: " لايمكنك التراجع",
      background:"var(--black-color)",
              color:"var(--white-color)",
      showCancelButton: true,
      confirmButtonColor: "var(--red-color)",
      confirmButtonText: "تم", 
      cancelButtonText: 'أغلاق',
      closeOnConfirm: false
  }).then((result) => {
  /* Read more about isConfirmed, isDenied below */
  if (result.isConfirmed) {
   form.submit();
  } 
});
return false;
}
    
      </script>
<?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>;
<?php echo \Livewire\Livewire::scripts(); ?>

</body><?php /**PATH G:\Programing\universtyHousing\resources\views/admin/rooms.blade.php ENDPATH**/ ?>