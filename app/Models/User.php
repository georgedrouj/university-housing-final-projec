<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    use SoftDeletes;
    protected $fillable = [
        'name',
        'user_id',
        'password',
        'type',
        'gender',
    ];
    protected $hidden = [
        'password',
    ];
    protected $appends=[
        'house_id',
    ];
    public function getHouseIdAttribute(){
        return $this->getSupervisors()->first()?->house_id;
    }
    public function getSupervisors(){
        return $this->hasMany(Supervisor::class,'user_id');
    }
  
}
