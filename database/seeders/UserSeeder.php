<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'user_id' =>'222',
            'password' => Hash::make('222'),
            'gender'=>'male',
            'type'=>'admin',
        ]);
        DB::table('users')->insert([
            'name' => 'manager',
            'user_id' =>'333',
            'password' => Hash::make('333'),
            'gender'=>'male',
            'type'=>'manager',
        ]);
        DB::table('users')->insert([
            'name' => 'supervisor',
            'user_id' =>'444',
            'password' => Hash::make('444'),
            'gender'=>'male',
            'type'=>'supervisor',
        ]);
        DB::table('users')->insert([
            'name' => 'أحمد',
            'user_id' =>'777',
            'password' => Hash::make('777'),
            'gender'=>'male',
            'type'=>'supervisor',
        ]);
        DB::table('users')->insert([
            'name' => 'أمل',
            'user_id' =>'555',
            'password' => Hash::make('555'),
            'gender'=>'female',
            'type'=>'supervisor',
        ]);
        DB::table('users')->insert([
            'name' => 'هناء',
            'user_id' =>'666',
            'password' => Hash::make('666'),
            'gender'=>'female',
            'type'=>'supervisor',
        ]);
    }
}
