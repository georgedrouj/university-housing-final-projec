<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;

class Room extends Model
{
    use HasFactory;
    use SoftDeletes;
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;
    protected $table='rooms';
    protected $fillable=['name','capacity','price','floor_name','direction','space','blocked_now','blocked_reason','house_id'];
    protected $appends =['emp'];
    protected $primaryKey='id';
    public function getContracts(){
        return $this->hasMany(Contract::class);
    }
    public function getHouse(){
        return $this->belongsTo(House::class,'house_id');
    }
    public function getMaintenaces(){
        return $this->hasManyThrough(Maintenance::class,Contract::class,'contract_id','id');
    }
    public function getQuittances(){
        return $this->hasManyThrough(Quittance::class,Contract::class);
    }
    
    public function getSwitchRoom(){
        return $this->hasManyThrough(SwitchRoom::class,Contract::class);
    }
    public function getSwitchRoomMe(){
        return $this->hasMany(SwitchRoom::class,'new_room_id');
    }
    public function getEmpty(){
        return $this->capacity - ($this->hasMany(Contract::class,'room_id')->where(function ($query) {
            $query->where('contract_state', '=', 'active_now')
                  ->orWhere('contract_state', '=', 'in_progress');
        })->count())-($this->getSwitchRoomMe()->where(function ($query) {
            $query->where('switch_room_state', '=', 'in_supervisor')
                  ->orWhere('switch_room_state', '=', 'in_manager');
        })->count());
    }
    public function getEmpAttribute(){
        return $this->getEmpty();
    }
  
}
