<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Supervisor;
use App\Models\User;
use Carbon\Carbon;
use GrahamCampbell\ResultType\Success;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=User::where('type','!=','student')->get();
        return view('admin.users')->with('users',$users);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $user=new User();
        $user_id = User::latest()->first()->id;
        $user_id++;
        $u="HOM".Carbon::now()->format('Y').$user_id;
        $user->password=Hash::make($u);
        $user->user_id=$u;
        $user->name=$request->input('name');
        $user->type=$request->input('type');
        $user->gender=$request->input('gender');
        $user->save();
        if($request->input('type')=="supervisor"){
            $supervisor =new Supervisor();
            $supervisor->user_id=$user->id;
            $supervisor->save();
        }
        return redirect()->back()->with('success',$u);


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.user.edit')->with('user',$user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        if($user->type=="supervisor"&&$request->input('type')!="supervisor"){
            $supervisor=Supervisor::where('user_id','=',$user->id);
            $supervisor->delete();
        }
        if($user->type!="supervisor"&&$request->input('type')=="supervisor"){
            $supervisor =new Supervisor();
            $supervisor->user_id=$user->id;
            $supervisor->save();
        }
       $user->update([    
       'name' => $request->input('name'),
       'type'=> $request->input('type'),
       'gender'=> $request->input('gender')
    ]);
    return redirect()->back()->with('success','تم تعديل بيانات المستخدم بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->back()->with('success','تم حذف المستخدم بنجاح');

    }
}

