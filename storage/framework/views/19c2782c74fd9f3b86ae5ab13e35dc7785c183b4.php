<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo e(asset('css/framework.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="<?php echo e(asset('css/normalize.css')); ?>">
    <!-- font awesome library -->
    <link rel="stylesheet" href="<?php echo e(asset('css/all.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/rooms.css')); ?>">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <title>جامعة القلمون الخاصة</title>
</head>

<body>

    <div class="page d-flex  ">
        <div class="sidebar  p-20 p-relative">
            <div class="logo">
                <img src="<?php echo e(asset('images/homoon.png')); ?>" alt="">          
          </div>
            <ul>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/">
                        <i class="fa-solid fa-sliders fa-fw"></i>
                        <span>الصفحة الرئيسية</span>
                    </a>
                </li>
                <li>
                    <a class="active d-flex align-center fs-14  rad-6 p-10 " href="/manager/houses">
                        <i class="fa-solid fa-hotel fa-fw"></i>
                        <span>السكنات </span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/import">

                        <i class="fa-solid fa-people-line fa-fw"></i>
                        <span> الطلاب</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="orders.html">

                        <i class="fa-brands fa-readme fa-fw"></i>
                        <span> طلب التثبيت</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/contract">

                        <i class="fa-brands fa-readme fa-fw"></i>
                        <span> طلب التسجيل</span>
                    </a>
                </li>
                <li>
                    <a class="d-flex align-center fs-14  rad-6 p-10 " href="/manager/switch_room">

                        <i class="fa-brands fa-readme fa-fw"></i>
                        <span> طلب التبديل</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="<?php echo e(route('manager.quittance.index')); ?>">

                        <i class="fa-brands fa-readme fa-fw"></i>
                        <span> طلب براءة الذمة</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="orders.html">

                        <i class="fa-brands fa-readme fa-fw"></i>
                        <span> طلب  الصيانة</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="">

                        <i class="fa-solid fa-users"></i>
                        <span>مشرفي الوحدات السكنية</span>
                    </a>
                </li>
                <li>
                    <form method="POST" action="<?php echo e(route('logout')); ?>" id="my_form">
                    <?php echo csrf_field(); ?>
                <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">

                    <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                    <span>  تسجيل خروج</span>
                </a>
                </form>
                </li>

            </ul>

        </div>
        <div class="content ">
            <!-- start head -->
            <!-- end head -->
            <form method="GET" action="/manager/houses/<?php echo e(last(request()->segments())); ?>">
                <button id="search"  type="submit">بحث </button>
                <input type="search" name="room-search" placeholder="بحث">
            </form>
            <h1 class="p-relative">الغرف</h1>
            
            <div class="wrapper d-flex gap-20 ">
                <!-- <div class="welcome bg-white rad-10 "> -->
                <div class="intro p-20   bg-eee">
                    <?php $__currentLoopData = $rooms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $room): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        
                  
                    <div id="container">
                        
                        <div class="product">

                            <div class="info">
                                <h2> <?php echo e($room->room_name); ?></h2>
                                <ul>
                                    <li><strong>المساحة : </strong><?php echo e($room->space); ?> </li>
                                    <li><strong>السعة : </strong><?php echo e($room->capacity); ?></li>
                                    <li><strong>الشواغر: </strong> <?php echo e($room->getEmpty()); ?></li>
                                    <li><strong>الاتجاه: </strong><?php echo e($room->direction); ?></li>
                                    <li><strong>السعر: </strong> <?php echo e($room->price); ?></li>

                                </ul>
                                <?php if($room->blocked_now==0): ?>
                                <form method="POST">
                                <button class="btn" onclick="openForm()"> <a href="/manager/houses/<?php echo e(last(request()->segments())); ?>/<?php echo e($room->id); ?>/block">حجب </a>
                                </button>
                            </form>
                                <?php else: ?>
                                <form method="POST" action="/manager/houses/<?php echo e($room->house_id); ?>/unblocked/<?php echo e($room->id); ?>" id="yes">
                                    <?php echo csrf_field(); ?>
                            <?php echo method_field('PUT'); ?>
                 
                                <button class="btn" type="submit"> <a  href="javascript:{}" onclick="">إلغاء الحجب </a>
                                </button>
                                </form>
                                <?php endif; ?>
                                <button class="btn"> <a href="">عقود </a>
                                </button>
                                <button class="btn" id="button"> <a href="<?php echo e(request()->segment(count(request()->segments()))); ?>/<?php echo e($room->id); ?>"> تسجيل</a>
                                </button>
                            </div>
                        </div>

                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  
               
                </div>



            </div>

          
        </div>
      
    </div>
 
    <?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>;
    <script type="text/javascript">
        function openForm()
        {
            document.getElementById("popupForm").style.display="block";
            document.body.id='popedup';
        }
        function closeForm()
        {
            document.getElementById("popupForm").style.display="none";
            document.body.id='';
        }

        </script>
            <script type="text/javascript">
                function confirm(){
              
              var form = document.getElementById("yes");
              swal({
                  title: "هل انت متأكد",
                  text: " لايمكنك التراجع",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "green",
                  confirmButtonText: "موافق", 
                  closeOnConfirm: false
              }, function (isConfirmed) {
                  if (isConfirmed) {
                    console.log('hi');
                      form.submit();
                  }
              });
            return false;
            }
</body>
<?php /**PATH C:\Users\User\Desktop\university-housing-final-projec\resources\views/manager/room.blade.php ENDPATH**/ ?>