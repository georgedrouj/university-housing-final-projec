<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('switch_rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('new_room_id');
            $table->string('switch_room_state');
            $table->string('electricity')->nullable();
            $table->string('missings')->nullable();
            $table->string('manager_comment')->nullable();
            $table->unsignedInteger('contract_id');
            $table->foreign('contract_id')
            ->references('id')
            ->on('contracts')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->foreign('new_room_id')
            ->references('id')
            ->on('rooms')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('switch_rooms');
    }
};
