<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('student_id');
            $table->unsignedInteger('room_id');
            $table->string('reject_reason')->nullable();
            $table->date('room_entery_date')->nullable();
            $table->date('room_checkout_date')->nullable();
            $table->string('contract_state');
            $table->foreign('student_id')
            ->references('university_id')
            ->on('students')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->foreign('room_id')
            ->references('id')
            ->on('rooms')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
};
