<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- framework css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/framework.css')); ?> ">
    <!-- main template css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="<?php echo e(asset('css/normalize.css')); ?>">
    <!-- font awesome library -->
    <link rel="stylesheet" href="<?php echo e(asset('css/all.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/rooms.css')); ?>">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">

    <title>جامعة القلمون الخاصة</title>
</head>

<body>

    <div class="page d-flex  ">
        <div class="sidebar  p-20 p-relative">
            <h3 class="p-relative txt-c mt-0">
                النبراس
            </h3>
            <ul>
                <li>
                    <a class="d-flex align-center fs-14  rad-6 p-10 " href="/">
                        <i class="fa-solid fa-sliders fa-fw"></i>
                        <span>الصفحة الرئيسية</span>
                    </a>
                </li>
                <li>
                    <a class=" active d-flex align-center fs-14  rad-6 p-10 " href="/admin/house">
                        <i class="fa-solid fa-hotel fa-fw"></i>
                        <span>إدارة السكنات</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/admin/user">

                        <i class="fa-solid fa-users-gear fa-fw"></i>
                        <span> إدارة المستخدمين </span>
                    </a>
                </li>
                <li>
                    <form method="POST" action="<?php echo e(route('logout')); ?>" id="my_form">
                        <?php echo csrf_field(); ?>
                    <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">

                        <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                        <span>  تسجيل خروج</span>
                    </a>
                    </form>
                </li>
            </ul>

        </div>
        <div class="content ">
            <!-- start head -->
            <!-- end head -->
            <h1 class="p-relative">إدارة السكن</h1>
            <form method="GET" action="">
                <div class="search">
                    <input type="search" name="search" id="search" class="form-control" placeholder="أدخل رقم الغرفة" >
                <input type="submit" value="بحث">
                </div>
            </form>
            <div class="wrapper d-flex gap-20 ">
                <!-- <div class="welcome bg-white rad-10 "> -->
                <div class="intro p-20   bg-eee" id="alldata">
               
             <?php if($rooms): ?>
                <?php $__currentLoopData = $rooms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $room): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div id="container"> 
                            <div class="product">
                                <div class="info">
                                    <h2> <?php echo e($room->room_name); ?></h2>
                                    <ul>
                                        <li><strong>المساحة : </strong><?php echo e($room->space); ?> </li>
                                        <li><strong>السعة : </strong><?php echo e($room->capacity); ?></li>
                                        <li><strong>الشواغر: </strong> <?php echo e($room->getEmpty()); ?></li>
                                        <li><strong>الاتجاه: </strong><?php echo e($room->direction); ?></li>
                                        <li><strong>السعر: </strong> <?php echo e($room->price); ?></li>
    
                                    </ul>

                                <button class="btn" title="تعديل"> <a href="/admin/room/{room->id}/edit"><i class="fa-solid fa-pen"></i> </a>
                                </button>
                                <button class="btn" id="button" title="حذف"> <a  href="javascript:{}" onclick="document.getElementById('delete-room').submit();"><i class="fa-solid fa-trash"></i></a>
                                    <form id="delete-room" method="POST" action='{' style="all: revert">
                                        <?php echo csrf_field(); ?>
                                        <?php echo method_field('DELETE'); ?>
                                         
                                       </form>
                                </button>
                            </div>
                        </div>
                        </div>
                   
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                 </div>
                  <?php else: ?>
                   <div> لا توجد غرفة بهذا الرقم</div>
                   <?php endif; ?>
            </div>
           
                       
                     
                     


                

            </div>



        </div>

      
    </div>
  

<?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>;
<script type="text/javascript">
    function confirm(){
      
        var form = document.getElementById('delete-room');
        swal({
            title: "هل تريد الحذف",
            text: "لا يمكنك الإلغاء  بعد التأكيد",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "orange",
            confirmButtonText: "تسجيل", 
            closeOnConfirm: false
        }, function (isConfirmed) {
            if (isConfirmed) {
                form.submit();
            }
        });
    return false;
    }
    </script>
    
        <script type="text/javascript">
            $.ajaxSetup({ headers: { 'csrftoken' : '<?php echo e(csrf_token()); ?>' } });
            </script>
</body><?php /**PATH G:\Programing\universtyHousing\resources\views/admin/room/search.blade.php ENDPATH**/ ?>