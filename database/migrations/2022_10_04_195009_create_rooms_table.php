<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('room_name');
            $table->integer('capacity');
            $table->string('house_id');
            $table->string('direction');
            $table->string('floor_name');
            $table->string('blocked_reason')->nullable();
            $table->boolean('blocked_now')->default('0');
            $table->double('price');
            $table->double('space');
            $table->foreign('house_id')
            ->references('id')
            ->on('houses')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
};
