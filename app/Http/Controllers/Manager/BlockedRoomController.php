<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Models\Room;
use Illuminate\Http\Request;

class BlockedRoomController extends Controller
{
    public function blockRoom(Request $request,$room_id)
    {
        $room=Room::find($room_id);
        $room->update([
            'blocked_now' =>true,
            'blocked_reason' => $request->input('block_reason')
     ]);
    return redirect()->back()->with('success','تم حجب الغرفة بنجاح');
    }
    public function UnBlockedRoom($room_id)
    {
        $room=Room::find($room_id);
        $room->update([
            'blocked_now' =>false,
            'blocked_reason' => null
     ]);
     return redirect()->back()->with('success','تم إلغاء حجب الغرفة بنجاح');
    }
}

