<div>
    <input wire:model.debounce.500ms="searchQuery" type="text" placeholder="بحث" >
    <select wire:model="houseQuery" style="background-color: var(--black-color); color:var(--white-color); text-align-last:center;border: none;">
        <option value="">الكل</option>
        <option value="without house">بدون سكن</option>
        @foreach ($maleHouses as $male)
        <option value="{{ $male->id }}">{{ $male->id }}</option>
        @endforeach
        @foreach ($femaleHouses as $female)

        <option value="{{ $female->id }}">{{ $female->id }}</option>
        @endforeach
    </select>
    <div class="container">
        <table>
            <thead>
                <tr>
                    <th> رقم المشرف</th>
                    <th> اسم المشرف</th>
                    <th> رقم السكن</th>
                </tr>
            </thead>
            
            <tbody>
                @foreach ($supervisors as $supervisor)
               
                <tr>
                    <td> {{ $supervisor->user_id }}</td>
                    <td> {{ $supervisor->name }}</td>
                 
                    <td>  
                     <form  method="POST" action="/manager/supervisor/{{$supervisor->id}}" id="{{ $supervisor->id }}" >
                            @method('PUT')
                            @csrf
                        <select  name="house_id" onchange="StoreSupervisor('{{ $supervisor->id }}')" style="background-color: var(--black-color); color:var(--white-color); text-align-last:center;border: none;">
                            <option value="">بدون سكن</option>
                            @if($supervisor->gender=="female")
                            @foreach ($femaleHouses as $female)

                            <option value="{{ $female->id }}" @if ($supervisor->house_id==$female->id)
                                selected
                            @endif>{{ $female->id }}</option>
                            @endforeach
                            @else
                            @foreach ($maleHouses as $male)
                            <option value="{{ $male->id }}" @if ($supervisor->house_id==$male->id)
                                selected
                            @endif>{{ $male->id }}</option>
                            @endforeach
                            @endif
                        </select>
                    </form>
                        
                        
                    </td>
                   
                    
                  
                </tr>
           
                @endforeach
            </tbody>
           
        </table>
    </div>
    
</div>
