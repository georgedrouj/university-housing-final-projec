<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rooms')->insert([
            'room_name' => '001',
            'capacity' =>'2',
            'floor_name' => 'FIRST',
            'direction'=>'شمالي غربي',
            'blocked_now'=>false,
            'space'=>'16.5',
            'price'=>'2000000',
            'house_id'=>'G1',
        
        ]);
        DB::table('rooms')->insert([
            'room_name' => '001',
            'capacity' =>'2',
            'floor_name' => 'SECOND',
            'direction'=>'غربي',
            'blocked_now'=>false,
            'space'=>'25',
            'price'=>'1000000',
            'house_id'=>'G2',
        
        ]);
        DB::table('rooms')->insert([
            'room_name' => '002',
            'capacity' =>'1',
            'floor_name' => 'THIRD',
            'direction'=>'شمالي',
            'blocked_now'=>false,
            'space'=>'25',
            'price'=>'2500000',
            'house_id'=>'G1',
        
        ]);
        DB::table('rooms')->insert([
            'room_name' => '122',
            'capacity' =>'1',
            'floor_name' => 'FIRST',
            'direction'=>'جنوبي',
            'blocked_now'=>false,
            'space'=>'9',
            'price'=>'3500000',
            'house_id'=>'G1',
        
        ]);
        
        DB::table('rooms')->insert([
            'room_name' => '002',
            'capacity' =>'1',
            'floor_name' => 'SECOND',
            'direction'=>'غربي',
            'blocked_now'=>false,
            'space'=>'16.5',
            'price'=>'2300000',
            'house_id'=>'G2',
        
        ]);
        DB::table('rooms')->insert([
            'room_name' => '315',
            'capacity' =>'2',
            'floor_name' => 'SECOND',
            'direction'=>'شرقي غربي',
            'blocked_now'=>false,
            'space'=>'16.5',
            'price'=>'5000000',
            'house_id'=>'G2',
        
        ]);
        DB::table('rooms')->insert([
            'room_name' => '006',
            'capacity' =>'1',
            'floor_name' => 'SECOND',
            'direction'=>'شرقي',
            'blocked_now'=>false,
            'space'=>'15.75',
            'price'=>'7000000',
            'house_id'=>'B1',
        
        ]);
        DB::table('rooms')->insert([
            'room_name' => '210',
            'capacity' =>'2',
            'floor_name' => 'FIRST',
            'direction'=>'شرقي',
            'blocked_now'=>false,
            'space'=>'16.5',
            'price'=>'6000000',
            'house_id'=>'B1',
        
        ]);
        DB::table('rooms')->insert([
            'room_name' => '100',
            'capacity' =>'1',
            'floor_name' => 'THIRD',
            'direction'=>'شمالي',
            'blocked_now'=>false,
            'space'=>'16.5',
            'price'=>'6500000',
            'house_id'=>'B2',
        
        ]);
        DB::table('rooms')->insert([
            'room_name' => '319',
            'capacity' =>'2',
            'floor_name' => 'FIRST',
            'direction'=>'جنوبي شرقي',
            'blocked_now'=>false,
            'space'=>'16.5',
            'price'=>'7000000',
            'house_id'=>'B2',
        
        ]);
        DB::table('rooms')->insert([
            'room_name' => '100',
            'capacity' =>'1',
            'floor_name' => 'SECOND',
            'direction'=>'غربي  شرقي',
            'blocked_now'=>false,
            'space'=>'9.75',
            'price'=>'3000000',
            'house_id'=>'G3',
        
        ]);
        DB::table('rooms')->insert([
            'room_name' => '306',
            'capacity' =>'3',
            'floor_name' => 'THIRD',
            'direction'=>'شمالي',
            'blocked_now'=>false,
            'space'=>'25',
            'price'=>'8000000',
            'house_id'=>'G3',
        
        ]);
        DB::table('rooms')->insert([
            'room_name' => '125',
            'capacity' =>'2',
            'floor_name' => 'SECOND',
            'direction'=>'جنوبي شرقي',
            'blocked_now'=>false,
            'space'=>'16.5',
            'price'=>'2500000',
            'house_id'=>'G3',
        
        ]);
        DB::table('rooms')->insert([
            'room_name' => '110',
            'capacity' =>'1',
            'floor_name' => 'FIRST',
            'direction'=>'غربي ',
            'blocked_now'=>false,
            'space'=>'9',
            'price'=>'7500000',
            'house_id'=>'B3',
        
        ]);
        DB::table('rooms')->insert([
            'room_name' => '306',
            'capacity' =>'3',
            'floor_name' => 'THIRD',
            'direction'=>'جنوبي ',
            'blocked_now'=>false,
            'space'=>'25.75',
            'price'=>'600000',
            'house_id'=>'B3',
        
        ]);
        DB::table('rooms')->insert([
            'room_name' => '315',
            'capacity' =>'2',
            'floor_name' => 'THIRD',
            'direction'=>'جنوبي شرقي',
            'blocked_now'=>false,
            'space'=>'16.5',
            'price'=>'9000000',
            'house_id'=>'B3',
        
        ]);
        DB::table('rooms')->insert([
            'room_name' => '215',
            'capacity' =>'2',
            'floor_name' => 'SECOND',
            'direction'=>' شرقي',
            'blocked_now'=>false,
            'space'=>'16.5',
            'price'=>'3000000',
            'house_id'=>'B4',
        
        ]);
        DB::table('rooms')->insert([
            'room_name' => '100',
            'capacity' =>'1',
            'floor_name' => 'FIRST',
            'direction'=>' شرقي',
            'blocked_now'=>false,
            'space'=>'9',
            'price'=>'9500000',
            'house_id'=>'B4',
        
        ]);
        DB::table('rooms')->insert([
            'room_name' => '120',
            'capacity' =>'1',
            'floor_name' => 'THIRD',
            'direction'=>' جنوبي',
            'blocked_now'=>false,
            'space'=>'9',
            'price'=>'6500000',
            'house_id'=>'G4',
        
        ]);
        DB::table('rooms')->insert([
            'room_name' => '315',
            'capacity' =>'2',
            'floor_name' => 'FIRST',
            'direction'=>' غربي',
            'blocked_now'=>false,
            'space'=>'16.5',
            'price'=>'230000',
            'house_id'=>'G4',
        
        ]);
        DB::table('rooms')->insert([
            'room_name' => '100',
            'capacity' =>'1',
            'floor_name' => 'FIRST',
            'direction'=>' شمالي',
            'blocked_now'=>false,
            'space'=>'9.25',
            'price'=>'950000',
            'house_id'=>'B5',
        
        ]);
    }
}
