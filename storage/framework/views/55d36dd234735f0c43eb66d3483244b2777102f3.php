<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- framework css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/framework.css')); ?>">
    <!-- main template css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="<?php echo e(asset('css/normalize.css')); ?>">
    <!-- font awesome library -->
    <link rel="stylesheet" href="<?php echo e(asset('css/all.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/houses.css')); ?>">


    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">

    <title>جامعة القلمون الخاصة</title>
</head>

<body>

    <div class="page d-flex  ">
        <div class="sidebar  p-20 p-relative">
            <div class="logo">
                <img src="<?php echo e(asset('images/homoon.png')); ?>" alt="">          
          </div>
            <ul>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/">
                        <i class="fa-solid fa-sliders fa-fw"></i>
                        <span>الصفحة الرئيسية</span>
                    </a>
                </li>
                <li>
                    <a class="active d-flex align-center fs-14  rad-6 p-10 " href="/manager/houses">
                        <i class="fa-solid fa-hotel fa-fw"></i>
                        <span>السكنات </span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/import">

                        <i class="fa-solid fa-people-line fa-fw"></i>
                        <span> الطلاب</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="orders.html">

                        <i class="fa-brands fa-readme fa-fw"></i>
                        <span> طلب التثبيت</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/contract">

                        <i class="fa-brands fa-readme fa-fw"></i>
                        <span> طلب التسجيل</span>
                    </a>
                </li>
                <li>
                    <a class="d-flex align-center fs-14  rad-6 p-10 " href="/manager/switch_room">

                        <i class="fa-brands fa-readme fa-fw"></i>
                        <span> طلب التبديل</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="<?php echo e(route('manager.quittance.index')); ?>">

                        <i class="fa-brands fa-readme fa-fw"></i>
                        <span> طلب براءة الذمة</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="orders.html">

                        <i class="fa-brands fa-readme fa-fw"></i>
                        <span> طلب  الصيانة</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="">

                        <i class="fa-solid fa-users"></i>
                        <span>مشرفي الوحدات السكنية</span>
                    </a>
                </li>
                <li>
                    <form method="POST" action="<?php echo e(route('logout')); ?>" id="my_form">
                        <?php echo csrf_field(); ?>
                    <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">

                        <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                        <span>  تسجيل خروج</span>
                    </a>
                    </form>
                </li>

            </ul>

        </div>


        <div class="content ">
            <!-- start head -->
            <!-- end head -->
            <h1 class="p-relative">السكنات</h1>
            <div class="wrapper d-flex gap-20 ">
                
                <!-- <div class="welcome bg-white rad-10 "> -->
                <div class="intro p-20   bg-eee">
                    <?php $__currentLoopData = $houses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $house): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="container">
                        <div class="card">

                            <div class="contentBx">
                                <h2><?php echo e($house->id); ?></h2>

                                <a href="/manager/houses/<?php echo e($house->id); ?>" style="margin:10px" id="enter">
                                    دخول
                                    
                                </a>


                            </div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>







                </div>


            </div>
        </div>

</body>
<?php /**PATH C:\Users\User\Desktop\university-housing-final-projec\resources\views/manager/hoses.blade.php ENDPATH**/ ?>