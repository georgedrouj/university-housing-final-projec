<div >
<input wire:model.debounce.500ms="searchQuery" type="text" placeholder="بحث" >
<select name="type" wire:model="type" >
    <option value="">all</option>
    <option value="admin">admin</option>
    <option value="manager">manager</option>
    <option value="supervisor">supervisor</option>
</select>

<div class="container">

 
    <table>
        <thead>
            <tr>
                <th> رقم المستخدم</th>
                <th> اسم المستخدم</th>
                <th> دور المستخدم</th>
                <th>جنس المستخدم</th>
                <th> الأحداث</th>


            </tr>
        </thead>
        @foreach ($users as $user)
        <tbody>
            <tr>
                <td> {{ $user->user_id }}</td>
                <td> {{ $user->name }}</td>
                <td>{{ $user->type }} </td>
                <td>{{ $user->gender }}</td>
                <td>
                    <a href="javascript:{}" onclick="openForm('{{ $user->id }}')"> <i class="fa-solid fa-pen fa-fw" style="color:var(--move1-color)"></i> </a>
               
                   <a href="javascript:{}" onclick="confirm('{{ $user->id }}d')"> <i class="fa-solid fa-trash fa-fw" style="color:var(--red-color)"></i></a>
                        <form id="{{ $user->id }}d" method="POST" action='{{ route('admin.user.destroy',$user->id) }}' style="all: revert;display:none">
                            @csrf
                            @method('DELETE')
                      </form>
                    
                </td>

            </tr>
        </tbody>
        @endforeach
    </table>
</div>
</div>