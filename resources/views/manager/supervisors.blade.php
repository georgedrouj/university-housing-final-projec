<!DOCTYPE html>
<html dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- main template css file -->
    
    <link rel="stylesheet" href="{{ asset('css/framework.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <!-- font awesome library -->
    <link rel="stylesheet" href="{{ asset('css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/users.css') }}">
 
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script src="{{ asset('vendor/sweetalert/sweetalert.all.js') }}"></script>
    @livewireStyles
    <title>جامعة القلمون الخاصة</title>
</head>
<body>
    <div class="page d-flex ">
    <div class="sidebar  p-20 p-relative">
        <div class="logo">
            <img src="{{ asset('images/homoon.png') }}" alt="">          
      </div>
        <ul>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/">
                    <i class="fa-solid fa-sliders fa-fw " ></i>
                    <span>الصفحة الرئيسية</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/houses">
                    <i class="fa-solid fa-hotel fa-fw"></i>
                    <span>السكنات </span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/students">

                    <i class="fa-solid fa-people-line fa-fw"></i>
                    <span> الطلاب</span>
                </a>
            </li>
            <li>
                <a class="acitve d-flex align-center fs-14  rad-6 p-10 " href="/manager/supervisor">

                    <i class="fa-solid fa-users"></i>
                    <span>مشرفي الوحدات السكنية</span>
                </a>
            </li>
        
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/contract">

                    <i class="fa-brands fa-readme fa-fw"></i>
                    <span> طلب التسجيل</span>
                </a>
            </li>
           
            <li>
                <a class="d-flex align-center fs-14  rad-6 p-10 " href="/manager/switch_room">

                    <i class="fa-solid fa-shuffle"></i>
                    <span> طلب التبديل</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/quittance">

                    <i class="fa-solid fa-person-walking-arrow-right"></i>
                    <span> طلب براءة الذمة</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/maintenance">

                    <i class="fa-sharp fa-solid fa-wrench"></i>
                    <span> طلب  الصيانة</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="comfirmation.html">

                    <i class="fa-solid fa-circle-check"></i>
                    <span> طلب التثبيت</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/advertisments">

                    <i class="fa-solid fa-bullhorn"></i>
                    <span> الإعلانات </span>
                </a>
            </li>
            <li>
              
                <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">

                    <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                    <span>  تسجيل خروج</span>
                </a>
                <form method="POST" action="{{ route('logout') }}" id="my_form" style="display: none">
                    @csrf
                </form>
            </li>

        </ul>

    </div>
    <div class="content">
        <!-- start head -->

        <!-- end head -->
        <div class="wrapper d-grid gap-20 ">
            <div class="welcome bg-white rad-10 ">
                <h1 class="p-relative">إدارة المشرفين</h1>

                <livewire:supervisors >
                </div>
            </div>
        </div>

    </div>
    </div>
  
    @include('sweetalert::alert')
    <script>
        function StoreSupervisor(supervisor_id)
        {
            var form = document.getElementById(supervisor_id);
        console.log(form);
          Swal.fire({
                  title: "تبديل سكن المشرف",
                  text: " لايمكنك التراجع",
                  background:"var(--black-color)",
              color:"var(--white-color)",
                  showCancelButton: true,
                  confirmButtonColor: "var(--red-color)",
                  confirmButtonText: "تم", 
                  cancelButtonText: 'أغلاق',
                  closeOnConfirm: false,
           
              }).then((result) => {
            if(result.isConfirmed){
            
        form.submit();
    }
        })
            return false;
            }
                 
            </script>
    @livewireScripts
</body>
</html>