<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
    <link href="{{ asset('css/framework.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <title>تسجيل الدخول</title>
</head>


<body>
    <div class="background">
        <div class="shape"></div>
        <div class="shape"></div>
    </div>
    <form method="POST" action="{{ route('login') }}">
        @csrf
        <h3> تسجيل الدخول</h3>

        <label for="username">اسم المستخدم</label>
        <input class="c-black" type="text" placeholder=" اسم المستخدم " id="username" name="user_id" required>

        <label for="password">كلمة المرور</label>
        <input class="c-black" type="password" placeholder="كلمة السر" id="password" name='password' required>
        <button type="submit"> تسجيل الدخول</button>
    </form>
    @include('sweetalert::alert');
</body>

</html>