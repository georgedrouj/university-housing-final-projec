<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo e(asset('css/blocked-form.css')); ?>">
    <title></title>
</head>

<body>
    <form style="" id="popupForm" >
        <h3> سبب الحجب</h3>
        <button class="closeModal" onclick="closeForm()"></button>
        <form  method="POST" action="<?php echo e(request()->segment(count(request()->segments()))); ?> }}" id="block-room">
            <?php echo csrf_field(); ?>
            <?php echo method_field('PUT'); ?>
          <textarea class="field-area" placeholder="سبب الحجب :" name="blocked_reason"></textarea>
          <button type="submit" href="javascript:{}" onclick="confirm()"> حجب </button>
        </form> 

    </form>

 <script type="text/javascript">
        function closeForm()
        {
            document.getElementById("popupForm").style.display="none";
            document.body.id='';
        }

</script>
<script type="text/javascript">
    function confirm(){
  
  var form = document.getElementById("yes");
  swal({
      title: "هل انت متأكد",
      text: " لايمكنك التراجع",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "green",
      confirmButtonText: "موافق", 
      closeOnConfirm: false
  }, function (isConfirmed) {
      if (isConfirmed) {
        console.log('hi');
          form.submit();
      }
  });
return false;
}
</script>
</body>
</html><?php /**PATH C:\Users\User\Desktop\university-housing-final-projec\resources\views/manager/block-form.blade.php ENDPATH**/ ?>