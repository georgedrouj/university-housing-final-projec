<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- framework css file -->
    <link rel="stylesheet" href="{{ asset('css/framework.css') }}">
    <!-- main template css file -->
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <!-- font awesome library -->
    <link rel="stylesheet" href="{{ asset('css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/users.css') }}">
    <link rel="stylesheet" href="{{ asset('css/create-user.css') }}">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap"
        rel="stylesheet">
    <script src="{{ asset('vendor/sweetalert/sweetalert.all.js') }}"></script>
    @livewireStyles
    <title>جامعة القلمون الخاصة</title>
</head>

<body>
    <div class="page d-flex ">
        <div class="sidebar  p-20 p-relative">
            <div class="logo">
                <img src="{{ asset('images/homoon.png') }}" alt="">
            </div>
            <ul>
                <li>
                    <a class="d-flex align-center fs-14  rad-6 p-10 " href="/">
                        <i class="fa-solid fa-sliders fa-fw"></i>
                        <span>الصفحة الرئيسية</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/admin/houses">
                        <i class="fa-solid fa-hotel fa-fw"></i>
                        <span>إدارة السكنات </span>
                    </a>
                </li>
                <li>
                    <a class="active  d-flex align-center fs-14  rad-6 p-10 " href="/admin/user">

                        <i class="fa-solid fa-users-gear fa-fw"></i>
                        <span> إدارة المستخدمين </span>
                    </a>
                    <li>
                       
                        <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">
    
                            <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                            <span>  تسجيل خروج</span>
                        </a>
                        <form method="POST" action="{{ route('logout') }}" id="my_form" style="display: none">
                            @csrf
                        </form>
                    </li>

            </ul>

        </div>
        <div class="content">
            <!-- start head -->
            <!-- end head -->
            <h1 class="p-relative">إدارة المستخدمين</h1>

            <div class="wrapper d-grid gap-20 ">
                <div class="welcome bg-white rad-10 ">
                    <div class="intro p-20 d-flex space-between bg-eee">
                        <button class="user">
                            <a href="javascript:{}" onclick="openForm('add-form')"> إنشاء مستخدم جديد
                                <i class="fa-solid fa-plus fa-fw"></i></a>
                        </button>

                        <livewire:users>

                    </div>
                </div>
            </div>

        </div>
    </div>
    @foreach ($users as $user)
    <form method="POST" action="{{ route('admin.user.update',$user->id) }}" class="popupForm add_user_form"
        style="display: none" id="{{ $user->id }}">
        @csrf
        @method('PUT')
        <h3> تعديل مستخدم</h3>
        <button class="closeModal" onclick="colseForm('{{ $user->id }}')" type="button"> </button>
        <input type="text" placeholder="أدخل اسم المستخدم: " id="username" name="name" value="{{ $user->name }}"
            required>
        <select class=" gender" name="type" value="{{ $user->type }}">
            <option value="manager" @if ( $user->type=='manager' )
                selected
            @endif>مدير</option>
            <option value="supervisor" @if ( $user->type=='supervisor' )
                selected
            @endif>مشرف</option>
            <option value="admin"value="supervisor" @if ( $user->type=='admin' )
                selected
            @endif>أدمن</option>
        </select>
        <select name="gender" class=" gender" value=" {{ $user->gender }}">

            <option value="male" @if ( $user->gender=='male' )
                selected
            @endif>ذكر</option>
            <option value="female" @if ( $user->gender=='female' )
                selected
            @endif>أنثى</option>
        </select>
        <button type="submit" class="add_user_button"> تعديل </button>
    </form>
    @endforeach
    <form method="POST" action="{{ route('admin.user.store') }}" class="popupForm add_user_form" style="display: none"
        id="add-form">
        @csrf
        <h3> إضافة مستخدم</h3>
        <button class="closeModal" onclick="colseForm('add-form')" type="button"> </button>
        <input type="text" placeholder="أدخل اسم المستخدم: " name="name" required>
      
        <select class=" gender" name="type">
            <option value="" selected hidden> اختر دور المستخدم</option>
            <option value="manager">مدير</option>
            <option value="supervisor">مشرف</option>
            <option value="admin">أدمن</option>
        </select>
      
        <select name="gender" class=" gender">
            <option value="" selected hidden> اختر جنس المستخدم</option>
            <option value="male">ذكر</option>
            <option value="female">أنثى</option>
        </select>
        <button type="submit" class="add_user_button"> إضافة </button>
    </form>
    

    @include('sweetalert::alert');

    <script type="text/javascript">
        function openForm(id){
          console.log(id);
          document.getElementById(id).style.display = "block";
          document.body.id='popedup';
      }
      function colseForm(id){
        document.getElementById(id).style.display = "none";
          document.body.id='';
      }
        
     
  
    </script>
    <script>
        function confirm(user_id){
          console.log("hi");
          var form = document.getElementById(user_id);
         Swal.fire({
              title: "حذف هذا المستخدم",
              text: " لايمكنك التراجع",
                background:"var(--black-color)",
              color:"var(--white-color)",
              showCancelButton: true,
              confirmButtonColor: "var(--red-color)",
              confirmButtonText: "حذف", 
              closeOnConfirm: false
          }).then((result)=>{
              if(result.isConfirmed){
                  form.submit();
              }
          });
      return false;
      }
    </script>
 
    @livewireScripts

</body>