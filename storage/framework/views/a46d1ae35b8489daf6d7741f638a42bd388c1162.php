<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- framework css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/framework.css')); ?>">
    <!-- main template css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="<?php echo e(asset('css/normalize.css')); ?>">
    <!-- font awesome library -->
    <link rel="stylesheet" href="<?php echo e(asset('css/all.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/order.css')); ?>">


    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <script src="<?php echo e(asset('vendor/sweetalert/sweetalert.all.js')); ?>"></script>
    <title>جامعة القلمون الخاصة</title>
</head>

<body>
    <div class="page d-flex ">
        <div class="sidebar  p-20 p-relative">
            <div class="logo">
                <img src="<?php echo e(asset('images/homoon.png')); ?>" alt="">          
          </div>
            <ul>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/student">
                        <i class="fa-solid fa-sliders fa-fw"></i>
                        <span>الصفحة الرئيسية</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/student/houses">
                        <i class="fa-solid fa-hotel fa-fw"></i>
                        <span> السكنات</span>
                    </a>
                </li>
                <li>
                    <a class="active d-flex align-center fs-14  rad-6 p-10 " href="/student/myOrder">

                        <i class="fa-solid fa-users-gear fa-fw"></i>
                        <span>  طلباتي </span>
                    </a>
                </li>
                <li>
                    <form method="POST" action="<?php echo e(route('logout')); ?>" id="my_form">
                        <?php echo csrf_field(); ?>
                    <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">

                        <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                        <span>  تسجيل خروج</span>
                    </a>
                    </form>
                </li>
            </ul>

        </div>
        <div class="content">
            <!-- start head -->
            <!-- end head -->
            <h1 class="p-relative">طلباتي</h1>

            <div class="wrapper d-grid gap-20 ">
                <div class="welcome bg-white rad-10 ">
                    <div class="intro p-20 d-flex space-between bg-eee">
            
                        <button class="maintenance" onclick="openForm()" >
                            <a href="/student/maintenance/create"> إنشاء طلب صيانة جديد
                                <i class="fa-solid fa-plus fa-fw"></i></a>
                        </button>
                </div>
                </div>
    </div>
        <div class="intro p-20 d-flex space-between bg-eee">
            <button class="quittance" > 
                <a href="javascript:{}" onclick="confirm()"  >
                طلب براءة ذمة
                </a>
                </button>
            <form method="POST" action="<?php echo e(route('student.quittance.store')); ?>" id="quit">
                <?php echo csrf_field(); ?>
           
            </form>


        </div>
</div>
<div class="container">
    <table>
        <thead>
            <tr>
                <th> رقم الطلب</th>
                <th> نوع الطلب</th>
                <th>حالة الطلب</th>

            </tr>
        </thead>
      
        <tbody>
            <tr>
                <td>1</td>
                <td> صيانة</td>
                <td>قيد المعالجة</td>
            </tr>
        </tbody>
    
    </table>
</div>
    <?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>;
    <script type="text/javascript">
        function confirm2(){
          
            var form = document.getElementById('delete-user');
            swal({
                title: "هل انت متأكد",
                text: "هل تريد إرسال الطلب ",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "orange",
                confirmButtonText: "تأكيد", 
                closeOnConfirm: false
            }, function (isConfirmed) {
                if (isConfirmed) {
                    form.submit();
                }
            });
        return false;
        }
        </script>
        <script type="text/javascript">
        function openForm()
        {
            document.getElementById("popupForm").style.display="block";
            document.body.id='popedup';
        }
        function closeForm()
        {
            document.getElementById("popupForm").style.display="none";
            document.body.id='';
        }

        </script>
       <script type="text/javascript">
        function confirm(){
          
            var form = document.getElementById("quit");
            Swal.fire({
                title: "هل انت متأكد",
                text: "لا يمكنك الغاء الطلب بعد التأكيد",
                showCancelButton: true,
                confirmButtonColor:" var(--move-color)",
                confirmButtonText: "تم", 
                closeOnConfirm: false,
                cancelButtonText: 'إغلاق',
            }).then((result)=>{
                if (result.isConfirmed) {
                    form.submit();
                }
            });
        return false;
        }
        
        
        </script>
        
</body><?php /**PATH C:\Users\User\Desktop\university-housing-final-projec\resources\views/student/order.blade.php ENDPATH**/ ?>