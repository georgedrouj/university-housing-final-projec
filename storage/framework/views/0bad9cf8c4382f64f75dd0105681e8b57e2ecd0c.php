<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- framework css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/framework.css')); ?>">
    <!-- main template css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="<?php echo e(asset('css/normalize.css')); ?>">
    <!-- font awesome library -->
    <link rel="stylesheet" href="<?php echo e(asset('css/all.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/users.css')); ?>">


    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">

    <title>جامعة القلمون الخاصة</title>
</head>

<body>
    <div class="page d-flex ">
        <div class="sidebar  p-20 p-relative">
            <h3 class="p-relative txt-c mt-0">
                النبراس
            </h3>
            <ul>
                <li>
                    <a class="d-flex align-center fs-14  rad-6 p-10 " href="/">
                        <i class="fa-solid fa-sliders fa-fw"></i>
                        <span>الصفحة الرئيسية</span>
                    </a>
                </li>
                <li>
                    <a class="d-flex align-center fs-14  rad-6 p-10 " href="/admin/house">
                        <i class="fa-solid fa-hotel fa-fw"></i>
                        <span>إدارة السكنات</span>
                    </a>
                </li>
                <li>
                    <a class="active d-flex align-center fs-14  rad-6 p-10 " href="/admin/user">

                        <i class="fa-solid fa-users-gear fa-fw"></i>
                        <span> إدارة المستخدمين </span>
                    </a>
                </li>
                <li>
                    <form method="POST" action="<?php echo e(route('logout')); ?>" id="my_form">
                        <?php echo csrf_field(); ?>
                    <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">

                        <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                        <span>  تسجيل خروج</span>
                    </a>
                    </form>
                </li>
            </ul>

        </div>
        <div class="content">
            <!-- start head -->
            <!-- end head -->
            <h1 class="p-relative">إدارة المستخدمين</h1>

            <div class="wrapper d-grid gap-20 ">
                <div class="welcome bg-white rad-10 ">
                    <div class="intro p-20 d-flex space-between bg-eee">
                        <button class="user" onclick="openForm()" >
                            <a href="/admin/user/create"> إنشاء مستخدم جديد
                                <i class="fa-solid fa-plus fa-fw"></i></a>
                        </button>
                        <div class="container">
                            <table>
                                <thead>
                                    <tr>
                                        <th> رقم المستخدم</th>
                                        <th> اسم المستخدم</th>
                                        <th> دور المستخدم</th>
                                        <th>جنس المستخدم</th>
                                        <th> الأحداث</th>


                                    </tr>
                                </thead>
                                <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tbody>
                                    <tr>
                                        <td> <?php echo e($user->id); ?></td>
                                        <td> <?php echo e($user->name); ?></td>
                                        <td><?php echo e($user->type); ?> </td>
                                        <td><?php echo e($user->gender); ?></td>
                                        <td>
                                            
                                            <button class="trash" type="submit"><a  h href="javascript:{}" onclick="document.getElementById(<?php echo e($user->id); ?>).submit();"> <i class="fa-solid fa-trash fa-fw"></i></a>
                                                <form id="<?php echo e($user->id); ?>" method="POST" action='<?php echo e(route('admin.user.destroy',$user->id)); ?>' style="all: revert">
                                                    <?php echo csrf_field(); ?>
                                                    <?php echo method_field('DELETE'); ?>
                                              </form>
                                            </button>
                                        
                                            <button class="pen" > <a href="/admin/user/<?php echo e($user->id); ?>/edit"  > <i class="fa-solid fa-pen fa-fw"></i> </a> </button>
                                        </td>

                                    </tr>
                                </tbody>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>;
    <script type="text/javascript">
        function confirm(){
          
            var form = document.getElementById('delete-user');
            swal({
                title: "هل انت متأكد",
                text: "هل تريد حذف هذا المستخدم",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "orange",
                confirmButtonText: "حذف", 
                closeOnConfirm: false
            }, function (isConfirmed) {
                if (isConfirmed) {
                    form.submit();
                }
            });
        return false;
        }
        </script>
        <script type="text/javascript">
        function openForm()
        {
            document.getElementById("popupForm").style.display="block";
            document.body.id='popedup';
        }
        function closeForm()
        {
            document.getElementById("popupForm").style.display="none";
            document.body.id='';
        }

        </script>
</body><?php /**PATH C:\Users\User\Desktop\university-housing-final-projec\resources\views/admin/user/index.blade.php ENDPATH**/ ?>