<?php

namespace App\Console\Commands;

use App\Models\Contract;
use Illuminate\Console\Command;

class Confirmation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'confirmation:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'to flip all the contract to confirmation state';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $contracts=Contract::where('contract_state','=','active_now')->get();
        foreach($contracts as $contract)
        {
            $contract->contract_state='under_confirmation';
            $contract->save();
        }
    }
}
