<!DOCTYPE html>
<html dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- main template css file -->
    
    <link rel="stylesheet" href="<?php echo e(asset('css/framework.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="<?php echo e(asset('css/normalize.css')); ?>">
    <!-- font awesome library -->
    <link rel="stylesheet" href="<?php echo e(asset('css/all.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/users.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/supervisor.css')); ?>">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <title>جامعة القلمون الخاصة</title>
</head>
<body>
    <div class="page d-flex ">
    <div class="sidebar  p-20 p-relative">
        <h3 class="p-relative txt-c mt-0">
            النبراس
        </h3>
        <ul>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/">
                    <i class="fa-solid fa-sliders fa-fw"></i>
                    <span>الصفحة الرئيسية</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/houses">
                    <i class="fa-solid fa-hotel fa-fw"></i>
                    <span>السكنات </span>
                </a>
            </li>
            <li>
                <a class="active d-flex align-center fs-14  rad-6 p-10 " href="/manager/import">

                    <i class="fa-solid fa-people-line fa-fw"></i>
                    <span> الطلاب</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="comfirmation.html">

                    <i class="fa-brands fa-readme fa-fw"></i>
                    <span> طلب التثبيت</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/contract">

                    <i class="fa-brands fa-readme fa-fw"></i>
                    <span> طلب التسجيل</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="quitence.html">

                    <i class="fa-brands fa-readme fa-fw"></i>
                    <span> طلب براءة الذمة</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="mantinance.html">

                    <i class="fa-brands fa-readme fa-fw"></i>
                    <span> طلب  الصيانة</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="<?php echo e(route('manager.supervisor.index')); ?>">

                    <i class="fa-solid fa-people-line fa-fw"></i>
                    <span>مشرفي الوحدات السكنية</span>
                </a>
            </li>
            <li>
                <form method="POST" action="<?php echo e(route('logout')); ?>" id="my_form">
                    <?php echo csrf_field(); ?>
                <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">

                    <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                    <span>  تسجيل خروج</span>
                </a>
                </form>
            </li>

        </ul>

    </div>
    <div class="content">
        <!-- start head -->

        <!-- end head -->



        <div class="wrapper d-grid gap-20 ">
            <div class="welcome bg-white rad-10 ">
                <h1 class="p-relative">إدارة المشرفين</h1>

                <form method="POST" action="<?php echo e(route('manager.supervisor.store')); ?>" >
                    <?php echo csrf_field(); ?>
                    <h3> إضافة مشرف</h3>
                    <button class="closeModal" onclick="closeForm()"> </button>
                    <label for="text"> اختر السكن</label>
                    <select class=" " name="house_id" id="houselist" onchange="fillUser()">
                        <?php $__currentLoopData = $houses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $house): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($house->gender); ?>"  ><?php echo e($house->id); ?> </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                    <label for=""> اختر اسم المشرف</label>
                    <select  name="femaleSupervisor" class="" id="supervisorlist1" style="visibility: hidden">
                        <?php $__currentLoopData = $femaleSupervisors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $female): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($female->id); ?>" ><?php echo e($female->name); ?> </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                    <select  name="maleSupervisor" class="" id="supervisorlist2" style="visibility: hidden">
                        <?php $__currentLoopData = $maleSupervisors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $male): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($male->id); ?>"  ><?php echo e($male->name); ?> </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                 
                    <button type="submit"> إضافة </button>
                </form>
                
                <div class="intro p-20 d-flex space-between bg-eee">
                    <button class="user" onclick="openForm()" >
                  إضافة مشرف
                            <i class="fa-solid fa-plus fa-fw"></i>
                    </button>
                    <div class="container">
                        <table>
                            <thead>
                                <tr>
                                    <th> رقم السكن</th>
                                    <th> اسم المشرف</th>
                                    <th> الأحداث</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                <?php $__currentLoopData = $supervisors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $supervisor): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td> <?php echo e($supervisor->house_id); ?></td>
                                    <td> <?php echo e($supervisor->getUser->name); ?></td>
                                    <td>
                                            
                                        <button class="trash" type="submit"><a  h href="javascript:{}" onclick=""> <i class="fa-solid fa-trash fa-fw"></i></a>
                                            <form id="" method="POST" action='' style="all: revert">
                                                <?php echo csrf_field(); ?>
                                                <?php echo method_field('DELETE'); ?>
                                          </form>
                                        </button>
                                    
                                        <button class="pen" > <a href=""  > <i class="fa-solid fa-pen fa-fw"></i> </a> </button>
                                    </td>
                                
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                           
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>
    <?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <script type="text/javascript">
        function openForm()
        {
            document.getElementById("popupForm").style.display="block";
            document.body.id='popedup';
        }
        function closeForm()
        {
            document.getElementById("popupForm").style.display="none";
            document.body.id='';
        }

        function fillUser()
             {
                var e = document.getElementById("houselist");
                var value = e.value;
                if(value=='female')
                {
                    document.getElementById("supervisorlist1").style.visibility = "visible";
                    document.getElementById("supervisorlist2").style.visibility = "hidden";
                }
                else{
                    document.getElementById("supervisorlist1").style.visibility = "hidden";
                    document.getElementById("supervisorlist2").style.visibility = "visible";
                }

             }

        </script>
    
</body>
</html><?php /**PATH C:\Users\User\Desktop\university-housing-final-projec\resources\views/manager/supervisor.blade.php ENDPATH**/ ?>