<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;


class Student extends Model
{
    use HasFactory;
    use SoftDeletes;
    use Searchable;
    
    protected $table='students';
    protected $fillable=['university_id','student_name','gender','disapproval'];
    protected $primaryKey='university_id';
    public function getContracts(){
        return $this->hasMany(Contract::class);
    }
    public function getContract(){
        return $this->hasMany(Contract::class,'student_id')->where('contract_state','=','active_now')->first();
    }
    public function getActiveContract(){
        return $this->hasMany(Contract::class,'student_id')->where('contract_state','=','active_now');
    }
    public function getMaintenances(){
        return $this->hasManyThrough(Maintenance::class,Contract::class,'student_id','contract_id');
    }
    public function getQuittances(){
        return $this->hasManyThrough(Quittance::class,Contract::class,'student_id','contract_id');
    }
    public function getExcep(){
        return $this->hasMany(Excep::class);
    }
    public function getStudentData(){
        return $this->hasOne(StudentData::class);
    }
    public function getSwitchRoom(){
        return $this->hasManyThrough(SwitchRoom::class,Contract::class,'student_id','contract_id');
    }
    public function hasRoom(){
        return $this->hasMany(Contract::class,'student_id')->where(function ($query) {
            $query->where('contract_state', '=', 'active_now')
                  ->orWhere('contract_state', '=', 'in_progress');
        })->get()->count()>0;
    }
    public function toSearchableArray()
    {
        return [
            'university_id' => $this->university_id,
            'student_name' => $this->student_name,
        ];
    }
    public function hasSwitchRoom()
    {
        return $this->getSwitchRoom()->where(function ($query) {
            $query->where('switch_room_state', '=', 'in_supervisor')
            ->orWhere('switch_room_state', '=', 'in_manager');
        })->get()->count()>0;
    }
    public function hasMaintenance()
    {
        return $this->getMaintenances()->where(function ($query) {
            $query->where('maintenace_state', '=', 'in_supervisor')
            ->orWhere('maintenace_state', '=', 'in_manager');
        })->get()->count()>0;
    }
    public function hasQuittance(){
        return $this->getQuittances->where(function ($query){
            $query->where('quittance_state','=','in_supervisor')->orWhere('quittance_state','=', 'in_manager');
        })->count()>0;
    }
    public function hasOrder(){
        return $this->hasQuittance() || $this->hasMaintenance() || $this->hasSwitchRoom();
    }
    public function hasActiveRoom(){
        return $this->getActiveContract()->count()>0;
    }
}
