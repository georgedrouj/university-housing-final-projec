<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- framework css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/framework.css')); ?>">
    <!-- main template css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="<?php echo e(asset('css/normalize.css')); ?>">
    <!-- font awesome library -->
    <link rel="stylesheet" href="<?php echo e(asset('css/all.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/advertisment.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/create-adv.css')); ?>">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <script src="<?php echo e(asset('vendor/sweetalert/sweetalert.all.js')); ?>"></script>
    <?php echo \Livewire\Livewire::styles(); ?>

    <title>جامعة القلمون الخاصة</title>

</head>

<body>
   
    <div class="page d-flex ">
        <div class="sidebar  p-20 p-relative">
            <div class="logo">
                <img src="<?php echo e(asset('images/homoon.png')); ?>" alt="logo">
            </div>
            <ul>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/">
                        <i class="fa-solid fa-sliders fa-fw"></i>
                        <span>الصفحة الرئيسية</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/houses">
                        <i class="fa-solid fa-hotel fa-fw"></i>
                        <span>السكنات </span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/import">

                        <i class="fa-solid fa-people-line fa-fw"></i>
                        <span> الطلاب</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="comfirmation.html">

                        <i class="fa-brands fa-readme fa-fw"></i>
                        <span> طلب التثبيت</span>
                    </a>
                </li>

                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/contract">

                        <i class="fa-brands fa-readme fa-fw"></i>
                        <span> طلب التسجيل</span>
                    </a>
                </li>
                <li>
                    <a class="d-flex align-center fs-14  rad-6 p-10 " href="/manager/switch_room">

                        <i class="fa-brands fa-readme fa-fw"></i>
                        <span> طلب التبديل</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="<?php echo e(route('manager.quittance.index')); ?>">

                        <i class="fa-brands fa-readme fa-fw"></i>
                        <span> طلب براءة الذمة</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/maintenance">

                        <i class="fa-brands fa-readme fa-fw"></i>
                        <span> طلب  الصيانة</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="">

                        <i class="fa-solid fa-users"></i>
                        <span>مشرفي الوحدات السكنية</span>
                    </a>
                </li>
                <li>
                    <a class="active d-flex align-center fs-14  rad-6 p-10 " href="/manager/advertisments">

                        <i class="fa-solid fa-rectangle-ad  fa-fw "></i>
                        <span> الإعلانات </span>
                    </a>
                </li>
                <li>
                    <form method="POST" action="<?php echo e(route('logout')); ?>" id="my_form">
                        <?php echo csrf_field(); ?>
                    <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">

                        <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                        <span>  تسجيل خروج</span>
                    </a>
                    </form>
                </li>

            </ul>

        </div>
        
        <div class="content">
            <!-- start head -->
            <!-- end head -->
            <h1 class="p-relative"> الإعلانات </h1>
            <div class="wrapper d-grid gap-20 ">
               <?php
if (! isset($_instance)) {
    $html = \Livewire\Livewire::mount('advirtesment')->html();
} elseif ($_instance->childHasBeenRendered('2wgLh4q')) {
    $componentId = $_instance->getRenderedChildComponentId('2wgLh4q');
    $componentTag = $_instance->getRenderedChildComponentTagName('2wgLh4q');
    $html = \Livewire\Livewire::dummyMount($componentId, $componentTag);
    $_instance->preserveRenderedChild('2wgLh4q');
} else {
    $response = \Livewire\Livewire::mount('advirtesment');
    $html = $response->html();
    $_instance->logRenderedChild('2wgLh4q', $response->id(), \Livewire\Livewire::getRootElementTagName($html));
}
echo $html;
?>
            </div>

        </div>
    </div>
    <?php $__currentLoopData = $advetisments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $advertisment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <form method="GET" action="/manager/advertisments/<?php echo e($advertisment->id); ?>"   style="display: none; background-color: white" class="adv-form popupForm" id="<?php echo e($advertisment->id); ?>" >
        <?php echo csrf_field(); ?>
        <h3 style="color: black"> <?php echo e($advertisment->title); ?></h3>
        <button class="closeModal"  onclick="colseForm('<?php echo e($advertisment->id); ?>')" type="button"> </button>
        <?php if($advertisment->addressed_to=='all'): ?>
        <label for="username"  style="color: black">إلى كافة الطلاب والمشرفين</label>
        <?php elseif($advertisment->addressed_to=='student'): ?>
        <label for="username" style="color: black" >إلى كافة الطلاب </label>
        <?php elseif($advertisment->addressed_to=='supervisor'): ?>
        <label for="username"  style="color: black">إلى كافةالمشرفين</label>
        <?php endif; ?>
        <label for="username" style="color: black" ><?php echo e($advertisment->message); ?> </label>

    </form>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <form method="POST" action="<?php echo e(route('manager.advertisments.store')); ?>"  id="adv-form" style="display: none" class="adv-form popupForm">
        <?php echo csrf_field(); ?>
        <h3> إضافة إعلان</h3>
        <button class="closeModal" onclick="colseForm('adv-form')" type="button"> </button>
        <label for="username"  >عنوان الإعلان</label>
        <input type="text" placeholder=" عنوان الإعلان"  name="title" required >
        <label for="username"  >محتوى الإعلان</label>
        <textarea placeholder=" محتوى الإعلان"  name="message" required class="message"></textarea>
        <label for="username"  >لمن موجه هذا الإعلان</label>
        <select class="addressed_to" name="addressed_to" > 
            <option value="all ">المشرفين والطلاب</option>
            <option value="student">الطلاب</option>
            <option value="supervisor">المشرفين</option>
        </select>
        <button class="adv-button" type="submit"> نشر الإعلان</button>
    </form>
    <script type="text/javascript">
        function openForm(id){
            document.getElementById(id).style.display = "block";
            document.body.id='popedup';
        }
        function colseForm(id){
          document.getElementById(id).style.display = "none";
            document.body.id='';
        }
    </script>
 <?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>;
 <?php echo \Livewire\Livewire::scripts(); ?>

</body>

</html><?php /**PATH G:\Programing\universtyHousing\resources\views/manager/advertisment.blade.php ENDPATH**/ ?>