<?php

namespace App\Http\Livewire;

use App\Models\Room;
use App\Models\Student;
use Livewire\Component;

class Rooms extends Component
{
    public $house_id;
    public $hasRoom;
    public $hasSwitchRoom;
    public  $hasActiveRoom;
    public $searchQuery;
    public $orderQuery;
    public $emptyQuery;
    public function mount(){
        $this->house_id=request('house_id');
      if(Auth()->user()->type=='student'){
        $studnet=Student::find(Auth()->user()->user_id);
        $this->hasRoom=$studnet->hasRoom();
        $this->hasSwitchRoom=$studnet->hasSwitchRoom();
        $this->hasActiveRoom=($studnet->getActiveContract->count()>0);
      }
      $this->searchQuery="";
      $this->orderQuery="";
      $this->emptyQuery="";
    }
    public function render()
    {
        $rooms=Room::where('house_id','=',$this->house_id)->when(
            Auth()->user()->type=='student',function($query){
                $query->where('blocked_now','=','0');
            }
        )->when(
            $this->searchQuery!="",function($query){
                $query->where('room_name','like','%'.$this->searchQuery.'%');
            }
        )->when(
            $this->orderQuery!="",function($query){
                $query->orderBy($this->orderQuery);
             
            }
        )->get()->filter(function($item){
            if($this->emptyQuery!=""){
            if($this->emptyQuery=="full"){
                return $item['emp']==0;
            }
            else if($this->emptyQuery=="empty"){
                return $item['emp']==$item['capacity'];
            }
            else{
                return $item['emp']>0&&$item['emp']<$item['capacity'];
            }}
            else{
                return true;
            }
        });
      
      
        return view('livewire.rooms',['rooms'=>$rooms]);
    }
}
