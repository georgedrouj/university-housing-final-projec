<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo e(asset('css/create-user.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/framework.css')); ?>">
    <title>إدارة المستخدمين</title>
</head>
</head>

<body>
    <form method="POST" action="<?php echo e(route('admin.user.store')); ?>" id="popupForm"  >
        <?php echo csrf_field(); ?>
        <h3> إضافة مستخدم</h3>
        <button class="closeModal" onclick="closeForm()"> </button>
        <label for="username">رقم المستخدم</label>
        <input type="text" placeholder="أدخل رقم المستخدم" id="username" name="user_id" required>
        <label for="username">اسم المستخدم</label>
        <input type="text" placeholder="أدخل اسم المستخدم: " id="username" name="name" required>

        <label for="password">كلمة المرور</label>
        <input type="password" placeholder="أدخل كلمة المرور:" id="password" name="password" required>
        <label for="text"> اختر دور المستخدم</label>
        <select class=" gender" name="type">
            <option value="manager">مدير</option>
            <option value="supervisor">مشرف</option>
            <option value="admin">أدمن</option>
        </select>
        <label for=""> اختر جنس المستخدم</label>
        <select  name="gender" class=" gender">
         
            <option value="male">ذكر</option>
            <option value="female">أنثى</option>
        </select>
        <button type="submit"> إضافة </button>
    </form>
</body>

</html>

</body>

</html><?php /**PATH G:\Programing\universtyHousing\resources\views/admin/user/create.blade.php ENDPATH**/ ?>