<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- framework css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/framework.css')); ?>">
    <!-- main template css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="<?php echo e(asset('css/normalize.css')); ?>">
    <!-- font awesome library -->
    <link rel="stylesheet" href="<?php echo e(asset('css/all.min.css')); ?>">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <script src="<?php echo e(asset('vendor/sweetalert/sweetalert.all.js')); ?>"></script>
   
    <!-- main template css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/orders.css')); ?>">
    <title>Document</title>
</head>

<body>
    <div class="page d-flex ">
        <div class="sidebar  p-20 p-relative">
            <div class="logo">
                <img src="<?php echo e(asset('images/homoon.png')); ?>" alt="">          
          </div>
          <ul>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/">
                    <i class="fa-solid fa-sliders fa-fw " ></i>
                    <span>الصفحة الرئيسية</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/houses">
                    <i class="fa-solid fa-hotel fa-fw"></i>
                    <span>السكنات </span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/students">

                    <i class="fa-solid fa-people-line fa-fw"></i>
                    <span> الطلاب</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/supervisor">

                    <i class="fa-solid fa-users"></i>
                    <span>مشرفي الوحدات السكنية</span>
                </a>
            </li>
        
            <li>
                <a class="active d-flex align-center fs-14  rad-6 p-10 " href="/manager/contract">

                    <i class="fa-brands fa-readme fa-fw"></i>
                    <span> طلب التسجيل</span>
                </a>
            </li>
           
            <li>
                <a class="d-flex align-center fs-14  rad-6 p-10 " href="/manager/switch_room">

                    <i class="fa-solid fa-shuffle"></i>
                    <span> طلب التبديل</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/quittance">

                    <i class="fa-solid fa-person-walking-arrow-right"></i>
                    <span> طلب براءة الذمة</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/maintenance">

                    <i class="fa-sharp fa-solid fa-wrench"></i>
                    <span> طلب  الصيانة</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="comfirmation.html">

                    <i class="fa-solid fa-circle-check"></i>
                    <span> طلب التثبيت</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="comfirmation.html">

                    <i class="fa-solid fa-bullhorn"></i>
                    <span> الإعلانات </span>
                </a>
            </li>
            <li>
              
                <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">

                    <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                    <span>  تسجيل خروج</span>
                </a>
                <form method="POST" action="<?php echo e(route('logout')); ?>" id="my_form" style="display: none">
                    <?php echo csrf_field(); ?>
                </form>
            </li>

        </ul>

        </div>
        <div class="content">
            <!-- start head -->
            <!-- end head -->
            <h1 class="p-relative">الطلبات </h1>
            <div class="wrapper d-grid gap-20 ">
                <div class="welcome bg-white rad-10 ">
                    <div class="intro p-20 d-flex space-between bg-eee">
                        <div class="container">
                            <!-- section-success -->
                            <?php $__currentLoopData = $contracts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $contract): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="wrapper-success">
                               
                                <div class="card">
                                   
                                    <span dir="ltr"><?php echo e($contract->created_at->diffForHumans( Carbon\Carbon::now())); ?> </span>
                                  
                                    <div class="subject">
                                       
                                        <span> رقم الطالب: <?php echo e($contract->student_id); ?> </span>
                                        <span> اسم الطالب: <?php echo e($contract->getStudent->student_name); ?> </span>
                                        <span> رقم الغرفة: <?php echo e($contract->getHouse->id." ".$contract->getRoom->room_name); ?></span>
                                        <span>معلومات الطلب</span>
                                        <p>
                                            ارجو الموافقة عل طلب التسجيل في هذه الغرفة
                                        </p>
                                    </div>
                                    <div class="orderfooter">
                                    <button onclick="return  confirm(<?php echo e($contract->id); ?>)" type="button">موافق</button>
                                    <button  onclick="return confirm2(<?php echo e($contract->id); ?> )" type="button">غير موافق</button>
                                    <form method="POST" action="contract/<?php echo e($contract->id); ?>" id="<?php echo e($contract->id); ?>" style="display: none">
                                        <?php echo csrf_field(); ?>
                                        <?php echo method_field('PUT'); ?>
                                </form>
                                </div>
                                </div>
                                
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <!-- section-success -->
                        </div>
                    </div>
                </div>

            </div>
            <script type="text/javascript">
    function confirm(contract_id){
  var form = document.getElementById(contract_id);
  Swal.fire({
      title: "هل انت متأكد",
      text: " لايمكنك التراجع",
  
      showCancelButton: true,
      confirmButtonColor: "var(--green-color)",
      confirmButtonText: "تم", 
      cancelButtonText: 'أغلاق',
      closeOnConfirm: false
  }).then((result) => {
  /* Read more about isConfirmed, isDenied below */
  if (result.isConfirmed) {
    var ok = document.createElement("input");
    ok.value="true";
    ok.name="ok";
    ok.style.display="none";
    form.appendChild(ok);
   form.submit();
  } 
});
return false;
}
    </script>
       <script type="text/javascript">
        function confirm2(contract_id){
      
      var form = document.getElementById(contract_id);
      console.log(form);
      Swal.fire({
          title: "هل انت متأكد",
          text: " لايمكنك التراجع",
          html: `<input type="text" id="reject_reason" class="swal2-input" placeholder="سبب الرفض" required>`,
          showCancelButton: true,
          confirmButtonColor: "var(--red-color)",
          confirmButtonText: "تم", 
          cancelButtonText: 'أغلاق',
          closeOnConfirm: false,
          preConfirm: () => {
    const reject_reason = Swal.getPopup().querySelector('#reject_reason').value
    if (!reject_reason) {
      Swal.showValidationMessage(`الرجاء ادخال جميع المعلومات المطلوبة`)
    }
    return {reject_reason:reject_reason}
    }
   
      }).then((result) => {
    if(result.isConfirmed){
    var ok = document.createElement("input");
    ok.value="false";
    ok.name="ok";
    ok.style.display="none";
    form.appendChild(ok);
    var reason = document.createElement("input");
    reason.value= `${result.value.reject_reason}`.trim();
    reason.name="reason";
    reason.style.display="none";
    form.appendChild(reason);
   form.submit();}
})
    return false;
    }
        </script>
        

        </div>
<?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>

</html><?php /**PATH G:\Programing\universtyHousing\resources\views/manager/regestration_request.blade.php ENDPATH**/ ?>