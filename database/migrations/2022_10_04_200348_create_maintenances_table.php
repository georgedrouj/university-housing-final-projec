<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maintenances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->string('supirvisor_comment')->nullable();
            $table->string('reject_reason')->nullable();
            $table->date('maintenace_date')->nullable();
            $table->string('maintenace_state');
            $table->unsignedInteger('contract_id');
            $table->foreign('contract_id')
            ->references('id')
            ->on('contracts')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maintenances');
    }
};
