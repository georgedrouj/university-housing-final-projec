<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo e(asset('css/create-room.css')); ?>">
    <title></title>
</head>

<body>
    <form method="POST" action="">
        <?php echo csrf_field(); ?>
        <h3> إضافة غرفة</h3>
        <button class="closeModal"> </button>
        <label for="roomname">رقم الغرفة</label>
        <input type="text" placeholder="أدخل رقم الغرفة:" name="name" required>
        <label for="floorname">رقم الطابق</label>
        <input type="text" placeholder="أدخل رقم الطابق:" name="floor" required>
        <label for="roomcapacity">سعة الغرفة</label>
        <input type="text" placeholder="أدخل سعة الغرفة:" name="capacity" required>
        <label for="roomspace">مساحة الغرفة</label>
        <input type="text" placeholder="أدخل مساحة الغرفة:" name="space" required>
        <label for="roomdirection">اتجاه الغرفة</label>
        <input type="text" placeholder="أدخل اتجاه الغرفة:" name="direction" required>
        <label for="roomprice">سعر الغرفة</label>
        <input type="price" placeholder="أدخل سعر الغرفة:" name="price" required>
        <button> إضافة </button>

    </form>
</body>

</html><?php /**PATH C:\Users\User\Desktop\university-housing-final-projec\resources\views/admin/room/create.blade.php ENDPATH**/ ?>