<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- main template css file -->
    
    <link rel="stylesheet" href="<?php echo e(asset('css/framework.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="<?php echo e(asset('css/normalize.css')); ?>">
    <!-- font awesome library -->
    <link rel="stylesheet" href="<?php echo e(asset('css/all.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/studentregestration.css')); ?>">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

    <title>جامعة القلمون الخاصة</title>
</head>

<body>

    <div class="page d-flex  ">
        <div class="sidebar  p-20 p-relative">
            <div class="logo">
                <img src="<?php echo e(asset('images/homoon.png')); ?>" alt="">          
          </div>
            <ul>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/">
                        <i class="fa-solid fa-sliders fa-fw"></i>
                        <span>الصفحة الرئيسية</span>
                    </a>
                </li>
                <li>
                    <a class="active d-flex align-center fs-14  rad-6 p-10 " href="/manager/houses">
                        <i class="fa-solid fa-hotel fa-fw"></i>
                        <span>السكنات </span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/import">

                        <i class="fa-solid fa-people-line fa-fw"></i>
                        <span> الطلاب</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="orders.html">

                        <i class="fa-brands fa-readme fa-fw"></i>
                        <span> طلب التثبيت</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/contract">

                        <i class="fa-brands fa-readme fa-fw"></i>
                        <span> طلب التسجيل</span>
                    </a>
                </li>
                <li>
                    <a class="d-flex align-center fs-14  rad-6 p-10 " href="/manager/switch_room">

                        <i class="fa-brands fa-readme fa-fw"></i>
                        <span> طلب التبديل</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="<?php echo e(route('manager.quittance.index')); ?>">

                        <i class="fa-brands fa-readme fa-fw"></i>
                        <span> طلب براءة الذمة</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="<?php echo e(route('manager.maintenance.index')); ?>">

                        <i class="fa-brands fa-readme fa-fw"></i>
                        <span> طلب  الصيانة</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="">

                        <i class="fa-solid fa-users"></i>
                        <span>مشرفي الوحدات السكنية</span>
                    </a>
                </li>
                <li>
                    <form method="POST" action="<?php echo e(route('logout')); ?>" id="my_form" class="remove-all-styles">
                        <?php echo csrf_field(); ?>
                   
                    <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">

                        <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                        <span>  تسجيل خروج</span>
                    </a>
                    <form method="POST" action="<?php echo e(route('logout')); ?>" id="my_form" class="remove-all-styles">
                        <?php echo csrf_field(); ?>
                    </form>
                </li>

            </ul>

        </div>

        <div class="content ">
            <!-- start head -->
            <!-- end head -->
            <h1 class="p-relative"> تسجيل الطالب</h1>
            <div class="wrapper d-flex gap-20 ">
                <!-- <div class="welcome bg-white rad-10 "> -->
                <div class="intro p-20   bg-eee">

                    <form  id="regestrationForm" method="POST" action="<?php echo e(route('manager.contract.store',last(request()->segments()) )); ?>">
                        <?php echo csrf_field(); ?>
                        <h3> تسجيل الطالب</h3>
                        <label for="username">اسم الطالب</label>
                        <input type="text" placeholder="اسم الطالب"   name="student_name">
                        <label for="username" >رقم الطالب</label>
                        <input type="number" placeholder="20201113"  name='student_id'>
                        <div class="container">
                            <div class="selector">
                                <div class="selecotr-item">
                                    <input type="radio" id="radio1" name="type" class="selector-item_radio" checked onclick="hide()" value="1">
                                    <label for="radio1" class="selector-item_label">جديد </label>
                                </div>

                                <div class="selecotr-item">
                                    <input type="radio" id="radio2" name="type" class="selector-item_radio" onclick="hide()" value="0">
                                    <label for="radio2" class="selector-item_label"> قديم</label>
                                </div>
                                <div class="selecotr-item">
                                    <input type="radio" id="radio3" name="type" class="selector-item_radio" onclick="show()" value="2">
                                    <label for="radio3" class="selector-item_label">مستثنى </label>
                                </div>
                            </div>
                        </div>
                        <textarea class="field-area" placeholder="معلومات الاستثناء:" id="s" style="display: none" name="desc"></textarea>
                        <button type="submit"> تسجيل </button>

                    </form>


                </div>
           


            </div>


        </div>
    </div>
    <?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <script>
        function show() {
            document.getElementById('s').style.display = "block";
        }

        function hide() {
            document.getElementById('s').style.display = "none";
        }
    </script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
   <script type="text/javascript">
$(function () 
{
    var $regestrationForm=$('#regestrationForm');
        $regestrationForm.validate(
            {
                rules:{
                    student_name:{
                       required:true
                    },
                    student_id:{
                       required:true,
                       min:200010000,
                       max:202310000
                    }
                },
                messages:{
                    student_name:{
                       required:'يجب إدخال اسم الطالب'
                    },
                    student_id:{
                       required:'يجب إدخال رقم الطالب',
                       min:'الرقم الجامعي للطالب غير صحيح',
                       max:'الرقم الجامعي للطالب غير صحيح'
                    }
                }
            }
        );
  
});
    </script>
</body><?php /**PATH C:\Users\User\Desktop\university-housing-final-projec\resources\views/manager/student_regstration.blade.php ENDPATH**/ ?>