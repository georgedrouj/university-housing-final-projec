<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class House extends Model
{
    use HasFactory;
    use SoftDeletes;
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;
    protected $table='houses';
    protected $fillable=['id','gender'];
    protected $primaryKey='id';
    public $incrementing = false; 
     public function getRooms()
    {
        return $this->HasMany(Room::class);
    }
    public function getSupervisors(){
        return $this->hasMany(Supervisor::class);
    }
    public function getSwitchRooms(){
        return $this->hasManyDeep(SwitchRoom::class,[Room::class,Contract::class]);
    }
    public function getMaintenanaces(){
        return $this->hasManyDeep(Maintenance::class,[Room::class,Contract::class]);
    }
    public  function getQuittances()
    {
        return $this->hasManyDeep(Quittance::class,[Room::class,Contract::class]);
    }
}
