<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- framework css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/framework.css')); ?>">
    <!-- main template css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="<?php echo e(asset('css/normalize.css')); ?>">
    <!-- font awesome library -->
    <link rel="stylesheet" href="<?php echo e(asset('css/all.min.css')); ?>">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <script src="<?php echo e(asset('vendor/sweetalert/sweetalert.all.js')); ?>"></script>
   
    <!-- main template css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/orders.css')); ?>">
    <title>Document</title>
</head>

<body>
    <div class="page d-flex ">
        <div class="sidebar  p-20 p-relative">
            <div class="logo">
                  <img src="<?php echo e(asset('images/homoon.png')); ?>" alt="">          
            </div>
            <ul>
                <li>
                    <a class="d-flex align-center fs-14  rad-6 p-10 " href="/">
                        <i class="fa-solid fa-sliders fa-fw " ></i>
                        <span>الصفحة الرئيسية</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/supervisor/quittance">
                        <i class="fa-solid fa-building"></i>
                        <span> وحدتي السكنية</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/supervisor/switch_room">

                        <i class="fa-solid fa-shuffle"></i>
                        <span>طلب تبديل غرفة </span>
                    </a>
                </li>
                <li>
                    <a class="active d-flex align-center fs-14  rad-6 p-10 " href="/supervisor/quittance">

                      
                        <i class="fa-solid fa-person-walking-arrow-right"></i>
                        <span> طلب براءة الذمة</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/supervisor/maintenance">

                        <i class="fa-sharp fa-solid fa-wrench"></i>
                        <span> طلب  الصيانة</span>
                    </a>
                </li>
               
                <li>
                   
                    <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">

                        <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                        <span>  تسجيل خروج</span>
                    </a>
                    <form method="POST" action="<?php echo e(route('logout')); ?>" id="my_form" style="display:none">
                        <?php echo csrf_field(); ?>
                    </form>
                </li>

            </ul>

        </div>
        <div class="content">
            <!-- start head -->
            <!-- end head -->
            <h1 class="p-relative">الطلبات </h1>
            <div class="wrapper d-grid gap-20 ">
                <div class="welcome bg-white rad-10 ">
                    <div class="intro p-20 d-flex space-between bg-eee">
                        <div class="container">
                            <!-- section-success -->
                            <?php $__currentLoopData = $quittances; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $quittance): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        
                            <div class="wrapper-success">
                               
                                <div class="card">
                                   
                                    <div class="icon">
                                        <i class="fa-solid fa-bell"></i>
                                    </div>
                                  
                                    <div class="subject">
                                       
                                        <span> رقم الطالب: <?php echo e($quittance->getStudent->university_id); ?> </span>
                                        <span> رقم الغرفة: <?php echo e($quittance->getRoom->house_id." ".$quittance->getRoom->room_name); ?></span>
                                        <span>معلومات الطلب</span>
                                        <p>
                                            أرجو الموافقة على طلب براءة الذمة    
                                        </p>
                                    </div>
                                    <div class="orderfooter">
                                        <button  onclick="confirm3('<?php echo e($quittance->id); ?>')" type="button"> يوجد نواقص</button>
                                        <button  onclick="confirm2('<?php echo e($quittance->id); ?>')" type="button">لا يوجد نواقص</button>
                                    
                                        <form method="POST" action="quittance/<?php echo e($quittance->id); ?>" id="<?php echo e($quittance->id); ?>" style="display: none">
                                            <?php echo csrf_field(); ?>
                                            <?php echo method_field('PUT'); ?>
                                    </form>
                                </div>
                                </div>
                                
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <!-- section-success -->
                        </div>
                    </div>
                </div>

            </div>
     
       <script>
        function confirm2(quittance_id){
      
      var form = document.getElementById(quittance_id);
      console.log(form);
      Swal.fire({
          title: "اضف تعليقك ",
          text: " لايمكنك التراجع",
          background:"var(--black-color)",
          color:"var(--white-color)",
          html: `<input type=number  id="electricity" class="swal2-input" placeholder="قراءة عداد الكهرباء " required style="height:100px">`,
          showCancelButton: true,
          confirmButtonColor: "var(--move1-color)",
          confirmButtonText: "تم", 
          cancelButtonText: 'أغلاق',
          closeOnConfirm: false,
          preConfirm: () => {
    const electricity = Swal.getPopup().querySelector('#electricity').value
    if (!electricity) {
      Swal.showValidationMessage(`الرجاء ادخال جميع المعلومات المطلوبة`)
    }
    return {electricity:electricity}
    }
   
      }).then((result) => {
    if(result.isConfirmed){
    var electricity = document.createElement("input");
    electricity.value= `${result.value.electricity}`.trim();
    electricity.name="electricity";
    electricity.style.display="none";
    form.appendChild(electricity);
   form.submit();}
})
    return false;
    }
    
        </script>
    <script>
        function confirm3(quittance_id){
          console.log('hi');
          var form = document.getElementById(quittance_id);
          console.log(form);
          Swal.fire({
              title: "اضف تعليقك ",
              text: " لايمكنك التراجع",
              background:"var(--black-color)",
          color:"var(--white-color)",
              html: `<input type=number  id="electricity" class="swal2-input" placeholder="قراءة عداد الكهرباء " required">
              <textarea   id="missings" class="swal2-input" placeholder="النواقص " required style="height: 150px;width: 298px">`,
              showCancelButton: true,
              confirmButtonColor: "var(--move1-color)",
              confirmButtonText: "تم", 
              cancelButtonText: 'أغلاق',
              closeOnConfirm: false,
              preConfirm: () => {
        const electricity = Swal.getPopup().querySelector('#electricity').value
        const missings = Swal.getPopup().querySelector('#missings').value
        if (!electricity || !missings) {
      Swal.showValidationMessage(`الرجاء ادخال جميع المعلومات المطلوبة`)
    }
        return {electricity:electricity,missings:missings}
        }
       
          }).then((result) => {
        if(result.isConfirmed){
        var electricity = document.createElement("input");
        var missings=document.createElement("input");
        electricity.value= `${result.value.electricity}`.trim();
        missings.value=`${result.value.missings}`.trim();
        missings.name="missings";
        electricity.name="electricity";
        missings.style.display="none";
        electricity.style.display="none";
        form.appendChild(electricity);
        form.appendChild(missings);
       form.submit();}
    })
        return false;
        }
    </script>
        </div>
<?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>

</html><?php /**PATH G:\Programing\universtyHousing\resources\views/supervisor/quittance.blade.php ENDPATH**/ ?>