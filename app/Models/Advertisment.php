<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Advertisment extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table='advertisments';
    protected $primaryKey='id';
    protected $fillable=['message','addressed_to'];


}
