<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- framework css file -->
    <link rel="stylesheet" href="{{ asset('css/framework.css') }}">
    <!-- main template css file -->
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <!-- font awesome library -->
    <link rel="stylesheet" href="{{ asset('css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/advertisment.css') }}">
    <link rel="stylesheet" href="{{ asset('css/create-adv.css') }}">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    @livewireStyles
    <title>جامعة القلمون الخاصة</title>
</head>

<body>
    <div class="page d-flex ">
        <div class="sidebar  p-20 p-relative">
            <div class="logo">
                <img src="{{ asset('images/homoon.png') }}" alt="">          
          </div>
          <ul>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/student">
                    <i class="fa-solid fa-sliders fa-fw"></i>
                    <span>الصفحة الرئيسية</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/student/houses">
                    <i class="fa-solid fa-hotel fa-fw"></i>
                    <span> السكنات</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/student/myOrder">
                    <i class="fa-solid fa-heart"></i>
                    <span>  طلباتي </span>
                </a>
            </li>
            <li>
                <a class="active d-flex align-center fs-14  rad-6 p-10 " href="/student/advertisments">

                    <i class="fa-solid fa-bullhorn"></i>
                    <span> الإعلانات </span>
                </a>
            </li>
            <li>
                
                <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">

                    <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                    <span>  تسجيل خروج</span>
                </a>
                <form method="POST" action="{{ route('logout') }}" id="my_form" style="display: none">
                    @csrf
                </form>
            </li>
        </ul>

        </div>
        <div class="content">
            <!-- start head -->
            <!-- end head -->
            <h1 class="p-relative"> الإعلانات </h1>
            <div class="wrapper d-grid gap-20 ">
                <div class="welcome bg-white rad-10 ">
                    <div class="intro p-20 d-flex space-between bg-eee">
                        <div class="latest_news p-20 bg-white rad-10 txt-c">
                            
                                @livewire('advirtesment')
                            



                        </div>


                    </div>
                </div>
            </div>

        </div>
    </div>
    @foreach  ($advetisments as $advertisment)
    <form method="GET" action="/student/advertisments/{{ $advertisment->id }}"   style="display: none" class="adv-form popupForm" id="{{ $advertisment->id}}">
        @csrf
        <h3> {{ $advertisment->title }}</h3>
        <button class="closeModal" onclick="colseForm('{{$advertisment->id}}')" type="button"> </button>
        <label for="username"  >إلى كافة الطلاب</label>
        <label for="username"  >{{ $advertisment->message }} </label>

    </form>
    @endforeach
    <script type="text/javascript">
        function openForm(id){
            document.getElementById(id).style.display = "block";
            document.body.id='popedup';
        }
        function colseForm(id){
          document.getElementById(id).style.display = "none";
            document.body.id='';
        }
    </script>
     @livewireScripts
</body>

</html>