<?php

namespace App\Http\Controllers\Shared;
use App\Http\Controllers\Controller;
use App\Models\Advertisment;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AdvertismentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->type=='supervisor')
        {
            $advertisments=Advertisment::where('addressed_to','=','supervisor')->orwhere('addressed_to','=','all')->latest()->get();
            return view('supervisor.advertisments')->with('advetisments',$advertisments);

        }
        elseif (auth()->user()->type=='student') {
            $advertisments=Advertisment::where('addressed_to','=','student')->orwhere('addressed_to','=','all')->latest()->get();   
            return view('student.advertisments')->with('advetisments',$advertisments);
        }
        else
        {
            $advertisments=Advertisment::latest()->get();
            return view('manager.advertisment')->with('advetisments',$advertisments);
        }

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $title=$request->input('title');
        $message=$request->input('message');
        $addresed_to=$request->input('addressed_to');
        $adverisment=new Advertisment();
        $adverisment->message=$message;
        $adverisment->addressed_to=$addresed_to;
        $adverisment->title=$title;
        $adverisment->save();
        return redirect()->route('manager.advertisments.index')->with('success',' تم اضافة الاعلان بنجاح');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  
    public function show(Advertisment $advertisment)
    {
        return view('manager.advertisment')->with('advertisments',$advertisment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit(Advertisment $advertisment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Advertisment $advertisment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 
    public function destroy(Advertisment $advertisment)
    {
        //
    }
}
