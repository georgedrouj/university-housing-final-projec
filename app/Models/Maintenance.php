<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use PHPUnit\Framework\MockObject\Builder\Stub;

class Maintenance extends Model
{
    use HasFactory;
    use SoftDeletes;
    use \Znck\Eloquent\Traits\BelongsToThrough;
    protected $table='maintenances';
    protected $primaryKey='id';
    protected $fillable=['maintenace_date','description','maintenace_state','contract_id','reject_reason','supervisor_comment'];
    public function getContract(){
        return $this->belongsTo(Contract::class);
    }
    public function getStudent(){
        return $this->belongsToThrough(Student::class,Contract::class);
    }
    public function getRoom(){
        return $this->belongsToThrough(Room::class,Contract::class);
    }
    public  function getHouse()
    {
    return $this->belongsToThrough(House::class,[Room::class,Contract::class]);
    }
 
}
