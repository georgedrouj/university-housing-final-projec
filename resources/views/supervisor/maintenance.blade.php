<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- framework css file -->
    <link rel="stylesheet" href="{{ asset('css/framework.css') }}">
    <!-- main template css file -->
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <!-- font awesome library -->
    <link rel="stylesheet" href="{{ asset('css/all.min.css') }}">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <script src="{{ asset('vendor/sweetalert/sweetalert.all.js') }}"></script>
   
    <!-- main template css file -->
    <link rel="stylesheet" href="{{ asset('css/orders.css')}}">
    <title>Document</title>
</head>

<body>
    <div class="page d-flex ">
        <div class="sidebar  p-20 p-relative">
            <div class="logo">
                  <img src="{{ asset('images/homoon.png') }}" alt="">          
            </div>
            <ul>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/">
                        <i class="fa-solid fa-sliders fa-fw " ></i>
                        <span>الصفحة الرئيسية</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/supervisor/quittance">

                        <i class="fa-solid fa-building"></i>
                        <span> وحدتي السكنية</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/supervisor/switch_room">

                        <i class="fa-solid fa-shuffle"></i>
                        <span>طلب تبديل غرفة </span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/supervisor/quittance">

                      
                        <i class="fa-solid fa-person-walking-arrow-right"></i>
                        <span> طلب براءة الذمة</span>
                    </a>
                </li>
                <li>
                    <a class="active d-flex align-center fs-14  rad-6 p-10 " href="/supervisor/maintenance">

                        <i class="fa-sharp fa-solid fa-wrench"></i>
                        <span> طلب  الصيانة</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/supervisor/advertisments">

                        <i class="fa-solid fa-bullhorn"></i>
                        <span> الإعلانات </span>
                    </a>
                </li>
                <li>
                   
                    <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">

                        <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                        <span>  تسجيل خروج</span>
                    </a>
                    <form method="POST" action="{{ route('logout') }}" id="my_form" style="display:none">
                        @csrf
                    </form>
                </li>

            </ul>

        </div>
        <div class="content">
            <!-- start head -->
            <!-- end head -->
            <h1 class="p-relative">الطلبات </h1>
            <div class="wrapper d-grid gap-20 ">
                <div class="welcome bg-white rad-10 ">
                    <div class="intro p-20 d-flex space-between bg-eee">
                        <div class="container">
                            <!-- section-success -->
                            @foreach ( $maintenances as $maintenance)
                        
                            <div class="wrapper-success">
                               
                                <div class="card">
                                   
                                    <div class="icon">
                                        <i class="fa-solid fa-bell"></i>
                                    </div>
                                  
                                    <div class="subject">
                                       
                                        <span> رقم الطالب: {{ $maintenance->getStudent->university_id }} </span>
                                        <span> رقم الغرفة: {{ $maintenance->getRoom->house_id." ".$maintenance->getRoom->room_name }}</span>
                                        <span>معلومات الطلب</span>
                                        <p>
                                            {{$maintenance->description }}      
                                        </p>
                                    </div>
                                    <div class="orderfooter">
                                
                                        <button  onclick="return confirm2({{ $maintenance->id }})" type="button">إضافة تعليق</button>
                                        <form method="POST" action="maintenance/{{ $maintenance->id }}" id="{{ $maintenance->id }}" style="display: none">
                                            @csrf
                                            @method('PUT')
                                    </form>
                                </div>
                                </div>
                                
                            </div>
                            @endforeach
                            <!-- section-success -->
                        </div>
                    </div>
                </div>

            </div>
     
       <script type="text/javascript">
        function confirm2(contract_id){
      
      var form = document.getElementById(contract_id);
      console.log(form);
      Swal.fire({
          title: "اضف تعليقك ",
          text: " لايمكنك التراجع",
          background:"var(--black-color)",
          color:"var(--white-color)",
          html: '<textarea  id="login" class="swal2-input" placeholder="التعليق" required style="height:100px">',
          showCancelButton: true,
          confirmButtonColor: "var(--move1-color)",
          confirmButtonText: "تم", 
          cancelButtonText: 'أغلاق',
          closeOnConfirm: false,
          preConfirm: () => {
    const login = Swal.getPopup().querySelector('#login').value
    return {login:login}
    }
   
      }).then((result) => {
    if(result.isConfirmed){
    var reason = document.createElement("input");
    reason.value= `${result.value.login}`.trim();
    reason.name="description";
    reason.style.display="none";
    form.appendChild(reason);
   form.submit();}
})
    return false;
    }
        </script>
        

        </div>
@include('sweetalert::alert')
</body>

</html>