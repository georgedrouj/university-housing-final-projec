<?php

namespace App\Http\Controllers\Shared;

use App\Http\Controllers\Controller;
use App\Models\Contract;
use App\Models\Quittance;
use App\Models\Supervisor;
use App\Models\SwitchRoom;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SwitchRoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->type=='manager'){
            $switchRooms=SwitchRoom::where('switch_room_state','=','in_manager')->get();
        
            return view('manager.switch_room')->with(['switchRooms'=>$switchRooms]);
        }
        else if(auth()->user()->type=='supervisor'){

            $supervisor=auth()->user()->getSupervisors->first();
            $house=$supervisor->getHouse;
            $switchRooms=$house->getSwitchRooms->where('switch_room_state','=','in_supervisor');
            return view('supervisor.switch_room')->with(['switchRooms'=>$switchRooms]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$room_id)
    {
        $student_id=Auth()->user()->user_id;
        $contract=Contract::where([['student_id','=',$student_id],['contract_state','=','active_now']])->first();
        if($contract->room_id==$room_id){
            return redirect()->back()->with('errors','لا يمكنك التبديل على نفس الغرفة');
        
        }
        $switchRoom=new SwitchRoom();
        $switchRoom->switch_room_state='in_supervisor';
        $switchRoom->contract_id=$contract->id;
        $switchRoom->new_room_id=$room_id;
        $switchRoom->save();
        return redirect()->back()->with('success','تمت العملية بنجاح');
     
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SwitchRoom  $switchRoom
     * @return \Illuminate\Http\Response
     */
    public function show(SwitchRoom $switchRoom)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SwitchRoom  $switchRoom
     * @return \Illuminate\Http\Response
     */
    public function edit(SwitchRoom $switchRoom)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SwitchRoom  $switchRoom
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SwitchRoom $switchRoom)
    {
  
        if(Auth()->user()->type=='supervisor'){
          
          $switchRoom->missings=$request->input('missings');
          $switchRoom->electricity=$request->input('electricity');
           $switchRoom->switch_room_state='in_manager';
           $switchRoom->save();
        }
        else{
           
            $acc=$request->input('ok');
        if($acc=="true"){
        
        $switchRoom->manager_comment=$request->input('manager_comment');
        $switchRoom->switch_room_state='accepted';
        $switchRoom->save();
        $quittance=new Quittance();
        $quittance->quittance_state='accepted';
        $quittance->contract_id=$switchRoom->contract_id;
        $quittance->quittance_date=Carbon::now();
        $quittance->electricity=$switchRoom->electricity;
        $quittance->missings=$switchRoom->missings;
        $quittance->manager_comment=$switchRoom->manager_comment;
        $quittance->save();
        $contract=new Contract();
        $contract->contract_state='active_now';
        $contract->room_entery_date=Carbon::now();
        $oldContract=Contract::where('id','=',$switchRoom->contract_id)->get()->first();
        $contract->student_id=$oldContract->student_id;
        $contract->room_id=$switchRoom->new_room_id;
        $contract->save();
        $oldContract->contract_state="deleted";
        $oldContract->save();
        $oldContract->delete();
       
        
        }
        else{
        $switchRoom->switch_room_state='rejected';
        $switchRoom->manager_comment=$request->input('manager_comment');
        $switchRoom->save();
        } 
      
        }
        return redirect()->back()->with('success','تمت العملية بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SwitchRoom  $switchRoom
     * @return \Illuminate\Http\Response
     */
    public function destroy(SwitchRoom $switchRoom)
    {
        //
    }
}
