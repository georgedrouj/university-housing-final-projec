<div>
    <input wire:model.debounce.500ms="searchQuery" type="text" placeholder="بحث" >
    <select wire:model="type">
        <option value="" selected hidden> اختر نوع الطلب </option>
        <option value=""> الكل </option>
        <option value="صيانة"> صيانة</option>
        <option value="براءة ذمة"> براءة ذمة</option>
        <option value="تبديل"> تبديل</option>

    </select>
    <div class="container">
  
        <table>
            <thead>
                <tr>
                    <th> رقم التسلسلي للطلب</th>
                    <th> نوع الطلب</th>
                    <th>حالة الطلب</th>
                    <th> تاريخ الطلب </th>
                </tr>
            </thead>
          
            <tbody>
                @foreach ($orders as $order)
                <tr>
                    <td>{{ $order['order_id'] }}</td>
                    <td> {{ $order['name']}}</td>
                    <td>{{ $order['order_state'] }}</td>
                    <td>{{ Carbon\Carbon::parse($order['created_at'])->format('Y-m-d') }}</td>
                </tr>
                @endforeach
              
            </tbody>
        
        </table>
    </div>
</div>
