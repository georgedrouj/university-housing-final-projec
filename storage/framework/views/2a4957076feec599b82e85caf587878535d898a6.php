<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- framework css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/framework.css')); ?>">
    <!-- main template css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="<?php echo e(asset('css/normalize.css')); ?>">
    <!-- font awesome library -->
    <link rel="stylesheet" href="<?php echo e(asset('css/all.min.css')); ?>">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <!-- main template css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/orders.css')); ?>">
    <title>Document</title>
</head>

<body>
    <div class="page d-flex ">
        <div class="sidebar  p-20 p-relative">
            <h3 class="p-relative txt-c mt-0">
                النبراس
            </h3>
            <ul>
                <li>
                    <a class="d-flex align-center fs-14  rad-6 p-10 " href="/">
                        <i class="fa-solid fa-sliders fa-fw"></i>
                        <span>الصفحة الرئيسية</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/houses">
                        <i class="fa-solid fa-hotel fa-fw"></i>
                        <span>السكنات </span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/import">

                        <i class="fa-solid fa-people-line fa-fw"></i>
                        <span> الطلاب</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="confimation.html">

                        <i class="fa-brands fa-readme fa-fw"></i>
                        <span> طلب التثبيت</span>
                    </a>
                </li>
                <li>
                    <a class="active d-flex align-center fs-14  rad-6 p-10 " href="/manager/contract">

                        <i class="fa-brands fa-readme fa-fw"></i>
                        <span> طلب التسجيل</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="quitence.html">

                        <i class="fa-brands fa-readme fa-fw"></i>
                        <span> طلب براءة الذمة</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="mantinance.html">

                        <i class="fa-brands fa-readme fa-fw"></i>
                        <span> طلب  الصيانة</span>
                    </a>
                </li>
                <li>
                    <form method="POST" action="<?php echo e(route('logout')); ?>" id="my_form">
                        <?php echo csrf_field(); ?>
                    <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">

                        <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                        <span>  تسجيل خروج</span>
                    </a>
                    </form>
                </li>

            </ul>

        </div>
        <div class="content">
            <!-- start head -->
            <!-- end head -->
            <h1 class="p-relative">الطلبات </h1>
            <div class="wrapper d-grid gap-20 ">
                <div class="welcome bg-white rad-10 ">
                    <div class="intro p-20 d-flex space-between bg-eee">
                        <?php $__currentLoopData = $contracts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $contract): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            
                       
                        <div class="Message" id="js-timer">
                            <div class="Message-icon">
                                <i class="fa-sharp fa-solid fa-bell"></i>
                            </div>
                            <div class="Message-body">
                                <span>رقم الطالب:<?php echo e($contract->student_id); ?></span>
                                <span> رقم الغرفة:<?php echo e($contract->getHouse->id." ".$contract->room_id); ?> </span>
                                <span> 
               
                                <p>
                                 معلومات الطلب: أودالتسجيل على الغرفة
                               </p> 
                        </span>
                        <form method="POST" action="contract/<?php echo e($contract->id); ?>" id="yes">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('PUT'); ?>
                            <input name="ok" style="display: none" value="true" >
                                <button class="btn"> <a href="javascript:{}" onclick="return confirm()"> موافق<i ></i> </a>
                        </button>
                    </form>
                    <form method="POST" action="contract/<?php echo e($contract->id); ?>" id="no">
                        <?php echo csrf_field(); ?>
                        <?php echo method_field('PUT'); ?>
                        <input name="ok" style="display: none" value="false" >
                            <button class="btn"> <a href="javascript:{}" onclick="return confirm2()" >غير موافق<i ></i> </a>
                    </button>
                </form>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                       
    













                    </div>
                </div>

            </div>
            <script type="text/javascript">
    function confirm(){
  
  var form = document.getElementById("yes");
  swal({
      title: "هل انت متأكد",
      text: " لايمكنك التراجع",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "green",
      confirmButtonText: "موافق", 
      closeOnConfirm: false
  }, function (isConfirmed) {
      if (isConfirmed) {
        console.log('hi');
          form.submit();
      }
  });
return false;
}
    </script>
       <script type="text/javascript">
        function confirm2(){
      
      var form = document.getElementById("no");
      swal({
          title: "هل انت متأكد",
          text: "لا يمكنك التراجع",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "red",
          confirmButtonText: "غير موافق", 
          closeOnConfirm: false
      }, function (isConfirmed) {
          if (isConfirmed) {
              form.submit();
          }
      });
    return false;
    }
        </script>
        </div>
<?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>

</html><?php /**PATH C:\Users\User\Desktop\university-housing-final-projec\resources\views/manager/contract.blade.php ENDPATH**/ ?>