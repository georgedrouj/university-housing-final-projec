<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;

class Users extends Component
{
  
    public $searchQuery;
    public $type;
    public function mount(){
       $this->searchQuery="";
     $this->type="";
    }
    public function render()
    {
    
        $users=User::where('type','!=','student')->when($this->searchQuery!='',
        function ($query){
            $query->where('name','like','%'.$this->searchQuery.'%')->orWhere('user_id','like','%'.$this->searchQuery.'%');
        })->when($this->type!='',
        function($query){
            $query->where('type','=',$this->type);
        })->get();
        return view('livewire.users',['users'=>$users]);
    }
}
