<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- framework css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/framework.css')); ?>">
    <!-- main template css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="<?php echo e(asset('css/normalize.css')); ?>">
    <!-- font awesome library -->
    <link rel="stylesheet" href="<?php echo e(asset('css/all.min.css')); ?>">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <script src="<?php echo e(asset('vendor/sweetalert/sweetalert.all.js')); ?>"></script>
   
    <!-- main template css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/orders.css')); ?>">
    <title>Document</title>
</head>

<body>
    <div class="page d-flex ">
        <div class="sidebar  p-20 p-relative">
            <div class="logo">
                <img src="<?php echo e(asset('images/homoon.png')); ?>" alt="">          
          </div>
          <ul>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/">
                    <i class="fa-solid fa-sliders fa-fw " ></i>
                    <span>الصفحة الرئيسية</span>
                </a>
            </li>
            <li>
                <a class="d-flex align-center fs-14  rad-6 p-10 " href="/manager/houses">
                    <i class="fa-solid fa-hotel fa-fw"></i>
                    <span>السكنات </span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/students">

                    <i class="fa-solid fa-people-line fa-fw"></i>
                    <span> الطلاب</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/supervisor">

                    <i class="fa-solid fa-users"></i>
                    <span>مشرفي الوحدات السكنية</span>
                </a>
            </li>
        
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/contract">

                    <i class="fa-brands fa-readme fa-fw"></i>
                    <span> طلب التسجيل</span>
                </a>
            </li>
           
            <li>
                <a class="active d-flex align-center fs-14  rad-6 p-10 " href="/manager/switch_room">

                    <i class="fa-solid fa-shuffle"></i>
                    <span> طلب التبديل</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/quittance">

                    <i class="fa-solid fa-person-walking-arrow-right"></i>
                    <span> طلب براءة الذمة</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/maintenance">

                    <i class="fa-sharp fa-solid fa-wrench"></i>
                    <span> طلب  الصيانة</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="comfirmation.html">

                    <i class="fa-solid fa-circle-check"></i>
                    <span> طلب التثبيت</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="comfirmation.html">

                    <i class="fa-solid fa-bullhorn"></i>
                    <span> الإعلانات </span>
                </a>
            </li>
            <li>
              
                <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">

                    <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                    <span>  تسجيل خروج</span>
                </a>
                <form method="POST" action="<?php echo e(route('logout')); ?>" id="my_form" style="display: none">
                    <?php echo csrf_field(); ?>
                </form>
            </li>

        </ul>

        </div>
        <div class="content">
            <!-- start head -->
            <!-- end head -->
            <h1 class="p-relative">الطلبات </h1>
            <div class="wrapper d-grid gap-20 ">
                <div class="welcome bg-white rad-10 ">
                    <div class="intro p-20 d-flex space-between bg-eee">
                        <div class="container">
                            <!-- section-success -->
                            <?php $__currentLoopData = $switchRooms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $switchRoom): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="wrapper-success">
                               
                                <div class="card">
                                   
                                    <div class="icon">
                                        <i class="fa-solid fa-bell"></i>
                                    </div>
                                  
                                    <div class="subject">
                                        
                                        <span> رقم الطالب: <?php echo e($switchRoom->getStudent->university_id); ?> </span>
                                        <span> رقم الغرفة: <?php echo e($switchRoom->getRoom->house_id." ".$switchRoom->getRoom->room_name); ?></span>
                                        <span> رقم الغرفة الجديدة: <?php echo e($switchRoom->getNewRoom->house_id." ".$switchRoom->getNewRoom->room_name); ?></span>
                                        <span>معلومات الطلب</span>
                                        <p>
                                            ارجو الموافقة عل طلب التبديل في هذه الغرفة
                                        </p>
                                        <span>
                                            النواقص
                                        </span>
                                        <?php if(isset($switchRoom->missings)): ?>
                                        <p>
                                            <?php echo e($switchRoom->missings); ?>

                                        </p>
                                        <?php else: ?>
                                        <p>
                                            لايوجد
                                        </p>
                                        <?php endif; ?>

                                        <span>
                                            قراءة عداد الكهرباء
                                        </span>
                                        <p>
                                            <?php echo e($switchRoom->electricity); ?>

                                        </p>
                                    </div>
                                    <div class="orderfooter">
                                        <?php if(isset($switchRoom->missings)): ?>
                                        <button onclick="return  confirm3(<?php echo e($switchRoom->id); ?>)" type="button">موافق</button>
                                        <?php else: ?>
                                    <button onclick="return  confirm(<?php echo e($switchRoom->id); ?>)" type="button">موافق</button>
                                    <?php endif; ?>
                                    <button  onclick="return confirm2(<?php echo e($switchRoom->id); ?> )" type="button">غير موافق</button>
                                    <form method="POST" action="switch_room/<?php echo e($switchRoom->id); ?>" id="<?php echo e($switchRoom->id); ?>" style="display: none">
                                        <?php echo csrf_field(); ?>
                                        <?php echo method_field('PUT'); ?>
                                </form>
                                </div>
                                </div>
                                
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <!-- section-success -->
                        </div>
                    </div>
                </div>

            </div>
            <script type="text/javascript">
    function confirm(switcht_room_id){
  console.log('hi');
  var form = document.getElementById(switcht_room_id);
  Swal.fire({
      title: "هل انت متأكد",
      text: " لايمكنك التراجع",
  
      showCancelButton: true,
      confirmButtonColor: "var(--green-color)",
      confirmButtonText: "تم", 
      cancelButtonText: 'أغلاق',
      closeOnConfirm: false
  }).then((result) => {
  /* Read more about isConfirmed, isDenied below */
  if (result.isConfirmed) {
    var ok = document.createElement("input");
    ok.value="true";
    ok.name="ok";
    ok.style.display="none";
    form.appendChild(ok);
   form.submit();
  } 
});
return false;
}
    </script>
       <script type="text/javascript">
        function confirm2(switcht_room_id){
      
      var form = document.getElementById(switcht_room_id);
      console.log(form);
      Swal.fire({
          title: "هل انت متأكد",
          text: " لايمكنك التراجع",
          html: '<input type="text" id="manager_comment" class="swal2-input" placeholder="سبب الرفض" required>',
          showCancelButton: true,
          confirmButtonColor: "var(--red-color)",
          confirmButtonText: "تم", 
          cancelButtonText: 'أغلاق',
          closeOnConfirm: false,
          preConfirm: () => {
    const manager_comment = Swal.getPopup().querySelector('#manager_comment').value
    if (!manager_comment) {
      Swal.showValidationMessage(`الرجاء ادخال جميع المعلومات المطلوبة`)
    }
    return {manager_comment:manager_comment}
    }
   
      }).then((result) => {
    if(result.isConfirmed){
    var ok = document.createElement("input");
    ok.value="false";
    ok.name="ok";
    ok.style.display="none";
    form.appendChild(ok);
    var manager_comment = document.createElement("input");
    manager_comment.value= `${result.value.manager_comment}`.trim();
    manager_comment.name="manager_comment";
    manager_comment.style.display="none";
    form.appendChild(manager_comment);
   form.submit();}
})
    return false;
    }
        </script>
        
        <script type="text/javascript">
            function confirm3(switcht_room_id){
          
          var form = document.getElementById(switcht_room_id);
          console.log(form);
          Swal.fire({
              title: "هل انت متأكد",
              text: " لايمكنك التراجع",
              html: '<input type="number" id="manager_comment" class="swal2-input" placeholder="قيمة النواقص" required>',
              showCancelButton: true,
              confirmButtonColor: "var(--green-color)",
              confirmButtonText: "تم", 
              cancelButtonText: 'أغلاق',
              closeOnConfirm: false,
              preConfirm: () => {
        const manager_comment = Swal.getPopup().querySelector('#manager_comment').value
        if (!manager_comment) {
      Swal.showValidationMessage(`الرجاء ادخال جميع المعلومات المطلوبة`)
    }
        return {manager_comment:manager_comment}
        }
       
          }).then((result) => {
        if(result.isConfirmed){
        var ok = document.createElement("input");
        ok.value="true";
        ok.name="ok";
        ok.style.display="none";
        form.appendChild(ok);
        var manager_comment = document.createElement("input");
        manager_comment.value= `${result.value.manager_comment}`.trim();
        manager_comment.name="manager_comment";
        manager_comment.style.display="none";
        form.appendChild(manager_comment);
       form.submit();}
    })
        return false;
        }
            </script>
        </div>
<?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>

</html><?php /**PATH G:\Programing\universtyHousing\resources\views/manager/switch_room.blade.php ENDPATH**/ ?>