<?php

namespace App\Http\Controllers\Shared;
use App\Http\Controllers\Controller;
use App\Models\Contract;
use App\Models\Maintenance;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MaintenanceController extends Controller
{
    /**
     * Display a listing of the resource.
     
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->type=='supervisor'){
            $supervisor=auth()->user()->getSupervisors->first();
            $house=$supervisor->getHouse; 
            $maintenances=$house->getMaintenanaces()->where('maintenace_state','=','in_supervisor')->get();
            return view('supervisor.maintenance',['maintenances'=>$maintenances]);
        }
        else{

            $maintenances=Maintenance::where(['maintenace_state'=>'in_manager'])->get();
            return view('manager.maintenance_requests',['maintenances'=>$maintenances]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('student.maintenance-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $student_id=Auth()->user()->user_id;
        $contract_id=Contract::where('student_id','=',$student_id)->where('contract_state','=','active_now')->first();
        $maintenance=new Maintenance();
        $maintenance->maintenace_state='in_supervisor';
       // $maintenance->description='رقم الطالب'.$student_id.PHP_EOL.$request->input('description');
        $maintenance->description=$request->input('description');
        $maintenance->contract_id=$contract_id->id;
        $maintenance->maintenace_date=Carbon::now();
        $maintenance->save();
        return redirect()->route('student.order')->with('success','تم إرسال طلب الصيانة بنجاح');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Maintenance  $maintenance
     * @return \Illuminate\Http\Response
     */
    public function show(Maintenance $maintenance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Maintenance  $maintenance
     * @return \Illuminate\Http\Response
     */
    public function edit(Maintenance $maintenance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Maintenance  $maintenance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Maintenance $maintenance)
    {
        if(Auth()->user()->type=='supervisor'){
            $maintenance->supirvisor_comment=$request->input('description');
            $maintenance->maintenace_state='in_manager';
            $maintenance->save();
        }
        else{
            $acc=$request->input('ok');
        if($acc==true){
         $maintenance->maintenace_state='accepted';
         $maintenance->save();
        }
        else{
         $maintenance->maintenace_state='rejected';
         $maintenance->reject_reason=$request->input('reject_reason');
         $maintenance->save();
        } 
        }
        return redirect()->back()->with('success','تمت العملية بنجاح');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Maintenance  $maintenance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Maintenance $maintenance)
    {
        //
    }
}
