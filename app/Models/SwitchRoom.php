<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SwitchRoom extends Model
{
    use HasFactory;
    use SoftDeletes;
    use \Znck\Eloquent\Traits\BelongsToThrough;
    protected $table='switch_rooms';
    protected $fillable=['contract_id','new_room_id','switch_room_state','manager_comment','missings','electricity'];
    public function getContract(){
        return $this->belongsTo(Contract::class);
    }
    public function getStudent(){
        return $this->belongsToThrough(Student::class,Contract::class);
    }
    public function getRoom(){
        return $this->belongsToThrough(Room::class,Contract::class);
    }
    public function getNewRoom(){
        return $this->belongsTo(Room::class,'new_room_id');
    }
    public  function getHouse()
    {
    return $this->belongsToThrough(House::class,[Room::class,Contract::class]);
    }
}
