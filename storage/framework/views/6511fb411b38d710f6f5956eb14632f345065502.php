<div>
    <input wire:model.debounce.500ms="searchQuery" type="text" placeholder="بحث" class="input">
    <select wire:model="orderQuery">
    <option value="">ترتيب حسب</option>
    <option value="price">سعر</option>
    <option value="space">مساحة</option>
    <option value="capacity">سعة</option>
    </select>
    <select wire:model="emptyQuery">
    <option value="">شواغر الغرفة</option>
    <option value="full">ممتلئة</option>
    <option value="empty">فارغة تماماً</option>
    <option value="half empty">فارغة جزئياً</option>
    </select>
    <div class="wrapper d-flex gap-20 ">
        <div class="intro p-20   bg-eee">
            <?php if(Auth()->user()->type=='student'): ?>

            <?php $__currentLoopData = $rooms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $room): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if($room->getEmpty() >0): ?>
            <div class="container">
                <div class="product">
                    <div class="info">
                        <h2> <?php echo e($room->room_name); ?></h2>
                        <ul>
                            <li><strong>المساحة : </strong><?php echo e($room->space); ?> </li>
                            <li><strong>السعة : </strong><?php echo e($room->capacity); ?></li>
                            <li><strong>الشواغر: </strong> <?php echo e($room->getEmpty()); ?></li>
                            <li><strong>الاتجاه: </strong><?php echo e($room->direction); ?></li>
                            <li><strong>السعر: </strong> <?php echo e($room->price); ?></li>
                        </ul>
                        <?php if(!$hasRoom): ?>

                        <form method="POST" action="/student/regestroom/<?php echo e($room->id); ?>" id="<?php echo e($room->id); ?>">
                            <?php echo csrf_field(); ?>
                            <button class="btn">
                                <a href="javascript:{}" onclick="confirm(<?php echo e($room->id); ?>)">
                                    تسجيل
                                </a>
                            </button>
                        </form>
                        <?php elseif(!$hasSwitchRoom && $hasActiveRoom): ?>
                        <form method="POST" action="/student/switch_room/<?php echo e($room->id); ?>" id="<?php echo e($room->id); ?>">
                            <?php echo csrf_field(); ?>
                            <button class="btn">
                                <a href="javascript:{}" onclick="confirm(<?php echo e($room->id); ?>)">
                                    تبديل
                                </a>
                            </button>
                        </form>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
            <?php if(Auth()->user()->type=='manager'): ?>
            <?php $__currentLoopData = $rooms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $room): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="container">
                <div class="product">
                    <div class="info">
                        <h2> <?php echo e($room->room_name); ?></h2>
                        <ul>
                            <li><strong>المساحة : </strong><?php echo e($room->space); ?> </li>
                            <li><strong>السعة : </strong><?php echo e($room->capacity); ?></li>
                            <li><strong>الشواغر: </strong> <?php echo e($room->getEmpty()); ?></li>
                            <li><strong>الاتجاه: </strong><?php echo e($room->direction); ?></li>
                            <li><strong>السعر: </strong> <?php echo e($room->price); ?></li>
                        </ul>
                        <?php if($room->blocked_now==0): ?>
                        <button class="btn" onclick="confirm3('<?php echo e($room->id); ?>')" type="submit">حجب </button>
                        <form method="POST" action="<?php echo e(route('manager.block',$room->id)); ?>" id="<?php echo e($room->id); ?>">
                            <?php echo csrf_field(); ?>
                        </form>
                        <?php else: ?>
                        <button class="btn" type="submit"> <a href="javascript:{}"
                                onclick="confirm2('<?php echo e($room->id); ?>')">إلغاء الحجب </a></button>
                        <form method="POST" action="<?php echo e(route('manager.unblock',$room->id)); ?>" id="<?php echo e($room->id); ?>">
                            <?php echo csrf_field(); ?>
                        </form>
                        <?php endif; ?>
                        <button class="btn"> <a href="">عقود </a>
                        </button>
                        <button class="btn"> <a
                                href="<?php echo e(request()->segment(count(request()->segments()))); ?>/<?php echo e($room->id); ?>">
                                تسجيل</a>
                        </button>
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
            <?php if(Auth()->user()->type=='admin'): ?>
            <?php $__currentLoopData = $rooms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $room): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="container">
                <div class="product">
                    <div class="info">
                        <h2> <?php echo e($room->room_name); ?></h2>
                        <ul>
                            <li><strong>المساحة : </strong><?php echo e($room->space); ?> </li>
                            <li><strong>السعة : </strong><?php echo e($room->capacity); ?></li>
                            <li><strong>الشواغر: </strong> <?php echo e($room->getEmpty()); ?></li>
                            <li><strong>الاتجاه: </strong><?php echo e($room->direction); ?></li>
                            <li><strong>السعر: </strong> <?php echo e($room->price); ?></li>
                        </ul>
                        <button class="btn" title="تعديل"> <a href="javascript:{}"
                                onclick="openForm('<?php echo e($room->id); ?>')"><i class="fa-solid fa-pen"></i> </a>
                        </button>
                        <button class="btn remove-all-styles"  title="حذف"> <a href="javascript:{}"
                                onclick="confirm('<?php echo e($room->id); ?>d')"><i
                                    class="fa-solid fa-trash remove-all-styles"></i></a>
                            <form id="<?php echo e($room->id); ?>d" method="POST" action="/admin/rooms/<?php echo e($room->id); ?>"
                                class="remove-all-styles ">
                                <?php echo csrf_field(); ?>
                                <?php echo method_field('DELETE'); ?>
                            </form>
                        </button>
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <div class="container">
                <div class="product-add">
                    <div class="info-add">
                        <button class="btn-add" title="إضافة غرفة"> <a href="javascript:{}"
                                onclick="openForm('add-form')"><i class="fa-solid fa-plus"></i> </a></button>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div><?php /**PATH G:\Programing\universtyHousing\resources\views/livewire/rooms.blade.php ENDPATH**/ ?>