<div>
    <input wire:model.debounce.500ms="searchQuery" type="text" placeholder="بحث" >
    <?php if(auth()->user()->type=='manager'): ?>
    <select wire:model="addressed_to">
        <option value="" selected hidden> اختر  الجهة المرسل لها </option>
        <option value="">الكل</option>
        <option value="all"> الطلاب والمشرفين </option>
        <option value="student"> الطلاب</option>
        <option value="supervisor"> المشرفين </option>
    </select>
    <?php endif; ?>
    <div class="welcome bg-white rad-10 ">
        <div class="intro p-20 d-flex space-between bg-eee">
            <div class="latest_news p-20 bg-white rad-10 txt-c">
                <div class="container">
                    <?php if(auth()->user()->type=='manager'): ?>
                    <div class="title">
                        <h2> الإعلانات</h2>
                         <button class="ads-plus" onclick="openForm('adv-form')" > 
                             إعلان جديد
                            <i class="fa-solid fa-plus "></i>
                        </button>
                    </div>
                    <?php $__empty_1 = true; $__currentLoopData = $advetisments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $advertisment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <div class="list" onclick="openForm('<?php echo e($advertisment->id); ?>')">
                        <div class="person">
                            <div class="name">
                                <?php if($advertisment->addressed_to=='all'): ?>
                                <img src="<?php echo e(asset('images/319-3192009_team-group-of-people-animated-png-transparent-png.png')); ?>" alt="">
                                <?php elseif($advertisment->addressed_to=='student'): ?>
                                <img src="<?php echo e(asset('images/IMG-20230125-WA0029.jpg')); ?>" alt="">
                                <?php elseif($advertisment->addressed_to=='supervisor'): ?>
                                <img src="<?php echo e(asset('images/IMG-20230125-WA0037.jpg')); ?>" alt="">
                                <?php endif; ?>
                                <a href="javascript:{}" ><?php echo e($advertisment->title); ?> </a>
                            </div>
                            <div class="net-worth">
                                <span><?php echo e($advertisment->created_at->shortRelativeDiffForHumans()); ?></span>
                            </div>
                        </div>
                    </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <span> لايوجد أي إعلانات حالياً</span>
                    <?php endif; ?>
                   <?php endif; ?>


                   <?php if(auth()->user()->type=='student'): ?>
                   <div class="title">
                    <h2> الإعلانات</h2>
                </div>
                <?php $__empty_1 = true; $__currentLoopData = $advetisments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $advertisment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <div class="list"  onclick="openForm('<?php echo e($advertisment->id); ?>')">
                    <div class="person">
                        <div class="name">
                            <?php if($advertisment->addressed_to=='all'): ?>
                            <img src="<?php echo e(asset('images/319-3192009_team-group-of-people-animated-png-transparent-png.ifif')); ?>" alt="">
                            <?php else: ?>
                            <img src="<?php echo e(asset('images/IMG-20230125-WA0029.jpg')); ?>" alt="">
                            <?php endif; ?>
                            <a href="javascript:{}"><?php echo e($advertisment->title); ?> </a>
                        </div>
                        <div class="net-worth">
                            <span><?php echo e($advertisment->created_at->shortRelativeDiffForHumans()); ?></span>
                        </div>
                    </div>
                </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <span> لايوجد أي إعلانات حالياً</span>
                <?php endif; ?>

                   <?php endif; ?>
                   <?php if(auth()->user()->type=='supervisor'): ?>
                                <div class="title">
                                    <h2> الإعلانات</h2>
                                </div>
                                <?php $__empty_1 = true; $__currentLoopData = $advetisments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $advertisment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                <div class="list"  onclick="openForm('<?php echo e($advertisment->id); ?>')">
                                    <div class="person">
                                        <div class="name">
                                            <?php if($advertisment->addressed_to=='all'): ?>
                                            <img src="<?php echo e(asset('images/319-3192009_team-group-of-people-animated-png-transparent-png.ifif')); ?>" alt="">
                                            <?php else: ?>
                                            <img src="<?php echo e(asset('images/IMG-20230125-WA0037.jpg')); ?>" alt="">
                                            <?php endif; ?>
                                            <a href="javascript:{}"><?php echo e($advertisment->title); ?> </a>
                                        </div>
                                        <div class="net-worth">
                                            <span><?php echo e($advertisment->created_at->shortRelativeDiffForHumans()); ?></span>
                                        </div>
                                    </div>
                                </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                    <span> لايوجد أي إعلانات حالياً</span>
                                <?php endif; ?>
                   <?php endif; ?>
                </div>



            </div>


        </div>
    </div>
</div>
<?php /**PATH G:\Programing\universtyHousing\resources\views/livewire/advirtesment.blade.php ENDPATH**/ ?>