<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo e(asset('css/create-house.css')); ?>">
    <title></title>
</head>

<body>
    <form method="POST" action="<?php echo e(route('admin.house.update',$house->id)); ?>" >
        <?php echo csrf_field(); ?>
        <?php echo method_field('PUT'); ?>
        <h3> تعديل سكن</h3>
        <label for="housename">اسم السكن</label>
        <input type="text" placeholder="اسم السكن" name="house_id" value="<?php echo e($house->id); ?>" required>

        <select  name="gender" class="gender" value="<?php echo e($house->gender); ?>"> 
            <option value="اختر جنس السكن">اختر جنس السكن </option>
            <option value="male">ذكر</option>
            <option value="female">أنثى</option>
        </select>
        <button type="submit">تعديل</button>

    </form>
</body>

</html><?php /**PATH C:\Users\User\Desktop\university-housing-final-projec\resources\views/admin/house/edit.blade.php ENDPATH**/ ?>