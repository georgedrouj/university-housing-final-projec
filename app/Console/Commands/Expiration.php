<?php

namespace App\Console\Commands;

use App\Models\Contract;
use Carbon\Carbon;
use Illuminate\Console\Command;

class Expiration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'contract:expires';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $contracts=Contract::where([['contract_state','=','in_progress']])->get();
        foreach($contracts as $contract){
            if(Carbon::now()->diffInDays($contract->created_at)>=2){
                $contract->contract_state="rejected";
                $contract->reject_reason="لم يتم التثبيت المالي خلال يومين";
                $contract->save();
                $contract->delete();
            }
        }
    }
}
