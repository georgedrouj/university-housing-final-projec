<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- framework css file -->
    <link rel="stylesheet" href="{{ asset('css/framework.css') }}">
    <!-- main template css file -->
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <!-- font awesome library -->
    <link rel="stylesheet" href="{{ asset('css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/advertisment.css') }}">
    <link rel="stylesheet" href="{{ asset('css/create-adv.css') }}">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <script src="{{ asset('vendor/sweetalert/sweetalert.all.js') }}"></script>
    @livewireStyles
    <title>جامعة القلمون الخاصة</title>

</head>

<body>
   
    <div class="page d-flex ">
        <div class="sidebar  p-20 p-relative">
            <div class="logo">
                <img src="{{ asset('images/homoon.png') }}" alt="logo">
            </div>
            <ul>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/">
                        <i class="fa-solid fa-sliders fa-fw " ></i>
                        <span>الصفحة الرئيسية</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/houses">
                        <i class="fa-solid fa-hotel fa-fw"></i>
                        <span>السكنات </span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/students">

                        <i class="fa-solid fa-people-line fa-fw"></i>
                        <span> الطلاب</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/supervisor">

                        <i class="fa-solid fa-users"></i>
                        <span>مشرفي الوحدات السكنية</span>
                    </a>
                </li>
            
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/contract">

                        <i class="fa-brands fa-readme fa-fw"></i>
                        <span> طلب التسجيل</span>
                    </a>
                </li>
               
                <li>
                    <a class="d-flex align-center fs-14  rad-6 p-10 " href="/manager/switch_room">

                        <i class="fa-solid fa-shuffle"></i>
                        <span> طلب التبديل</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/quittance">

                        <i class="fa-solid fa-person-walking-arrow-right"></i>
                        <span> طلب براءة الذمة</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/maintenance">

                        <i class="fa-sharp fa-solid fa-wrench"></i>
                        <span> طلب  الصيانة</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="comfirmation.html">

                        <i class="fa-solid fa-circle-check"></i>
                        <span> طلب التثبيت</span>
                    </a>
                </li>
                <li>
                    <a class="active d-flex align-center fs-14  rad-6 p-10 " href="/manager/advertisments">

                        <i class="fa-solid fa-bullhorn"></i>
                        <span> الإعلانات </span>
                    </a>
                </li>
                <li>
                  
                    <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">

                        <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                        <span>  تسجيل خروج</span>
                    </a>
                    <form method="POST" action="{{ route('logout') }}" id="my_form" style="display: none">
                        @csrf
                    </form>
                </li>

            </ul>

        </div>
        
        <div class="content">
            <!-- start head -->
            <!-- end head -->
            <h1 class="p-relative"> الإعلانات </h1>
            <div class="wrapper d-grid gap-20 ">
               @livewire('advirtesment')
            </div>

        </div>
    </div>
    @foreach  ($advetisments as $advertisment)
    <form method="GET" action="/manager/advertisments/{{ $advertisment->id }}"   style="display: none; background-color: white" class="adv-form popupForm" id="{{ $advertisment->id}}" >
        @csrf
        <h3 style="color: black"> {{ $advertisment->title }}</h3>
        <button class="closeModal"  onclick="colseForm('{{$advertisment->id}}')" type="button"> </button>
        @if ($advertisment->addressed_to=='all')
        <label for="username"  style="color: black">إلى كافة الطلاب والمشرفين</label>
        @elseif($advertisment->addressed_to=='student')
        <label for="username" style="color: black" >إلى كافة الطلاب </label>
        @elseif($advertisment->addressed_to=='supervisor')
        <label for="username"  style="color: black">إلى كافةالمشرفين</label>
        @endif
        <label for="username" style="color: black" >{{ $advertisment->message }} </label>

    </form>
    @endforeach
    <form method="POST" action="{{ route('manager.advertisments.store') }}"  id="adv-form" style="display: none" class="adv-form popupForm">
        @csrf
        <h3> إضافة إعلان</h3>
        <button class="closeModal" onclick="colseForm('adv-form')" type="button"> </button>
        <label for="username"  >عنوان الإعلان</label>
        <input type="text" placeholder=" عنوان الإعلان"  name="title" required >
        <label for="username"  >محتوى الإعلان</label>
        <textarea placeholder=" محتوى الإعلان"  name="message" required class="message"></textarea>
        <label for="username"  >لمن موجه هذا الإعلان</label>
        <select class="addressed_to" name="addressed_to" > 
            <option value="all ">المشرفين والطلاب</option>
            <option value="student">الطلاب</option>
            <option value="supervisor">المشرفين</option>
        </select>
        <button class="adv-button" type="submit"> نشر الإعلان</button>
    </form>
    <script type="text/javascript">
        function openForm(id){
            document.getElementById(id).style.display = "block";
            document.body.id='popedup';
        }
        function colseForm(id){
          document.getElementById(id).style.display = "none";
            document.body.id='';
        }
    </script>
 @include('sweetalert::alert');
 @livewireScripts
</body>

</html>