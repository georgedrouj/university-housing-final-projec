<?php

namespace App\Http\Controllers\Shared;
use App\Http\Controllers\Controller;
use App\Models\House;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth()->user()->type=='student'){
            
         $houses=House::where('gender','=',Auth()->user()->gender)->get();
     
        return view('student.myhouse',compact('houses'));
        }
        elseif(Auth()->user()->type=='manager'){
            $houses=House::all();
            if(Auth()->user()->type=='manager'){
            return view('manager.hoses')->with('houses',$houses);}
            else{
                return view('admin.houses')->with('houses',$houses);
            }
        }
        elseif(Auth()->user()->type=='admin'){
            $houses=House::all();
            return view('admin.houses')->with('houses',$houses);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.house.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->Validate($request,[
            'house_id'=>['required','unique:houses,id'],
            'gender'=>'required'
        ],
        [
          'house_id.required'=>'يجب إدخال رقم السكن',
          'house_id.unique'=>'هذا السكن موجود مسبقاً',
          'gender.required'=>'يجب إدخال الجنس'
        ]
        );
        $house=new House();
        $house->id=$request->input('house_id');
        $house->gender=$request->input('gender');
        $house->save();
        return redirect()->route('admin.house.index')->with('success','تم إضافة السكن بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\House  $house
     * @return \Illuminate\Http\Response
     */
    public function show(House $house)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\House  $house
     * @return \Illuminate\Http\Response
     */
    public function edit(House $house)
    {
        return view('admin.house.edit')->with('house',$house);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\House  $house
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, House $house)
    {
        $house->update([
            'id' => $request->input('house_id'),
            'gender' => $request->input('gender')
        ]);
        return redirect()->back()->with('success','تمت العملية بنجاح');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\House  $house
     * @return \Illuminate\Http\Response
     */
    public function destroy(House $house)
    {
        $house->delete();
        return redirect()->back()->with('success','تمت العملية بنجاح');

    }

}
