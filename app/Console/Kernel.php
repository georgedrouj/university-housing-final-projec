<?php

namespace App\Console;

use App\Console\Commands\Confirmation;
use App\Models\Contract;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;
use PSpell\Config;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected $commands=[Confirmation::class];
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('confirmation:start')->daily()->when(Carbon::now(),Config('global.start_date'));
        $schedule->command('contract:expires')->everyTwoHours();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
