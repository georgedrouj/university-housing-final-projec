<?php

namespace App\Http\Livewire;

use App\Models\House;
use App\Models\User;
use Livewire\Component;

class Supervisors extends Component
{
    public $searchQuery;
    public $femaleHouses;
    public $maleHouses;
    public $houseQuery;
    public function mount(){
        $this->femaleHouses=House::where('gender','=','female')->get();
        $this->maleHouses=House::where('gender','=','male')->get();
        $this->searchQuery="";
    }
    public function render()
    {
        $supervisors=User::where('type','=','supervisor')->when(
            $this->searchQuery !="",function($query){
                $query->where('name','like','%'.$this->searchQuery.'%')->orWhere('user_id','like','%'.$this->searchQuery.'%');
            }
        )->get()->filter(function($item){
            if($this->houseQuery!=""){
            if($this->houseQuery=="without house"){
                return $item['house_id']==null;
            }
            else
            return $item['house_id']==$this->houseQuery;
          }
            else{
                return true;
            }
        });
        return view('livewire.supervisors',['supervisors'=>$supervisors]);
    }
}
