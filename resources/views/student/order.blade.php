<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- framework css file -->
    <link rel="stylesheet" href="{{ asset('css/framework.css') }}">
    <!-- main template css file -->
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <!-- font awesome library -->
    <link rel="stylesheet" href="{{ asset('css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/users.css') }}">


    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="{{ asset('vendor/sweetalert/sweetalert.all.js') }}"></script>
    @livewireStyles
    <title>جامعة القلمون الخاصة</title>
</head>

<body>
    <div class="page d-flex ">
        <div class="sidebar  p-20 p-relative">
            <div class="logo">
                <img src="{{ asset('images/homoon.png') }}" alt="">          
          </div>
          <ul>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/student">
                    <i class="fa-solid fa-sliders fa-fw"></i>
                    <span>الصفحة الرئيسية</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/student/houses">
                    <i class="fa-solid fa-hotel fa-fw"></i>
                    <span> السكنات</span>
                </a>
            </li>
            <li>
                <a class="active d-flex align-center fs-14  rad-6 p-10 " href="/student/myOrder">
                    <i class="fa-solid fa-heart"></i>
                    <span>  طلباتي </span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/student/advertisments">

                    <i class="fa-solid fa-bullhorn"></i>
                    <span> الإعلانات </span>
                </a>
            </li>
            <li>
                
                <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">

                    <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                    <span>  تسجيل خروج</span>
                </a>
                <form method="POST" action="{{ route('logout') }}" id="my_form" style="display: none">
                    @csrf
                </form>
            </li>
        </ul>

        </div>
        <div class="content">
            <!-- start head -->
            <!-- end head -->
            <h1 class="p-relative">طلباتي</h1>

            <div class="wrapper d-grid gap-20 ">
                
                <div class="welcome bg-white rad-10 ">
                    @if(!$hasOrder && $hasActiveRoom)
                    <select id="select" class="swal2-select" style="background-color: var(--black-color); height:50px;color:var(--white-color);hover:var(--move-color)" onchange=""> 
                        <option>أضافة طلب</option>
                       <option value="1"> صيانة </option>
                       <option value="2">براءة ذمة</option>
                       <option value="3">تبديل</option>
                    </select>
                
                    @endif
                    <form class="order-form" method="POST" action="{{ route('student.maintenance.store') }}" style="display: none" id="maintenance">
                        @csrf
                    </form>
                
             
              
          
                <form method="POST" action="{{ route('student.quittance.store') }}" id="quit"  style="display: none">
                @csrf
                
                </form>
                    <div class="intro p-20 d-flex space-between bg-eee">
                        @if ($hasActiveRoom)
               
                        <livewire:orders>
                        @else
                        <h3>نحن بانتظارك لأن تحصل على غرفة اولاً</h3>
                        @endif
                      
                    </div>
                </div>
            </div>
    @include('sweetalert::alert')
    <script>
        $("#select").change(function () {
  if ($(this).val() == 1) {
    confirm2();
  }
  else if($(this).val() == 2){
    confirm();
  }
  else if($(this).val() == 3){
 
    window.location.assign('/student/houses');
  }
});
    </script>
    <script>
        function confirm2(){
      
      var form = document.getElementById("maintenance");
      console.log(form);
      Swal.fire({
          title: " اضف معلومات الطلب ",
          background:"var(--black-color)",
          color:"var(--white-color)",
          html: `   <textarea   id="description" class="swal2-input" placeholder="معلومات الطلب" required style="height: 150px;width: 298px">`,
          showCancelButton: true,
          confirmButtonColor: "var(--move-color)",
          confirmButtonText: "تم", 
          cancelButtonText: 'أغلاق',
          closeOnConfirm: false,
          preConfirm: () => {
    const description = Swal.getPopup().querySelector('#description').value
    if (!description) {
      Swal.showValidationMessage(`الرجاء ادخال جميع المعلومات المطلوبة`)
    }
    return {description:description}
    }
   
      }).then((result) => {
    if(result.isConfirmed){
    var description = document.createElement("input");
    description.value= `${result.value.description}`.trim();
    description.name="description";
    description.style.display="none";
    form.appendChild(description);
   form.submit();}
})
    return false;
    }
    
        </script>
        <script>
        function confirm(){
          
            var form = document.getElementById("quit");
            Swal.fire({
                title: "هل انت متأكد",
                text: "لا يمكنك الغاء الطلب بعد التأكيد",
                background:"var(--black-color)",
          color:"var(--white-color)",
                showCancelButton: true,
                confirmButtonColor:" var(--move-color)",
                confirmButtonText: "تم", 
                closeOnConfirm: false,
                cancelButtonText: 'إغلاق',
            }).then((result)=>{
                if (result.isConfirmed) {
                    form.submit();
                }
            });
        return false;
        }
        
        
        </script>
        @livewireScripts
</body>