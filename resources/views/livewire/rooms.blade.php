<div>
    <input wire:model.debounce.500ms="searchQuery" type="text" placeholder="بحث" class="input">
    <select wire:model="orderQuery">
    <option value="">ترتيب حسب</option>
    <option value="price">سعر</option>
    <option value="space">مساحة</option>
    <option value="capacity">سعة</option>
    </select>
    <select wire:model="emptyQuery">
    <option value="">شواغر الغرفة</option>
    <option value="full">ممتلئة</option>
    <option value="empty">فارغة تماماً</option>
    <option value="half empty">فارغة جزئياً</option>
    </select>
    <div class="wrapper d-flex gap-20 ">
        <div class="intro p-20   bg-eee">
            @if (Auth()->user()->type=='student')

            @foreach ($rooms as $room)
            @if($room->getEmpty() >0)
            <div class="container">
                <div class="product">
                    <div class="info">
                        <h2> {{ $room->room_name}}</h2>
                        <ul>
                            <li><strong>المساحة : </strong>{{ $room->space }} </li>
                            <li><strong>السعة : </strong>{{ $room->capacity }}</li>
                            <li><strong>الشواغر: </strong> {{ $room->getEmpty() }}</li>
                            <li><strong>الاتجاه: </strong>{{ $room->direction }}</li>
                            <li><strong>السعر: </strong> {{ $room->price }}</li>
                        </ul>
                        @if(!$hasRoom)

                        <form method="POST" action="/student/regestroom/{{ $room->id }}" id="{{ $room->id }}">
                            @csrf
                            <button class="btn">
                                <a href="javascript:{}" onclick="confirm({{ $room->id }})">
                                    تسجيل
                                </a>
                            </button>
                        </form>
                        @elseif (!$hasSwitchRoom && $hasActiveRoom)
                        <form method="POST" action="/student/switch_room/{{ $room->id }}" id="{{ $room->id }}">
                            @csrf
                            <button class="btn">
                                <a href="javascript:{}" onclick="confirm({{ $room->id }})">
                                    تبديل
                                </a>
                            </button>
                        </form>
                        @endif
                    </div>
                </div>
            </div>
            @endif
            @endforeach
            @endif
            @if(Auth()->user()->type=='manager')
            @foreach ($rooms as $room)
            <div class="container">
                <div class="product">
                    <div class="info">
                        <h2> {{ $room->room_name}}</h2>
                        <ul>
                            <li><strong>المساحة : </strong>{{ $room->space }} </li>
                            <li><strong>السعة : </strong>{{ $room->capacity }}</li>
                            <li><strong>الشواغر: </strong> {{ $room->getEmpty() }}</li>
                            <li><strong>الاتجاه: </strong>{{ $room->direction }}</li>
                            <li><strong>السعر: </strong> {{ $room->price }}</li>
                        </ul>
                        @if($room->blocked_now==0)
                        <button class="btn" onclick="confirm3('{{ $room->id }}')" type="submit">حجب </button>
                        <form method="POST" action="{{ route('manager.block',$room->id) }}" id="{{ $room->id }}">
                            @csrf
                        </form>
                        @else
                        <button class="btn" type="submit"> <a href="javascript:{}"
                                onclick="confirm2('{{$room->id}}')">إلغاء الحجب </a></button>
                        <form method="POST" action="{{ route('manager.unblock',$room->id) }}" id="{{  $room->id  }}">
                            @csrf
                        </form>
                        @endif
                        <button class="btn"> <a href="">عقود </a>
                        </button>
                        <button class="btn"> <a
                                href="{{  request()->segment(count(request()->segments())) }}/{{ $room->id }}">
                                تسجيل</a>
                        </button>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
            @if (Auth()->user()->type=='admin')
            @foreach ($rooms as $room)
            <div class="container">
                <div class="product">
                    <div class="info">
                        <h2> {{ $room->room_name}}</h2>
                        <ul>
                            <li><strong>المساحة : </strong>{{ $room->space }} </li>
                            <li><strong>السعة : </strong>{{ $room->capacity }}</li>
                            <li><strong>الشواغر: </strong> {{ $room->getEmpty() }}</li>
                            <li><strong>الاتجاه: </strong>{{ $room->direction }}</li>
                            <li><strong>السعر: </strong> {{ $room->price }}</li>
                        </ul>
                        <button class="btn" title="تعديل"> <a href="javascript:{}"
                                onclick="openForm('{{ $room->id }}')"><i class="fa-solid fa-pen"></i> </a>
                        </button>
                        <button class="btn remove-all-styles"  title="حذف"> <a href="javascript:{}"
                                onclick="confirm('{{ $room->id }}d')"><i
                                    class="fa-solid fa-trash remove-all-styles"></i></a>
                            <form id="{{ $room->id }}d" method="POST" action="/admin/rooms/{{ $room->id }}"
                                class="remove-all-styles ">
                                @csrf
                                @method('DELETE')
                            </form>
                        </button>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="container">
                <div class="product-add">
                    <div class="info-add">
                        <button class="btn-add" title="إضافة غرفة"> <a href="javascript:{}"
                                onclick="openForm('add-form')"><i class="fa-solid fa-plus"></i> </a></button>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>