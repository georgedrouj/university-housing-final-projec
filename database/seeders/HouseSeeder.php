<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HouseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('houses')->insert([
            'id' => 'G1',
            'gender' =>'female',
        ]);
 DB::table('houses')->insert([
            'id' => 'G2',
            'gender' =>'female',
        ]);
 DB::table('houses')->insert([
            'id' => 'G3',
            'gender' =>'female',
        ]);
 DB::table('houses')->insert([
            'id' => 'G4',
            'gender' =>'female',
        ]);
 DB::table('houses')->insert([
            'id' => 'G5',
            'gender' =>'female',
        ]);
 DB::table('houses')->insert([
            'id' => 'G6',
            'gender' =>'female',
        ]);
 DB::table('houses')->insert([
            'id' => 'B1',
            'gender' =>'male',
        ]);
 DB::table('houses')->insert([
            'id' => 'B2',
            'gender' =>'male',
        ]);
 DB::table('houses')->insert([
            'id' => 'B3',
            'gender' =>'male',
        ]);
 DB::table('houses')->insert([
            'id' => 'B4',
            'gender' =>'male',
        ]);
 DB::table('houses')->insert([
            'id' => 'B5',
            'gender' =>'male',
        ]);
 DB::table('houses')->insert([
            'id' => 'B6',
            'gender' =>'male',
        ]);
    }
}
