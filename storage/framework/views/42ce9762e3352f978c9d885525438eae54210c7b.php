<div >
<input wire:model.debounce.500ms="searchQuery" type="text" placeholder="بحث" >
<select name="type" wire:model="type" >
    <option value="">all</option>
    <option value="admin">admin</option>
    <option value="manager">manager</option>
    <option value="supervisor">supervisor</option>
</select>

<div class="container">

 
    <table>
        <thead>
            <tr>
                <th> رقم المستخدم</th>
                <th> اسم المستخدم</th>
                <th> دور المستخدم</th>
                <th>جنس المستخدم</th>
                <th> الأحداث</th>


            </tr>
        </thead>
        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tbody>
            <tr>
                <td> <?php echo e($user->user_id); ?></td>
                <td> <?php echo e($user->name); ?></td>
                <td><?php echo e($user->type); ?> </td>
                <td><?php echo e($user->gender); ?></td>
                <td>
                    <a href="javascript:{}" onclick="openForm('<?php echo e($user->id); ?>')"> <i class="fa-solid fa-pen fa-fw" style="color:var(--move1-color)"></i> </a>
               
                   <a href="javascript:{}" onclick="confirm('<?php echo e($user->id); ?>d')"> <i class="fa-solid fa-trash fa-fw" style="color:var(--red-color)"></i></a>
                        <form id="<?php echo e($user->id); ?>d" method="POST" action='<?php echo e(route('admin.user.destroy',$user->id)); ?>' style="all: revert;display:none">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('DELETE'); ?>
                      </form>
                    
                </td>

            </tr>
        </tbody>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </table>
</div>
</div><?php /**PATH G:\Programing\universtyHousing\resources\views/livewire/users.blade.php ENDPATH**/ ?>