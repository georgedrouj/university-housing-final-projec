<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SupervisorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('supervisors')->insert([
            'user_id' => '5',
            'house_id' =>'G1',
        ]);
  DB::table('supervisors')->insert([
            'user_id' => '6',
            'house_id' =>'G2',
        ]);
  DB::table('supervisors')->insert([
            'user_id' => '3',
            'house_id' =>'B1',
        ]);
  DB::table('supervisors')->insert([
            'user_id' => '4',
            'house_id' =>'B2',
        ]);
    }
}
