<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contract extends Model
{
    use HasFactory;
    use SoftDeletes;
    use \Znck\Eloquent\Traits\BelongsToThrough;
    protected $table='contracts';
    protected $fillable=['room_id','student_id','room_entery_date','room_checkout_date','contract_state','reject_reason'];
    protected $primaryKey='id';
    public function getRoom(){
        return $this->belongsTo(Room::class,'room_id');
    }
    public function getStudent(){
        return $this->belongsTo(Student::class,'student_id');
    }
    public function getQuittances(){
        return $this->hasMany(Quittance::class);
    }
    public function getMaintenances(){
        return $this->hasMany(Quittance::class);
    }
    public function  getDays(){
        $c=new Carbon($this->room_entery_date);
        $n=Carbon::now();
        return $c->diffInDays($n);

    }
    public function getHouse(){
    return $this->belongsToThrough(House::class,Room::class);
    }
}
