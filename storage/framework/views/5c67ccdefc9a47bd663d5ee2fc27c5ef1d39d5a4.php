<!DOCTYPE html>
<html dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- main template css file -->
    
    <link rel="stylesheet" href="<?php echo e(asset('css/framework.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="<?php echo e(asset('css/normalize.css')); ?>">
    <!-- font awesome library -->
    <link rel="stylesheet" href="<?php echo e(asset('css/all.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/users.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/students.css')); ?>">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <title>جامعة القلمون الخاصة</title>
</head>
<body>
    <div class="page d-flex ">
    <div class="sidebar  p-20 p-relative">
        <div class="logo">
            <img src="<?php echo e(asset('images/homoon.png')); ?>" alt="">          
      </div>
        <ul>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/"">
                    <i class="fa-solid fa-sliders fa-fw"></i>
                    <span>الصفحة الرئيسية</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/houses">
                    <i class="fa-solid fa-hotel fa-fw"></i>
                    <span>السكنات </span>
                </a>
            </li>
            <li>
                <a class="d-flex align-center fs-14  rad-6 p-10 " href="/manager/import">

                    <i class="fa-solid fa-people-line fa-fw"></i>
                    <span> الطلاب</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="comfirmation.html">

                    <i class="fa-brands fa-readme fa-fw"></i>
                    <span> طلب التثبيت</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/contract">

                    <i class="fa-brands fa-readme fa-fw"></i>
                    <span> طلب التسجيل</span>
                </a>
            </li>
            <li>
                <a class="d-flex align-center fs-14  rad-6 p-10 " href="/manager/switch_room">

                    <i class="fa-brands fa-readme fa-fw"></i>
                    <span> طلب التبديل</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="quitence.html">

                    <i class="fa-brands fa-readme fa-fw"></i>
                    <span> طلب براءة الذمة</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="mantinance.html">

                    <i class="fa-brands fa-readme fa-fw"></i>
                    <span> طلب  الصيانة</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="">

                    <i class="fa-solid fa-users"></i>
                    <span>مشرفي الوحدات السكنية</span>
                </a>
            </li>
            <li>
                <form method="POST" action="<?php echo e(route('logout')); ?>" id="my_form">
                    <?php echo csrf_field(); ?>
                <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">

                    <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                    <span>  تسجيل خروج</span>
                </a>
                </form>
            </li>

        </ul>

    </div>
    <div class="content">
        <!-- start head -->

        <!-- end head -->



        <div class="wrapper d-grid gap-20 ">
            <div class="welcome bg-white rad-10 ">
                <h1 class="p-relative">إدارة الطلاب</h1>
                <div class="searchimport">
                    <form method="GET" action="<?php echo e(route('manager.import.index')); ?>">
                    <button id="search"  type="submit">بحث </button>
                    <input type="search" name="student-search" placeholder="بحث">
                    </form>
                <hr style="width: 100%">
            
                    <form  method="POST" enctype="multipart/form-data" action="import" class="remove-all-styles " >
                        <?php echo csrf_field(); ?>
                    <input type="file" id="file" name="file" accept=".xls,.xlsx" required>
                    <div>
                    <button class="import" type="submit">
                        استيراد ملف الطلاب 
                  </button>
                    </div>
                </form>
              
                     
                </div>
                <div class="intro p-20 d-flex space-between bg-eee">

                    <div class="container">
                        <table>
                            <thead>
                                <tr>
                                    <th> رقم الطالب</th>
                                    <th> اسم الطالب</th>
                                    <th>اسم السكن</th>
                                    <th> رقم الغرفة</th>
                                   



                                </tr>
                            </thead>
                            
                            <tbody>
                                <?php $__currentLoopData = $students; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $student): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td> <?php echo e($student->university_id); ?></td>
                                    <td> <?php echo e($student->student_name); ?></td>
                                   <?php if($student->getContract()==null): ?>
                                   <td>لا يوجد</td>
                                   <td>لا يوجد</td>
                                    <?php else: ?> 
                                 
                                    <td><?php echo e($student->getContract()->first()->getRoom()->first()->house_id); ?> </td>
                                   <td><?php echo e($student->getContract()->first()->getRoom()->first()->room_name); ?></td>  
                                   <?php endif; ?>
                                   
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                           
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>
    <?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html><?php /**PATH C:\Users\User\Desktop\university-housing-final-projec\resources\views/manager/students.blade.php ENDPATH**/ ?>