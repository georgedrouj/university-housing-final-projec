<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\Contract;
use App\Models\Maintenance;
use App\Models\Quittance;
use App\Models\Student;
use App\Models\SwitchRoom;
use Doctrine\DBAL\Schema\View;
use Illuminate\Contracts\View\View as ViewView;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function index(){
        if(Auth()->user()->type=="student"){
        $student=Student::find(Auth()->user()->user_id);
        $hasOrder=$student->hasOrder();
        $hasActiveRoom=$student->hasActiveRoom();
       return View('student.order')->with(['hasOrder'=>$hasOrder,'hasActiveRoom'=>$hasActiveRoom]);}
    }
}
