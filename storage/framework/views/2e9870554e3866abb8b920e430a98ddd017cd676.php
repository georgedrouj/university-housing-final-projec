<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo e(asset('css/framework.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="<?php echo e(asset('css/normalize.css')); ?>">
    <!-- font awesome library -->
    <link rel="stylesheet" href="<?php echo e(asset('css/all.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/rooms.css')); ?>">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
    <?php echo \Livewire\Livewire::styles(); ?>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script src="<?php echo e(asset('vendor/sweetalert/sweetalert.all.js')); ?>"></script>
    <title>جامعة القلمون الخاصة</title>
</head>

<body>

    <div class="page d-flex  ">
        <div class="sidebar  p-20 p-relative">
            <div class="logo">
                <img src="<?php echo e(asset('images/homoon.png')); ?>" alt="">          
          </div>
          <ul>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/">
                    <i class="fa-solid fa-sliders fa-fw " ></i>
                    <span>الصفحة الرئيسية</span>
                </a>
            </li>
            <li>
                <a class="active d-flex align-center fs-14  rad-6 p-10 " href="/manager/houses">
                    <i class="fa-solid fa-hotel fa-fw"></i>
                    <span>السكنات </span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/students">

                    <i class="fa-solid fa-people-line fa-fw"></i>
                    <span> الطلاب</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/supervisor">

                    <i class="fa-solid fa-users"></i>
                    <span>مشرفي الوحدات السكنية</span>
                </a>
            </li>
        
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/contract">

                    <i class="fa-brands fa-readme fa-fw"></i>
                    <span> طلب التسجيل</span>
                </a>
            </li>
           
            <li>
                <a class="d-flex align-center fs-14  rad-6 p-10 " href="/manager/switch_room">

                    <i class="fa-solid fa-shuffle"></i>
                    <span> طلب التبديل</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/quittance">

                    <i class="fa-solid fa-person-walking-arrow-right"></i>
                    <span> طلب براءة الذمة</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/manager/maintenance">

                    <i class="fa-sharp fa-solid fa-wrench"></i>
                    <span> طلب  الصيانة</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="comfirmation.html">

                    <i class="fa-solid fa-circle-check"></i>
                    <span> طلب التثبيت</span>
                </a>
            </li>
            <li>
                <a class=" d-flex align-center fs-14  rad-6 p-10 " href="comfirmation.html">

                    <i class="fa-solid fa-bullhorn"></i>
                    <span> الإعلانات </span>
                </a>
            </li>
            <li>
              
                <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">

                    <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                    <span>  تسجيل خروج</span>
                </a>
                <form method="POST" action="<?php echo e(route('logout')); ?>" id="my_form" style="display: none">
                    <?php echo csrf_field(); ?>
                </form>
            </li>

        </ul>

        </div>
        <div class="content ">
            <!-- start head -->
            <!-- end head -->
            <h1 class="p-relative">الغرف</h1>
        
            <?php
if (! isset($_instance)) {
    $html = \Livewire\Livewire::mount('rooms', [])->html();
} elseif ($_instance->childHasBeenRendered('2K3hmwk')) {
    $componentId = $_instance->getRenderedChildComponentId('2K3hmwk');
    $componentTag = $_instance->getRenderedChildComponentTagName('2K3hmwk');
    $html = \Livewire\Livewire::dummyMount($componentId, $componentTag);
    $_instance->preserveRenderedChild('2K3hmwk');
} else {
    $response = \Livewire\Livewire::mount('rooms', []);
    $html = $response->html();
    $_instance->logRenderedChild('2K3hmwk', $response->id(), \Livewire\Livewire::getRootElementTagName($html));
}
echo $html;
?>

      
      
    </div>
 
    <?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <script type="text/javascript">
        function openForm()
        {
            document.getElementById("popupForm").style.display="block";
            document.body.id='popedup';
        }
        function closeForm()
        {
            document.getElementById("popupForm").style.display="none";
            document.body.id='';
        }

        </script>
            <script type="text/javascript">
                function confirm(){
              
              var form = document.getElementById("yes");
              swal({
                  title: "هل انت متأكد",
                  text: " لايمكنك التراجع",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "green",
                  confirmButtonText: "موافق", 
                  closeOnConfirm: false
              }, function (isConfirmed) {
                  if (isConfirmed) {
                    console.log('hi');
                      form.submit();
                  }
              });
            return false;
            }
        </script>
         <script type="text/javascript">
            function confirm2(room_id){
              
                var form = document.getElementById(room_id);
                Swal.fire({
                    title: "هل انت متأكد",
                    text: "لا يمكنك الغاء الطلب بعد التأكيد",
                    showCancelButton: true,
                    confirmButtonColor:" var(--move-color)",
                    confirmButtonText: "تم", 
                    closeOnConfirm: false,
                    cancelButtonText: 'إغلاق',
                }).then((result)=>{
                    if (result.isConfirmed) {
                        form.submit();
                    }
                });
            return false;
            }
            
            
            </script>
            <script>
        function confirm3(room_id){
          var form = document.getElementById(room_id);
          console.log(form);
          Swal.fire({
              title: "ادخل سبب الحجب",
              text: " لايمكنك التراجع",
              html: `<textarea   id="block_reason" class="swal2-input" placeholder="سبب الحجب " required style="height: 150px;width: 298px">`,
              showCancelButton: true,
              confirmButtonColor: "var(--move1-color)",
              confirmButtonText: "تم", 
              cancelButtonText: 'أغلاق',
              closeOnConfirm: false,
              preConfirm: () => {
        const block_reason = Swal.getPopup().querySelector('#block_reason').value
        if (!block_reason) {
      Swal.showValidationMessage(`الرجاء ادخال سبب الحجب `)
    }
        return {block_reason:block_reason}
        }
       
          }).then((result) => {
        if(result.isConfirmed){
        var block_reason=document.createElement("input");
        block_reason.value=`${result.value.block_reason}`.trim();
        block_reason.name="block_reason";
        block_reason.style.display="none";
        form.appendChild(block_reason);
       form.submit();}
    })
        return false;
        }
    </script>
    <?php echo \Livewire\Livewire::scripts(); ?>

</body>
<?php /**PATH G:\Programing\universtyHousing\resources\views/manager/room.blade.php ENDPATH**/ ?>