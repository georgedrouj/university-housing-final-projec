<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- framework css file -->
    <  <link rel="stylesheet" href="<?php echo e(asset('css/framework.css')); ?>">
    <!-- main template css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="<?php echo e(asset('css/normalize.css')); ?>">
    <!-- font awesome library -->
    <link rel="stylesheet" href="<?php echo e(asset('css/all.min.css')); ?>">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">

    <title>جامعة القلمون الخاصة</title>
</head>

<body>
    <div class="page d-flex ">
        <div class="sidebar  p-20 p-relative">
            <h3 class="p-relative txt-c mt-0">
                النبراس
            </h3>
            <ul>
                <li>
                    <a class="active d-flex align-center fs-14  rad-6 p-10 " href="/>
                        <i class="fa-solid fa-sliders fa-fw"></i>
                        <span>الصفحة الرئيسية</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/admin/house">
                        <i class="fa-solid fa-hotel fa-fw"></i>
                        <span>إدارة السكنات </span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/admin/user">

                        <i class="fa-solid fa-users-gear fa-fw"></i>
                        <span> إدارة المستخدمين </span>
                    </a>
                    <li>
                        <form method="POST" action="<?php echo e(route('logout')); ?>" id="my_form">
                            <?php echo csrf_field(); ?>
                        <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">
    
                            <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                            <span>  تسجيل خروج</span>
                        </a>
                        </form>
                    </li>

            </ul>

        </div>
        <div class="content">
            <!-- start head -->
            <!-- end head -->
            <h1 class="p-relative">الصفحة الرئيسية</h1>
            <div class="wrapper d-grid gap-20 ">
                <div class="welcome bg-white rad-10 ">
                    <div class="intro p-20 d-flex space-between bg-eee">
                        <div>
                            <h2 class="m-0 ">أهلا بك </h2>
                            <p class="c-gray mt-5 ">admin </p>
                        </div>
                        <img src="../images/download.png" alt="">
                    </div>
                </div>
            </div>

        </div>
    </div>
    
</body>

</html><?php /**PATH C:\Users\User\Desktop\university-housing-final-projec\resources\views/admin/admin.blade.php ENDPATH**/ ?>