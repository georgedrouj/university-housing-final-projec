<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supervisor extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table='supervisors';
    protected $fillable=['user_id','house_id'];
    public function getHouse(){
        return $this->belongsTo(House::class,'house_id');
    }
    public  function getUser()
    {
       return $this->belongsTo(User::class,'user_id');
    }
    public function hasMaintenance()
    {
        return $this->getMaintenances()->where(function ($query) {
            $query->where('maintenace_state', '=', 'in_supervisor');
        })->get()->count()>0;
    }
}
