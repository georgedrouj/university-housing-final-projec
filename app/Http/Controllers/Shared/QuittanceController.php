<?php

namespace App\Http\Controllers\Shared;
use App\Http\Controllers\Controller;
use App\Models\Contract;
use App\Models\Maintenance;
use App\Models\Quittance;
use Carbon\Carbon;
use Illuminate\Http\Request;

class QuittanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        if(auth()->user()->type=='supervisor'){
            $supervisor=auth()->user()->getSupervisors->first();
            $house=$supervisor->getHouse;
            $quittances=$house->getQuittances->where('quittance_state','=','in_supervisor');
            return view('supervisor.quittance',['quittances'=>$quittances]);
        }
        else{
            $quittances=Quittance::where(['quittance_state'=>'in_manager'])->get();
            return view('manager.quittance',['quittances'=>$quittances]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('student.quittance');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $student_id=Auth()->user()->user_id;
        $contract=Contract::where([['student_id','=',$student_id],['contract_state','=','active_now']])->first();
        $quittance=new Quittance();
        $quittance->quittance_state='in_supervisor';
        $quittance->contract_id=$contract->id;
        $quittance->quittance_date=Carbon::now();
        $quittance->save();
        return redirect()->back()->with('success','تمت العملية بنجاح');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Quittance  $quittance
     * @return \Illuminate\Http\Response
     */
    public function show(Quittance $quittance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Quittance  $quittance
     * @return \Illuminate\Http\Response
     */
    public function edit(Quittance $quittance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Quittance  $quittance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quittance $quittance)
    {
        if(Auth()->user()->type=='supervisor'){
            $quittance->missings=$request->input('missings');
            $quittance->electricity=$request->input('electricity');
            $quittance->quittance_state='in_manager';
            $quittance->save();
        }
        else{
            $acc=$request->input('ok');
        if($acc=="true"){
         $quittance->quittance_state='accepted';
         $quittance->manager_comment=$request->input('manager_comment');
         $contract=$quittance->getContract;
         $contract->contract_state='deleted';
         $quittance->save();
         $contract->save();
         $contract->delete();
        }
        else{
         $quittance->quittance_state='rejected';
         $quittance->manager_comment=$request->input('manager_comment');
         $quittance->save();
        } 
        
        }
        return redirect()->back()->with('success','تمت العملية بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Quittance  $quittance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Quittance $quittance)
    {
        //
    }
    public function quittance_by_manager(Request $request,$contract_id)
    {
        $contract=Contract::find($contract_id);
        $quittance=new Quittance();
        $quittance->missings=$request->input('missings');
        $quittance->electricity=$request->input('electricity');
        $quittance->manager_comment=$request->input('manager_comment');
        $quittance->quittance_state='accepted';
        $quittance->contract_id=$contract->id;
        $quittance->quittance_date=Carbon::now();
        $quittance->save();
        $contract->contract_state='deleted';
        $contract->save();
        $contract->delete();
        return redirect()->back()->with('success','تمت عملية تفريغ الغرفة بنجاح');

    }

}
