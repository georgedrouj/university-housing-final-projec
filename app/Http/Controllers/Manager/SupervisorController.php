<?php

namespace App\Http\Controllers\Manager;
use App\Http\Controllers\Controller;
use App\Models\House;
use App\Models\Supervisor;
use App\Models\User;
use Illuminate\Http\Request;

class SupervisorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //  $supervisors=Supervisor::all();
      //  $femaleSupervisors=User::where('type','=','supervisor')->where('gender','=','female')->get();
      //  $maleSupervisors=User::where('type','=','supervisor')->where('gender','=','male')->get();
        $supervisors=User::where('type','=','supervisor')->get();
    
        $femaleHouses=House::where('gender','=','female')->get();
        $maleHouses=House::where('gender','=','male')->get();
        return view('manager.supervisors')->with(['supervisors'=>$supervisors,'femaleHouses'=>$femaleHouses,'maleHouses'=>$maleHouses]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
   //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,User $user)
    {
        $supervisor=new Supervisor();
        $supervisor->user_id=$user->id;
        $supervisor->house_id=$request->input('house_id');
        $supervisor->save();
        return redirect()->back()->with('success','تم إضافة المشرف بنجاح');
       /* if($request->input('femaleSupervisor')==null)
        {
            $supervisor->user_id=$request->input('maleSupervisor'); 
        }
        else
        {
            $supervisor->user_id=$request->input('femaleSupervisor');
        }*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Supervisor $supervisor,House $house)
    {
        
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $user_id)
    {
       $supervisor=Supervisor::where('user_id',$user_id)->first();
        $supervisor->house_id=$request->input('house_id');
        $supervisor->save();
  
    return redirect()->route('manager.supervisor.index')->with('success','تم تعديل سكن المشرف بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supervisor $supervisor)
    {
        $supervisor->delete();
        return redirect()->route('manager.supervisor.index')->with('success','تم حذف المشرف بنجاح');
    }
}
