<div>
    <input wire:model.debounce.500ms="searchQuery" type="text" placeholder="بحث" >
    <select wire:model="type">
        <option value="" selected hidden> اختر نوع الطلب </option>
        <option value=""> الكل </option>
        <option value="صيانة"> صيانة</option>
        <option value="براءة ذمة"> براءة ذمة</option>
        <option value="تبديل"> تبديل</option>

    </select>
    <div class="container">
  
        <table>
            <thead>
                <tr>
                    <th> رقم التسلسلي للطلب</th>
                    <th> نوع الطلب</th>
                    <th>حالة الطلب</th>
                    <th> تاريخ الطلب </th>
                </tr>
            </thead>
          
            <tbody>
                <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($order['order_id']); ?></td>
                    <td> <?php echo e($order['name']); ?></td>
                    <td><?php echo e($order['order_state']); ?></td>
                    <td><?php echo e(Carbon\Carbon::parse($order['created_at'])->format('Y-m-d')); ?></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              
            </tbody>
        
        </table>
    </div>
</div>
<?php /**PATH G:\Programing\universtyHousing\resources\views/livewire/orders.blade.php ENDPATH**/ ?>