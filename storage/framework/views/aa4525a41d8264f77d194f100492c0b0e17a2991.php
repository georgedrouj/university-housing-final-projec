<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo e(asset('css/create-user.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/framework.css')); ?>">
    <title>إدارة المستخدمين</title>
</head>
</head>

<body>
    <form method="POST" action="<?php echo e(route('admin.user.update',$user->id)); ?>" id="popupForm" >
        <?php echo csrf_field(); ?>
        <?php echo method_field('PUT'); ?>
        <h3> تعديل مستخدم</h3>
        <button class="closeModal" onclick="closeForm()"> </button>
        <label for="username">رقم المستخدم</label>
        <input type="text" placeholder="أدخل رقم المستخدم" id="username" name="user_id" value="<?php echo e($user->user_id); ?>" required>
        <label for="username">اسم المستخدم</label>
        <input type="text" placeholder="أدخل اسم المستخدم: " id="username" name="name" value="<?php echo e($user->name); ?>" required>

        <label for="password">كلمة المرور</label>
        <input type="password" placeholder="أدخل كلمة المرور:" id="password" name="password" value="<?php echo e($user->password); ?>" required>
        <label for="text"> اختر دور المستخدم</label>
        <select class=" gender"  name="type" value="<?php echo e($user->type); ?>">
            <option value="manager">مدير</option>
            <option value="supervisor">مشرف</option>
            <option value="admin">أدمن</option>
        </select>
        <label for=""> اختر جنس المستخدم</label>
        <select  name="gender" class=" gender" value=" <?php echo e($user->gender); ?>">
         
            <option value="male">ذكر</option>
            <option value="female">أنثى</option>
        </select>
        <button type="submit"> تعديل </button>
    </form>
</body>

</html>

</body>

</html><?php /**PATH G:\Programing\universtyHousing\resources\views/admin/user/edit.blade.php ENDPATH**/ ?>