<?php

namespace App\Http\Livewire;

use App\Models\Advertisment;
use Livewire\Component;

class Advirtesment extends Component
{
    public $searchQuery;
    public $addressed_to;
    public function mount(){
       $this->searchQuery="";
       $this->addressed_to="";
    }

    public function render()
    {
        if(auth()->user()->type=='manager')
        {
                $advertisments=Advertisment::when($this->searchQuery!='',
               function ($query){
                   $query->where('title','like','%'.$this->searchQuery.'%');
                 })->when($this->addressed_to!='',
                 function($query){
                 $query->where('addressed_to','=',$this->addressed_to);
                 })->get();

        }
            
        else{
            if(auth()->user()->type=='student'){
            $advertisments=Advertisment::where('addressed_to','=','student')->orwhere('addressed_to','=','all')->when($this->searchQuery!='',
        function ($query){
            $query->where('title','like','%'.$this->searchQuery.'%');
        })->latest()->get();}
        else{
            $advertisments=Advertisment::where('addressed_to','=','supervisor')->orwhere('addressed_to','=','all')->when($this->searchQuery!='',
            function ($query){
                $query->where('title','like','%'.$this->searchQuery.'%');
            })->latest()->get();  
        }
        }
        return view('livewire.advirtesment',['advetisments'=>$advertisments]);
    }
}
