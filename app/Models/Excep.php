<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Excep extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table='exceps';
    protected $fillable=['student_id','date','description'];
    public function getStudent(){
        return $this->belongsTo(Student::class);
    }
}
