<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
class CheckDate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if(config::get('global.Start_date')<=Carbon::now() || config::get('global.End_date')<=Carbon::now())
        {
         return view('student.confirmation');
        }
        else
        {
            abort(403,'تاريخ تجديد العقد(التثبيت) لم يبدأ بعد');
        }
        return $next($request);
    }
}
