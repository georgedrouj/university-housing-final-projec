<?php

namespace App\Http\Controllers\Shared;

use App\Http\Controllers\Controller;
use App\Models\Contract;
use App\Models\Excep;
use App\Models\Room;
use App\Models\Student;
use App\Models\StudentData;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class ContractController extends Controller
{
    public $error='';
    public function checkElegibaltiy($id){
        $years=array('bussnies'=>4,'IT'=>5,"pharmacy"=>5,"dentist"=>5,"mecatronics"=>5,"arch"=>5,"law"=>5,"graphic_design"=>4,
        "decore"=>4,"medicine"=>5,"civil"=>5
        );
        $student_data= StudentData::where('university_id','=',$id)->first();
        $student=Student::where('university_id','=',$id)->first();
        $yearEleigibal=(int)date('Y')-(int)Str::substr($student_data['university_id'], 0, 4);
        $collegYear=$years[$student_data['collage']];
        if($student_data['gender']=='female'&&$student_data['collage']=='medicine'){
            $collegYear++;
        }
        $disaproval=false;
        if($student!=Null){
            $disaproval=$student['disapproval'];
        }
     
        if(!$disaproval){
            if(!($student_data['academic_status']=='regular')){
                $this->error='إن الحالة الاكادمية لهذا الطالب غير نظامية ولا يحق له التسجيل في السكن ';
            return false;}
            if(($yearEleigibal>$collegYear)){
                $this->error='إن هذا الطالب تجاوز الحد الاعلى من السنين  ولا يحق له التسجيل في السكن ';
            return false;}
        }
        else{
            $this->error='إن هذا الطالب مستنكف ولا يحق له التسجيل في السكن ';
            return false;
        }
    return true;
     }
    public function index()
    {
        $contracts=Contract::where('contract_state','=','in_progress')->get();
      
        return view('manager.regestration_request')->with(['contracts'=>$contracts]);
    }
    public function create()
    {
       
    }
    public function store(Request $request,$room_id)
    {
        $this->Validate($request,[
            'student_id'=>'required|integer|max:202300000|min:200010000',
            'student_name'=>'required|string',
            ''
        ],
        [
        'student_id.required'=>'يجب عليك إدخال الرقم الجامعي للطالب ',
        'student_id.max'=>'الرقم الجامعي المدخل خاطئ',
        'student_id.min'=>'الرقم الجامعي المدخل خاطئ',
        'student_id.integer'=>'الرقم الجامعي المدخل خاطئ',
        'student_name.required'=>'يجب عليك إدخال اسم الطالب',
        ]);
    
        $student_id=$request->input('student_id');
        $student_name=$request->input('student_name');
        $type=$request->input('type');
        $room=Room::where('id','=',$room_id)->first();
        $gender=$room->getHouse()->first()->gender;
        $ok=true;
        $student=Student::where('university_id','=',$student_id)->first();

        if($type!=1){
          
            if($student==null){
                return redirect()->back()->with('errors','إن هذا الطالب غير موجود');
            }
        }
        if($type==0){
           $ok=$this->checkElegibaltiy($student_id);
        }
        elseif($type==1){
            $student=new Student();
            $student->university_id=$student_id;
            $student->Student_name=$student_name;
            $student->gender=$gender;
            $student->disapproval=false;
            $student->save();
            $user =new User();
            $user->type='student';
            $user->gender=$gender;
            $user->user_id=$student_id;
            $user->name=$student_name;
            $user->password=Hash::make($student_id);
            $user->save();
        }
        else{
            $student->disapproval=false;
            $student->save();
            $desc=$request->input('desc');
            $excp=new  Excep();
            $excp->student_id=$student_id;
            $excp->date=Carbon::now();
            $excp->description=$desc;
            $excp->save();
         
        }
     
        if($ok){
            if($gender!=$student->gender){
                return redirect()->back()->with('errors','إن جنس هذا الطالب لا يطابق مع السكن');
            }
        if($type!=1&&$student->hasRoom()){
            return redirect()->back()->with('errors','أن هذا الطالب لديه غرفة او طلب على غرفة مسبقا');
        }
        $contract=new Contract();
        $contract->room_id=$room_id;
        $contract->student_id=$student_id;
        $contract->room_entery_date=Carbon::now();
        $contract->contract_state="active_now";
        $contract->save();
       return redirect()->back()->with('success', 'تمت عملية تسجيل الطالب بنجاح');}
       else{
        return redirect()->back()->with('errors',$this->error);
       }

    }
    public function show(Contract $contract)
    {
      
    }
    public function edit(Contract $contract)
    {
      
    }
    public function update(Request $request, Contract $contract)
    {
        $acc=$request->input('ok');
        $reason=$request->input('reason');
        if($acc=="true"){
         $contract->contract_state='active_now';
         $contract->save();
        }
        else{
         $contract->contract_state='rejected';
         $contract->reject_reason=$reason;
         $contract->save();
         $contract->delete();
    }
        return redirect()->back()->with('toast_success','تـمت العملية بنجاح');
 
    }
    public function regestRoom($room_id){
      
        $student_id=Auth()->user()->user_id;
 
       $ok= $this->checkElegibaltiy($student_id);
            if($ok){
            $contract=new Contract();
            $contract->student_id=$student_id;
            $contract->room_id=$room_id;
            $contract->room_entery_date=Carbon::now();
            $contract->contract_state='in_progress';
            $contract->save();
            return redirect()->back()->with('success','تمت عملية تسجيل بالنجاح الرجاء  تثبيت المالي خلال يومين ومراجعة مدير السكن');}
            else{
                return redirect()->back()->with('errors',$this->error);
            }
    }
   
    
}
