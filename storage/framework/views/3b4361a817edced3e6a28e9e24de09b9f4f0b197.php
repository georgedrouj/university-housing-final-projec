<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- framework css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/framework.css')); ?> ">
    <!-- main template css file -->
    <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
    <!-- render all elements normally -->
    <link rel="stylesheet" href="<?php echo e(asset('css/normalize.css')); ?>">
    <!-- font awesome library -->
    <link rel="stylesheet" href="<?php echo e(asset('css/all.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/rooms.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/create-room.css')); ?>">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <script src="<?php echo e(asset('vendor/sweetalert/sweetalert.all.js')); ?>"></script>
    <title>جامعة القلمون الخاصة</title>
</head>

<body>

    <div class="page d-flex  ">
        <div class="sidebar  p-20 p-relative">
            <h3 class="p-relative txt-c mt-0">
                النبراس
            </h3>
            <ul>
                <li>
                    <a class="d-flex align-center fs-14  rad-6 p-10 " href="/">
                        <i class="fa-solid fa-sliders fa-fw"></i>
                        <span>الصفحة الرئيسية</span>
                    </a>
                </li>
                <li>
                    <a class=" active d-flex align-center fs-14  rad-6 p-10 " href="/admin/house">
                        <i class="fa-solid fa-hotel fa-fw"></i>
                        <span>إدارة السكنات</span>
                    </a>
                </li>
                <li>
                    <a class=" d-flex align-center fs-14  rad-6 p-10 " href="/admin/user">

                        <i class="fa-solid fa-users-gear fa-fw"></i>
                        <span> إدارة المستخدمين </span>
                    </a>
                </li>
                <li>
                   
                    <a href="javascript:{}" class=" d-flex align-center fs-14  rad-6 p-10 " onclick="document.getElementById('my_form').submit();">

                        <i class="fa-solid fa-right-from-bracket fa-fw"></i>
                        <span>  تسجيل خروج</span>
                    </a>
                    <form method="POST" action="<?php echo e(route('logout')); ?>" id="my_form"  class="remove-all-styles">
                        <?php echo csrf_field(); ?>
                    </form>
                </li>
            </ul>

        </div>
        <div class="content ">
            <!-- start head -->
            <!-- end head -->
            <h1 class="p-relative">إدارة السكن</h1>
  
         
            <div class="wrapper d-flex gap-20 ">
                <!-- <div class="welcome bg-white rad-10 "> -->
                    <div class="intro p-20   bg-eee" id="alldata">
               
              
                        <?php $__currentLoopData = $rooms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $room): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div id="container"> 
                            <div class="product">
                                <div class="info">
                                    <h2> <?php echo e($room->room_name); ?></h2>
                                    <ul>
                                        <li><strong>المساحة : </strong><?php echo e($room->space); ?> </li>
                                        <li><strong>السعة : </strong><?php echo e($room->capacity); ?></li>
                                        <li><strong>الشواغر: </strong> <?php echo e($room->getEmpty()); ?></li>
                                        <li><strong>الاتجاه: </strong><?php echo e($room->direction); ?></li>
                                        <li><strong>السعر: </strong> <?php echo e($room->price); ?></li>
    
                                    </ul>

                                <button class="btn" title="تعديل"> <a href="javascript:{}" onclick="openForm('<?php echo e($room->id); ?>')"><i class="fa-solid fa-pen"></i> </a>
                                </button>
                                <button class="btn remove-all-styles" id="button" title="حذف"> <a  href="javascript:{}" onclick="confirm('<?php echo e($room->id); ?>d')"><i class="fa-solid fa-trash remove-all-styles"></i></a>
                                    <form id="<?php echo e($room->id); ?>d" method="POST" action="/admin/rooms/<?php echo e($room->id); ?>" class="remove-all-styles ">
                                        <?php echo csrf_field(); ?>
                                        <?php echo method_field('DELETE'); ?>
                                         
                                       </form>
                                </button>
                            </div>
                        </div>

                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <div id="container">
                    <div class="product-add">

                        <div class="info-add">
                            <button class="btn-add" title="إضافة غرفة"> <a href="javascript:{}" onclick="openForm('add-form')"><i class="fa-solid fa-plus"></i> </a>
                        </button>

                        </div>

                    </div>
                </div>
                <div  id="searchdata" class="intro p-20   bg-eee"> 
                     
                     


                </div>

            </div>



        </div>
     
      
    </div>
   
</div>
<?php $__currentLoopData = $rooms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $room): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<form method="POST" action="/admin/rooms/<?php echo e($room->id); ?>" id="<?php echo e($room->id); ?>" class="popupForm" style="display: none">
    <?php echo csrf_field(); ?>
    <?php echo method_field('PUT'); ?>
    <h3> تعديل غرفة</h3>
    <button class="closeModal" type="button" onclick="colseForm('<?php echo e($room->id); ?>')"> </button>
    <label for="roomname">رقم الغرفة</label>
    <input type="text" placeholder="أدخل رقم الغرفة:" name="name" value="<?php echo e($room->room_name); ?>">
    <label for="floorname">رقم الطابق</label>
    <input type="text" placeholder="أدخل رقم الطابق:" name="floor" value="<?php echo e($room->floor_name); ?>">
    <label for="roomcapacity">سعة الغرفة</label>
    <input type="text" placeholder="أدخل سعة الغرفة:" name="capacity" value="<?php echo e($room->capacity); ?>">
    <label for="roomspace">مساحة الغرفة</label>
    <input type="text" placeholder="أدخل مساحة الغرفة:" name="space" value="<?php echo e($room->space); ?>" >
    <label for="roomdirection">اتجاه الغرفة</label>
    <input type="text" placeholder="أدخل اتجاه الغرفة:" name="direction" value="<?php echo e($room->direction); ?>">
    <label for="roomprice">سعر الغرفة</label>
    <input type="price" placeholder="أدخل سعر الغرفة:" name="price" value="<?php echo e($room->price); ?>">
    <button> تعديل </button>

</form>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<form method="POST" action="/admin/houses/<?php echo e(last(request()->segments())); ?>" class="popupForm" id="add-form" style="display: none">
    <?php echo csrf_field(); ?>
    <h3> إضافة غرفة</h3>
    <button class="closeModal" type="button" onclick="colseForm('add-form')"> </button>
    <label for="roomname">رقم الغرفة</label>
    <input type="text" placeholder="أدخل رقم الغرفة:" name="name" required>
    <label for="floorname">رقم الطابق</label>
    <input type="text" placeholder="أدخل رقم الطابق:" name="floor" required>
    <label for="roomcapacity">سعة الغرفة</label>
    <input type="text" placeholder="أدخل سعة الغرفة:" name="capacity" required>
    <label for="roomspace">مساحة الغرفة</label>
    <input type="text" placeholder="أدخل مساحة الغرفة:" name="space" required>
    <label for="roomdirection">اتجاه الغرفة</label>
    <input type="text" placeholder="أدخل اتجاه الغرفة:" name="direction" required>
    <label for="roomprice">سعر الغرفة</label>
    <input type="price" placeholder="أدخل سعر الغرفة:" name="price" required>
    <button type="submit"> إضافة </button>

</form>
<script type="text/javascript">
    function openForm(id){
        document.getElementById(id).style.display = "block";
        document.body.id='popedup';
    }
    function colseForm(id){
      document.getElementById(id).style.display = "none";
        document.body.id='';
    }
    function confirm(room_id){
  var form = document.getElementById(room_id);
  Swal.fire({
      title: "هل انت متأكد",
      text: " لايمكنك التراجع",
  
      showCancelButton: true,
      confirmButtonColor: "red",
      confirmButtonText: "تم", 
      cancelButtonText: 'أغلاق',
      closeOnConfirm: false
  }).then((result) => {
  /* Read more about isConfirmed, isDenied below */
  if (result.isConfirmed) {
   form.submit();
  } 
});
return false;
}
    
      </script>
<?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>;
</body><?php /**PATH G:\Programing\universtyHousing\resources\views/admin/room/index.blade.php ENDPATH**/ ?>