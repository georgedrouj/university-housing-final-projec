
# HOMOON
A real-world web app for automat the administrative tasks in Alnebras company in the university of Kalamoon to help both students and employees From the moment the student is registered in the university housing until the moment of requesting a quittance.
* Its a final university project we get 95/100 mak for it 
## Problem The Project Solve 

- The main prolem we solve is make the work easier for bothe students and mangers in the university housing by transforming the paper work to automated work 
- You can see the full feature list in the documentation below


## Lessons Learned
While working in this project we learned alot :
* Gathered requirements by doing interviews with the managers
* Analysed the requirements
* Designed the System
* Working with laravel in advance laravel
* Working with sql for database 
* Deploy the app
* Team mangement 
* Using laravel livewire
* Using package for work with excel files 

## Documentation

[Documentation](https://drive.google.com/file/d/1DX_NhUBrPH13r6H-wATC6axrCUjUgrHd/view?usp=sharing)

[User_Manual](https://drive.google.com/file/d/1v_1AI5AfMxAheE7Gp022WhMGLktQjtas/view?usp=sharing)

![Logo](https://i.ibb.co/fkSdZMj/homoon.png)

